package systems.hexa.meek;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;

import io.fabric.sdk.android.Fabric;
import systems.hexa.meek.config.DebugConfig;
import systems.hexa.meek.utils.stetho.StethoCustomConfigBuilder;
import timber.log.Timber;

public class MeekApp extends Application {
    @Override
    public void onCreate() {
        MultiDex.install(getApplicationContext());
        super.onCreate();
//        setupFabric();
        setupTimber();
        setupTimber();
        if (DebugConfig.DEV_BUILD) {
            setupStetho();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new Timber.DebugTree());
//            Timber.plant(new CrashlyticsTree());
        }
    }

    private void setupStetho() {
        Stetho.Initializer initializer = new StethoCustomConfigBuilder(this)
                .viewHierarchyInspectorEnabled(false)
                .build();
        Stetho.initialize(initializer);
    }

    private void setupFabric() {
        Fabric.with(this, new Crashlytics());
    }
}

/*
https://github.com/nmoskalenko/RxFirebase/blob/master/sample/src/main/java/kelvinapps/com/sample/SampleActivity.java
https://github.com/nmoskalenko/RxFirebase/tree/master/sample/src/main/java/kelvinapps/com/sample
https://github.com/firebase/quickstart-android/blob/master/auth/app/src/main/java/com/google/firebase/quickstart/auth/EmailPasswordActivity.java
https://github.com/firebase/quickstart-android/blob/master/auth/app/src/main/java/com/google/firebase/quickstart/auth/GoogleSignInActivity.java
https://console.firebase.google.com/u/1/project/meek-final/database/data
 */