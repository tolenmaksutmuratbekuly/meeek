package systems.hexa.meek.config;

import systems.hexa.meek.BuildConfig;

public class MeekConfig {
    public static final String SCHEME = "https://";
    public static final String DOMAIN = BuildConfig.MEEK_HOST;
    public static final String API_BASE_URL = SCHEME + DOMAIN;
}
