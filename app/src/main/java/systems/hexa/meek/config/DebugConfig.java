package systems.hexa.meek.config;

import systems.hexa.meek.BuildConfig;

public class DebugConfig {
    public static final boolean DEV_BUILD = BuildConfig.DEBUG;
}