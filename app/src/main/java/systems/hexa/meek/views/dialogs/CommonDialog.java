package systems.hexa.meek.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import systems.hexa.meek.R;

public class CommonDialog {
    private Context context;
    private CallbackSingleBtn callbackSingleBtn;
    private CallbackYesNo callbackYesNo;
    private CallbackListView callbackListView;

    public CommonDialog(Context context) {
        this.context = context;
    }

    public void setCallbackSingleBtn(CallbackSingleBtn callbackSingleBtn) {
        this.callbackSingleBtn = callbackSingleBtn;
    }

    public void setCallbackYesNo(CallbackYesNo callbackYesNo) {
        this.callbackYesNo = callbackYesNo;
    }

    public void setCallbackListView(CallbackListView callbackListView) {
        this.callbackListView = callbackListView;
    }

    public void showDialogTitleDescSingleBtn(String title, String description, boolean showTitle, boolean showDescription) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_titledesc_single_btn);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        dialog.findViewById(R.id.tv_title).setVisibility(showTitle ? View.VISIBLE : View.GONE);
        ((TextView) dialog.findViewById(R.id.tv_title)).setText(title);
        ((TextView) dialog.findViewById(R.id.tv_desc)).setText(description);
        dialog.findViewById(R.id.tv_desc).setVisibility(showDescription ? View.VISIBLE : View.GONE);

        final Button btn_action = dialog.findViewById(R.id.btn_action);
        btn_action.setOnClickListener(v -> {
            if (callbackSingleBtn != null) {
                callbackSingleBtn.onClickAction();
            }
            dialog.dismiss();
        });
        dialog.show();
    }

    public void addFriendDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_friend);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final Button btn_okay = dialog.findViewById(R.id.btn_okay);
        final TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        View.OnClickListener clickListener = v -> {
            if (callbackYesNo != null) {
                if (v.equals(btn_okay))
                    callbackYesNo.onClickYes();
                else if (v.equals(tv_cancel))
                    callbackYesNo.onClickNo();

                dialog.dismiss();
            }
        };
        btn_okay.setOnClickListener(clickListener);
        tv_cancel.setOnClickListener(clickListener);

        dialog.show();
    }

    public void showDialogYesNo(String title) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_yesno);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        final Button btn_yes = dialog.findViewById(R.id.btn_yes);
        final Button btn_no = dialog.findViewById(R.id.btn_no);


        ((TextView) dialog.findViewById(R.id.tv_title)).setText(title);

        View.OnClickListener clickListener = v -> {
            if (callbackYesNo != null) {
                if (v.equals(btn_yes))
                    callbackYesNo.onClickYes();
                else if (v.equals(btn_no))
                    callbackYesNo.onClickNo();

                dialog.dismiss();
            }
        };
        btn_yes.setOnClickListener(clickListener);
        btn_no.setOnClickListener(clickListener);

        dialog.show();
    }

    public void showListDialog(Context context, List<String> titles) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_listview);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        ListView lv = dialog.findViewById(R.id.listView);
        lv.setOnItemClickListener((adapterView, view, index, l) -> {
            if (index == titles.size() - 1) {
                dialog.dismiss();
            } else {
                if (callbackListView != null) {
                    callbackListView.onItemClick(index);
                    dialog.dismiss();
                }
            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, titles);
        lv.setAdapter(adapter);

        dialog.show();
    }

    public interface CallbackSingleBtn {
        void onClickAction();
    }

    public interface CallbackListView {
        void onItemClick(int index);
    }

    public interface CallbackYesNo {
        void onClickYes();

        void onClickNo();
    }
}