package systems.hexa.meek.views.fonts.helvetica;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class BtnHelveticaMedium extends Button {

    public BtnHelveticaMedium(Context context) {
        super(context);
        init();
    }

    public BtnHelveticaMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BtnHelveticaMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "helvetica/HelveticaNeueCyr-Medium.otf");
        setTypeface(tf, Typeface.NORMAL);
    }
}