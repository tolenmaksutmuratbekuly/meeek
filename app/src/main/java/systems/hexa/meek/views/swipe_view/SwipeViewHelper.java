package systems.hexa.meek.views.swipe_view;

import android.animation.Animator;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import systems.hexa.meek.utils.LOTimber;
import systems.hexa.meek.utils.animation.AnimationUtils;

class SwipeViewHelper implements View.OnTouchListener {

    private final SwipeView mSwipeView;
    private GestureDetector gestureDetector;
    private View mObservedView;

    private boolean mListenForTouchEvents;
    private float mDownX;
    private float mDownY;
    private float mInitialX;
    private float mInitialY;
    private int mPointerId;

    private float mRotateDegrees = SwipeView.DEFAULT_SWIPE_ROTATION;
    private float mOpacityEnd = SwipeView.DEFAULT_SWIPE_OPACITY;
    private int mAnimationDuration = SwipeView.DEFAULT_ANIMATION_DURATION;

    SwipeViewHelper(SwipeView swipeView) {
        mSwipeView = swipeView;
        gestureDetector = new GestureDetector(swipeView.getContext(), new SingleTapConfirm());

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            mSwipeView.onViewClick();
            return true;
        } else {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (!mListenForTouchEvents || !mSwipeView.isEnabled()) {
                        return false;
                    }

                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    mSwipeView.onSwipeStart();
                    mPointerId = event.getPointerId(0);
                    mDownX = event.getX(mPointerId);
                    mDownY = event.getY(mPointerId);

                    return true;

                case MotionEvent.ACTION_MOVE:
                    int pointerIndex = event.findPointerIndex(mPointerId);
                    if (pointerIndex < 0) return false;

                    float dx = event.getX(pointerIndex) - mDownX;
                    float dy = event.getY(pointerIndex) - mDownY;

                    float newX = mObservedView.getX() + dx;
                    float newY = mObservedView.getY() + dy;

                    mObservedView.setX(newX);
                    mObservedView.setY(newY);

                    float dragDistanceX = newX - mInitialX;
                    float swipeProgress = Math.min(Math.max(
                            dragDistanceX / mSwipeView.getWidth(), -1), 1);

                    checkViewPosition(MotionEvent.ACTION_MOVE);

                    if (mRotateDegrees > 0) {
                        float rotation = mRotateDegrees * swipeProgress;
                        mObservedView.setRotation(rotation);
                    }

                    if (mOpacityEnd < 1f) {
                        float alpha = 1 - Math.min(Math.abs(swipeProgress * 2), 1);
                        mObservedView.setAlpha(alpha);
                    }

                    return true;

                case MotionEvent.ACTION_UP:
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    checkViewPosition(MotionEvent.ACTION_UP);
                    mSwipeView.onSwipeEnd();

                    return true;
            }
        }
        return false;
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }


    private void checkViewPosition(int action) {
        if (!mSwipeView.isEnabled()) {
            returnViewInitPosition();
            return;
        }

        float verticalCenterTop = mObservedView.getY() + (mObservedView.getHeight() / 2);
        float swipeDistanceTop = mSwipeView.getHeight() / 3f;

        float centerHorizontal = mObservedView.getX() + (mObservedView.getWidth() / 2);
        float swipeDistanceRight = mSwipeView.getWidth() / 3f;
        float swipeDistanceLeft = swipeDistanceRight * 2;

        if (verticalCenterTop < swipeDistanceTop) {
            if (action == MotionEvent.ACTION_UP) {
                swipeViewToTop(mAnimationDuration / 2);
            } else if (action == MotionEvent.ACTION_MOVE) {
                mSwipeView.onSwipeProgress(SwipeView.SWIPE_DIRECTION_ONLY_TOP);
            }
        } else if (centerHorizontal < swipeDistanceRight) {
            if (action == MotionEvent.ACTION_UP) {
                swipeViewToLeft(mAnimationDuration / 2);
            } else if (action == MotionEvent.ACTION_MOVE) {
                mSwipeView.onSwipeProgress(SwipeView.SWIPE_DIRECTION_ONLY_LEFT);
            }
        } else if (centerHorizontal > swipeDistanceLeft) {
            if (action == MotionEvent.ACTION_UP) {
                swipeViewToRight(mAnimationDuration / 2);
            } else if (action == MotionEvent.ACTION_MOVE) {
                mSwipeView.onSwipeProgress(SwipeView.SWIPE_DIRECTION_ONLY_RIGHT);
            }
        } else {
            if (action == MotionEvent.ACTION_UP) {
                returnViewInitPosition();
            } else if (action == MotionEvent.ACTION_MOVE) {
                mSwipeView.onSwipeProgress(SwipeView.SWIPE_DIRECTION_NOTHING);
            }
        }
    }

    private void returnViewInitPosition() {
        mObservedView.animate()
                .x(mInitialX)
                .y(mInitialY)
                .rotation(0)
                .alpha(1)
                .setDuration(mAnimationDuration)
                .setInterpolator(new OvershootInterpolator(1.4f))
                .setListener(null);
    }

    private void swipeViewToTop(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        mObservedView.animate()
                .y(-mSwipeView.getHeight() + mObservedView.getY())
                .rotation(0)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimationUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeView.onViewSwipedToTop();
                    }
                });
    }

    private void swipeViewToLeft(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        mObservedView.animate()
                .x(-mSwipeView.getWidth() + mObservedView.getX())
                .rotation(-mRotateDegrees)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimationUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeView.onViewSwipedToLeft();
                    }
                });
    }

    private void swipeViewToRight(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        mObservedView.animate()
                .x(mSwipeView.getWidth() + mObservedView.getX())
                .rotation(mRotateDegrees)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimationUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeView.onViewSwipedToRight();
                    }
                });
    }

    void registerObservedView(View view, float initialX, float initialY) {
        if (view == null) return;
        mObservedView = view;
        mObservedView.setOnTouchListener(this);
        mInitialX = initialX;
        mInitialY = initialY;
        mListenForTouchEvents = true;
    }

    void unregisterObservedView() {
        if (mObservedView != null) {
            mObservedView.setOnTouchListener(null);
        }
        mObservedView = null;
        mListenForTouchEvents = false;
    }

    void setAnimationDuration(int duration) {
        mAnimationDuration = duration;
    }

    void setRotation(float rotation) {
        mRotateDegrees = rotation;
    }

    void setOpacityEnd(float alpha) {
        mOpacityEnd = alpha;
    }

}