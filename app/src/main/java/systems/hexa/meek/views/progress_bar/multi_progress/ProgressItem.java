package systems.hexa.meek.views.progress_bar.multi_progress;

public class ProgressItem {
    public int color;
    public int labelColor;
    public String label;
    public int progressItemPercentage;

    public ProgressItem(int color, int labelColor, String label, int progressItemPercentage) {
        this.color = color;
        this.labelColor = labelColor;
        this.label = label;
        this.progressItemPercentage = progressItemPercentage;
    }
}