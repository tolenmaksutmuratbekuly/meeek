package systems.hexa.meek.views.circlemenu;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import systems.hexa.meek.R;
import systems.hexa.meek.views.fonts.helvetica.TvHelveticaMedium;
import systems.hexa.meek.views.fonts.open_sans.TvOpenSansBold;

public class CenterMenuButton extends CircleMenuButton {
    private boolean expanded;
    private AnimatorSet preLollipopAnimationSet;

    public CenterMenuButton(Context context) {
        super(context);
        this.expanded = false;

        setGravity(Gravity.CENTER);
        TextView textView = new TvOpenSansBold(context);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextSize(14f);
        textView.setTextColor(ContextCompat.getColor(context, R.color.center_btn_txt));
        textView.setText(context.getString(R.string.select_one));
        addView(textView);
    }

    @Override
    void updateBackground() {
        setColorNormalResId(R.color.center_btn_bg);
        setColorPressedResId(R.color.center_btn_bg_click);
        super.updateBackground();
    }

    public void setExpanded(boolean isExpanded) {
        expanded = isExpanded;
        startPreLollipopAnimation();
    }

    private void startPreLollipopAnimation() {
        if (preLollipopAnimationSet == null) {
            preLollipopAnimationSet = createPreLollipopIconAnimation();
        }

        preLollipopAnimationSet.start();
    }

    private AnimatorSet createPreLollipopIconAnimation() {
        AnimatorSet preLollipopAnimationSet = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(
                ContextCompat.getDrawable(getContext(), R.drawable.ab_drawer_menu), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(
                ContextCompat.getDrawable(getContext(), R.drawable.ab_drawer_menu), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(this, "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(this, "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        preLollipopAnimationSet.play(scaleOutX).with(scaleOutY);
        preLollipopAnimationSet.play(scaleInX).with(scaleInY).after(scaleOutX);
        preLollipopAnimationSet.setInterpolator(new OvershootInterpolator(2));
        return preLollipopAnimationSet;
    }
}
