package systems.hexa.meek.views.progress_bar;

interface OnProgressBarListener {
    void onProgressChange(int current, int max);
}