package systems.hexa.meek.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import systems.hexa.meek.R;

public class TmpProgressView extends LinearLayout {

    private ProgressBar progressBar;
    private TextView tvMessage;

    public TmpProgressView(Context context) {
        super(context);
        inflate();
    }

    public TmpProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public TmpProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_tmp_progress, this);

        initView();
    }

    private void initView() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tvMessage = (TextView) findViewById(R.id.tv_message);

        tvMessage.setVisibility(GONE);
    }

    public void setMessage(String message) {
        tvMessage.setText(message);
    }

    public void setMessageTextColor(int color) {
        tvMessage.setTextColor(color);
    }

}