package systems.hexa.meek.views.progress_bar.multi_progress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.ArrayList;

import systems.hexa.meek.utils.ContextUtils;
import systems.hexa.meek.utils.StringUtils;

public class MultiProgressBar extends android.support.v7.widget.AppCompatSeekBar {
    private int CLICK_ACTION_THRESHOLD = 200;
    private float startX;
    private float startY;

    private int HEIGHT_EXPANDED = 132;
    private int HEIGHT_COLLAPSED = 72;

    private ArrayList<ProgressItem> mProgressItemsList;
    private float mLabelSize;
    private float default_progress_text_offset;
    private Paint progressPaint;
    private Paint mLabelPaint;
    private boolean showLabel = false;
    private boolean touchable;

    public MultiProgressBar(Context context) {
        super(context);
        mProgressItemsList = new ArrayList<>();
    }

    public MultiProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initData(ArrayList<ProgressItem> progressItemsList) {
        this.mProgressItemsList = progressItemsList;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (touchable) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startX = event.getX();
                    startY = event.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    float endX = event.getX();
                    float endY = event.getY();
                    if (isAClick(startX, endX, startY, endY)) {
                        showLabel(!isShownLabel());
                        invalidate();
                    }
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD || differenceY > CLICK_ACTION_THRESHOLD);
    }

    protected void onDraw(Canvas canvas) {
        if (mProgressItemsList.size() > 0) {
            int progressBarWidth = getWidth();
//            int progressBarHeight = getHeight();
            int progressBarHeight = showLabel ? HEIGHT_EXPANDED : HEIGHT_COLLAPSED;
            int thumboffset = getThumbOffset();
            int lastProgressX = 0;
            int progressItemWidth, progressItemRight;

            progressPaint = new Paint();
            mLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mLabelPaint.setTextSize(mLabelSize);

            mLabelSize = ContextUtils.sp2px(getContext(), 12.5f);
            default_progress_text_offset = ContextUtils.dp2px(getContext(), 5.3f);

            Rect progressRect = new Rect();
            for (int i = 0; i < mProgressItemsList.size(); i++) {
                ProgressItem progressItem = mProgressItemsList.get(i);
                progressPaint.setColor(progressItem.color);

                progressItemWidth = progressItem.progressItemPercentage * progressBarWidth / 100;
                progressItemRight = lastProgressX + progressItemWidth;
                // for last item give right to progress item to the width
                if (i == mProgressItemsList.size() - 1 && progressItemRight != progressBarWidth) {
                    progressItemRight = progressBarWidth;
                }
                progressRect.set(lastProgressX, thumboffset / 2, progressItemRight, progressBarHeight - thumboffset / 2);
                canvas.drawRect(progressRect, progressPaint);
                lastProgressX = progressItemRight;

                // ===== Label Design =====
                if (showLabel) {
                    mLabelPaint.setColor(progressItem.labelColor);
                    float labelWidth = mLabelPaint.measureText(progressItem.label) + default_progress_text_offset;
                    float percentWidth = mLabelPaint.measureText(progressItem.progressItemPercentage + "% ");
                    float fullWidth = labelWidth + percentWidth;
                    if (percentWidth <= progressRect.width()) {
                        float mLabelStart = (progressRect.left + default_progress_text_offset);
                        float mDrawTextEnd = (int) ((progressBarHeight / 2.0f) - ((mLabelPaint.descent() + mLabelPaint.ascent()) / 2.0f));
                        canvas.drawText(progressItem.progressItemPercentage + "% "
                                        + (fullWidth <= progressRect.width()
                                        ? progressItem.label
                                        : StringUtils.EMPTY_STRING),
                                mLabelStart, mDrawTextEnd, mLabelPaint);
                    }
                }
            }
            super.onDraw(canvas);
        }
    }

    public void showLabel(boolean showLabel) {
        this.showLabel = showLabel;
    }

    public boolean isShownLabel() {
        return showLabel;
    }

    public void setTouchable(boolean touchable) {
        this.touchable = touchable;
    }
}
