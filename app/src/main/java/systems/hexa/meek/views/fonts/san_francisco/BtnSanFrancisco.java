package systems.hexa.meek.views.fonts.san_francisco;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class BtnSanFrancisco extends Button {

    public BtnSanFrancisco(Context context) {
        super(context);
        init();
    }

    public BtnSanFrancisco(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BtnSanFrancisco(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "san_francisco/SF-UI-Display-Regular.ttf");
        setTypeface(tf, Typeface.NORMAL);
    }
}