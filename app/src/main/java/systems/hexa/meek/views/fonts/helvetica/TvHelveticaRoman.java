package systems.hexa.meek.views.fonts.helvetica;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TvHelveticaRoman extends AppCompatTextView {

    public TvHelveticaRoman(Context context) {
        super(context);
        init();
    }

    public TvHelveticaRoman(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TvHelveticaRoman(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "helvetica/HelveticaNeueCyr-Roman.otf");
        setTypeface(tf, Typeface.NORMAL);
//        setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3.0f, getResources().getDisplayMetrics()), 1.0f);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            setLetterSpacing(.03f);
//        }
    }
}