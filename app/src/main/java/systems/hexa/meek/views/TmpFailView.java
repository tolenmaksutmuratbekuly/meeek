package systems.hexa.meek.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import systems.hexa.meek.R;

public class TmpFailView extends LinearLayout implements View.OnClickListener {

    private TextView tvMessage;
    private TextView btnTryAgain;

    private OnTryAgainClickListener onTryAgainClickListener;

    public TmpFailView(Context context) {
        super(context);
        inflate();
    }

    public TmpFailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public TmpFailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    public void setOnTryAgainClickListener(OnTryAgainClickListener onTryAgainClickListener) {
        this.onTryAgainClickListener = onTryAgainClickListener;
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_tmp_fail, this);

        initView();
    }

    private void initView() {
        tvMessage = (TextView) findViewById(R.id.tv_message);
        btnTryAgain = (TextView) findViewById(R.id.btn_try_again);

        btnTryAgain.setOnClickListener(this);
    }

    public void setMessage(String message) {
        tvMessage.setText(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_try_again:
                onTryAgainClickListener.onTryAgainClick();
                break;
        }
    }

    public interface OnTryAgainClickListener {
        void onTryAgainClick();
    }
}