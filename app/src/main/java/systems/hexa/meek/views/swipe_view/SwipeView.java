
package systems.hexa.meek.views.swipe_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import systems.hexa.meek.R;

public class SwipeView extends LinearLayout {

    public static final int SWIPE_DIRECTION_NOTHING = -1;
    public static final int SWIPE_DIRECTION_BOTH = 0;
    public static final int SWIPE_DIRECTION_ONLY_LEFT = 1;
    public static final int SWIPE_DIRECTION_ONLY_RIGHT = 2;
    public static final int SWIPE_DIRECTION_ONLY_TOP = 3;

    public static final int DEFAULT_ANIMATION_DURATION = 300;
    public static final float DEFAULT_SWIPE_ROTATION = 30f;
    public static final float DEFAULT_SWIPE_OPACITY = 1f;
    public static final float DEFAULT_SCALE_FACTOR = 1f;

    private int mAnimationDuration;
    private float mSwipeRotation;
    private float mSwipeOpacity;
    private float mScaleFactor;

    private SwipeViewHelper mSwipeHelper;
    private SwipeListener mListener;

    public SwipeView(Context context) {
        this(context, null);
    }

    public SwipeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttributes(attrs);
        initialize();
    }

    private void readAttributes(AttributeSet attributeSet) {
        TypedArray attrs = getContext().obtainStyledAttributes(attributeSet, R.styleable.SwipeView);

        try {
            mAnimationDuration = attrs.getInt(R.styleable.SwipeView_animation_duration,
                    DEFAULT_ANIMATION_DURATION);
            mSwipeRotation = attrs.getFloat(R.styleable.SwipeView_swipe_rotation, DEFAULT_SWIPE_ROTATION);
            mSwipeOpacity = attrs.getFloat(R.styleable.SwipeView_swipe_opacity, DEFAULT_SWIPE_OPACITY);
            mScaleFactor = attrs.getFloat(R.styleable.SwipeView_scale_factor, DEFAULT_SCALE_FACTOR);
        } finally {
            attrs.recycle();
        }
    }

    private void initialize() {
        setClipToPadding(false);
        setClipChildren(false);

        mSwipeHelper = new SwipeViewHelper(this);
        mSwipeHelper.setAnimationDuration(mAnimationDuration);
        mSwipeHelper.setRotation(mSwipeRotation);
        mSwipeHelper.setOpacityEnd(mSwipeOpacity);

        reorder();
    }

    private void reorder() {
        int newPositionX = (getWidth() - getMeasuredWidth()) / 2;
        int newPositionY = getPaddingTop();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTranslationZ(1);
        }

        float scaleFactor = (float) Math.pow(mScaleFactor, 1);

        mSwipeHelper.unregisterObservedView();
        mSwipeHelper.registerObservedView(this, newPositionX, newPositionY);
        animate()
                .y(newPositionY)
                .scaleX(scaleFactor)
                .scaleY(scaleFactor)
                .alpha(1)
                .setDuration(mAnimationDuration);
    }

    public void onSwipeStart() {
        if (mListener != null) mListener.onSwipeStart();
    }

    public void onSwipeProgress(int swipeDirection) {
        if (mListener != null) {
            mListener.onSwipeProgress(swipeDirection);
        }
    }

    public void onSwipeEnd() {
        if (mListener != null) mListener.onSwipeEnd();
    }

    public void onViewSwipedToTop() {
        if (mListener != null) mListener.onViewSwipedToTop();
    }

    public void onViewSwipedToLeft() {
        if (mListener != null) mListener.onViewSwipedToLeft();
    }

    public void onViewSwipedToRight() {
        if (mListener != null) mListener.onViewSwipedToRight();
    }

    public void onViewClick() {
        if (mListener != null) mListener.onViewClick();
    }

    public void setListener(@Nullable SwipeListener listener) {
        mListener = listener;
    }

    public interface SwipeListener {
        void onViewSwipedToTop();

        void onViewSwipedToLeft();

        void onViewSwipedToRight();

        void onSwipeStart();

        void onSwipeProgress(int swipeDirection);

        void onSwipeEnd();

        void onViewClick();
    }
}
