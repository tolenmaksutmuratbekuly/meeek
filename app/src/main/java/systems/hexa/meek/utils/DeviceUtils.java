package systems.hexa.meek.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;

import java.util.Locale;

import systems.hexa.meek.BuildConfig;
import systems.hexa.meek.R;

public class DeviceUtils {
    private final Context mContext;
    public final String OS = "android";

    public DeviceUtils(Application context) {
        this.mContext = context;
    }

    public String getDeviceId() {
        return Settings.Secure.getString(this.mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public String getAppVersion() {
        return BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getOS() {
        return OS;
    }

    public int getScreenWidth() {
        return ((WindowManager) this.mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
    }

    public int getScreenHeight() {
        return ((WindowManager) this.mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
    }

    public boolean isOnline() {
        NetworkInfo netInfo = ((ConnectivityManager) this.mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    private static final String PLATFORM = "Android";
    private static final String APP_NAME = "Meek";

    private static double getDiagonalInInches(Context context) {
        DisplayMetrics displayMetrics  = context.getResources().getDisplayMetrics();
        double d = displayMetrics.density * 160.0f;
        return Math.sqrt(Math.pow((double)displayMetrics.widthPixels / d, 2.0) + Math.pow((double)displayMetrics.heightPixels / d, 2.0));
    }

    public static Point getDisplaySize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point;
    }

    public static Point getDisplaySize(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public static String getLanguage() {
        return DeviceUtils.getLocale().getLanguage().toLowerCase();
    }

    public static Locale getLocale() {
        return Locale.getDefault();
    }

    public static int getStatusBarHeight(Context context) {
        if (Build.VERSION.SDK_INT < 21) {
            return 0;
        }
        return DeviceUtils.getStatusBarHeight(context.getResources());
    }

    public static int getStatusBarHeight(Resources resources) {
        int n = 0;
        int n2 = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (n2 > 0) {
            n = resources.getDimensionPixelSize(n2);
        }
        return n;
    }

    public static int getToolbarHeight(Context context) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true)) {
            return TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics());
        }
        return context.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
    }

    public static int getViewYLocationOnScreen(View view) {
        int[] arrn = new int[2];
        view.getLocationOnScreen(arrn);
        return arrn[1];
    }

    public static boolean hasLocationPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION ) == 0;
    }

    public static void removeOnGlobalLayoutListener(View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
            return;
        }
        view.getViewTreeObserver().removeGlobalOnLayoutListener(onGlobalLayoutListener);
    }

    /*
     * Enabled aggressive block sorting
     */
    public static void setUpViewBelowStatusBar(View view) {
        if (Build.VERSION.SDK_INT < 21 || view == null) {
            return;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, DeviceUtils.getStatusBarHeight(view.getContext()), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
    }

    public static boolean smallDensity(Context context) {
        int n = context.getResources().getDisplayMetrics().densityDpi;
        return n == 120 || n == 160 || n == 240;
    }

    public static boolean isAboveLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isAboveM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isAboveNougat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }

    public static boolean isJellyBeanMR1OrAbove() {
        return Build.VERSION.SDK_INT >= 17;
    }
}
