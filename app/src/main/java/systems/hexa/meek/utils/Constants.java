package systems.hexa.meek.utils;

public class Constants {
    public static final String T_USERS = "Users";
    public static final String T_USERNAMES = "Usernames";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_SEARCH_ATTRIBUTES = "search_attributes";
    public static final String KEY_GENDER_MALE = "male";
    public static final String KEY_GENDER_FEMALE = "female";

    public static final String FORM_KEY = "FORM_KEY";
    public static final String KEY_BOOLEAN = "isSomeBool";
    public static final String LAYOUT_ID = "layoutId";
    public static final String TYPE = "type";
    public static final String DESCRIPTION = "description";
    public static final String RESULT = "result";
    public static final String ANSWER = "answer";
    public static final String USER_ID = "USER_ID";

    public static final int TYPE_WORLD = 1;
    public static final int TYPE_COUNTRY = 2;
    public static final int TYPE_CITY = 3;
    public static final int TYPE_GROUP = 4;
    public static final int TYPE_FRIEND = 5;
}