package systems.hexa.meek.utils.rx;


import rx.Observable;
import systems.hexa.meek.data.models.APIResponse;
import systems.hexa.meek.data.network.exceptions.APIException;

public class Transformers {
    private static final Observable.Transformer responseDataExtractor =
            (Observable.Transformer<APIResponse, Object>) o -> o.map(response -> {
                try {
                    return response.extractData();
                } catch (APIException e) {
                    e.printStackTrace();
                    return null;
                }
            });

    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<APIResponse<T>, T> responseDataExtractor() {
        return (Observable.Transformer<APIResponse<T>, T>) responseDataExtractor;
    }

}
