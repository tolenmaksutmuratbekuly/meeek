package systems.hexa.meek.utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;

import java.util.Map;

@SuppressWarnings("unused")
public class DebugUtils {
    public static Map<String, Object> bundleToMap(Bundle bundle) {
        if (bundle == null) {
            return null;
        }

        Map<String, Object> result = new ArrayMap<>();
        for (String key : bundle.keySet()) {
            result.put(key, result.get(key));
        }
        return result;
    }

    public static String activityResultCodeToString(int resultCode) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                return "RESULT_OK(" + resultCode + ")";
            case Activity.RESULT_CANCELED:
                return "RESULT_CANCELED(" + resultCode + ")";
            case Activity.RESULT_FIRST_USER:
                return "RESULT_FIRST_USER(" + resultCode + ")";
            default:
                return Integer.toString(resultCode);
        }
    }

    public static void androidAssert(boolean condition) {
        if (!condition) {
            throw new AssertionError();
        }
    }

    public static void androidAssert(String message, boolean condition) {
        if (!condition) {
            throw new AssertionError(message);
        }
    }
}
