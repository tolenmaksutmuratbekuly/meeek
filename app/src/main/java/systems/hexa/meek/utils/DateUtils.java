package systems.hexa.meek.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static final DateFormat BIRTHDATE_DISPLAY_FORMAT = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());
    public static final DateFormat BIRTHDATE_USERNAME = new SimpleDateFormat("MMddyy", Locale.getDefault());
    public static final DateFormat BIRTHDATE_FACEBOOK = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

    public static String getBirthdateStr(Date birthdate) {
        return BIRTHDATE_DISPLAY_FORMAT.format(birthdate);
    }

    public static String getUsernameBirthdate(Date birthdate) {
        return BIRTHDATE_USERNAME.format(birthdate);
    }
}
