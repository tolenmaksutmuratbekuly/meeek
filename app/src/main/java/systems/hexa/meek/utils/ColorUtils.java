package systems.hexa.meek.utils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;

import systems.hexa.meek.R;

public class ColorUtils {
    public static final int[] GR_1 = {R.color.gr_i_1, R.color.gr_ii_1};
    public static final int[] GR_2 = {R.color.gr_i_2, R.color.gr_ii_2};
    public static final int[] GR_3 = {R.color.gr_i_3, R.color.gr_ii_3};
    public static final int[] GR_4 = {R.color.gr_i_4, R.color.gr_ii_4};
    public static final int[] GR_5 = {R.color.gr_i_5, R.color.gr_ii_5};
    public static final int[] GR_6 = {R.color.gr_i_6, R.color.gr_ii_6};
    public static final int[] GR_7 = {R.color.gr_i_7, R.color.gr_ii_7};
    public static final int[] GR_8 = {R.color.gr_i_8, R.color.gr_ii_8};
    public static final int[] GR_9 = {R.color.gr_i_9, R.color.gr_ii_9};
    public static final int[] GR_10 = {R.color.gr_i_10, R.color.gr_ii_10};
    public static final int[] GR_11 = {R.color.gr_i_11, R.color.gr_ii_11};
    public static final int[] GR_12 = {R.color.gr_i_12, R.color.gr_ii_12};
    public static final int[] GR_13 = {R.color.gr_i_13, R.color.gr_ii_13};
    public static final int GRADIENT_COLORS[][] = {
            GR_1, GR_2, GR_3, GR_4, GR_5, GR_6, GR_7, GR_8, GR_9, GR_10, GR_11, GR_12, GR_13
    };

    public static int getColor(Context context, int colorResourceId) {
        return ContextCompat.getColor(context, colorResourceId);
    }

    public static int[] getColorsForSwipeRefreshLayout(Context context) {
        return new int[]{
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorPrimaryDark),
                ContextCompat.getColor(context, R.color.colorAccent)
        };
    }

    public static void changeRoundedShapeBackgroundColor(View v, int color) {
        Drawable background = v.getBackground();

        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(color);
        } else {
            v.setBackgroundColor(color);
        }
    }

    public static GradientDrawable getGradient(Context context, @ColorRes int... colorResIds) {
        int[] colorRes = new int[colorResIds.length];
        for (int i = 0; i < colorResIds.length; i++) {
            colorRes[i] = ContextCompat.getColor(context, colorResIds[i]);
        }
        return new GradientDrawable(
                GradientDrawable.Orientation.BOTTOM_TOP, colorRes);
    }
}

