package systems.hexa.meek.utils;

import systems.hexa.meek.BuildConfig;
import timber.log.Timber;

public class LOTimber {
    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Timber.d(message);
        }
    }
}
