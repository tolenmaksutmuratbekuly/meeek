package systems.hexa.meek.utils.animation;

import android.content.Context;
import android.support.annotation.AnimRes;
import android.view.animation.Animation;
import android.widget.EditText;

public class AccessCodeAnimationUtils {
    private static EditText et;
    private static int step = 0;
    private static Animation animation;

    public static Animation loadAnimation(Context context, @AnimRes int id) {
        animation = android.view.animation.AnimationUtils.loadAnimation(context, id);
        return animation;
    }

    public static void setEditText(EditText editText) {
        et = editText;
    }

    public static Animation.AnimationListener setAnimationListener() {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (et != null) {
                    et.setText("");
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }
}
