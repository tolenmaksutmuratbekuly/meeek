package systems.hexa.meek.utils;

public class StringUtils {
    public static final String EMPTY_STRING = "";

    public static String trim(String s) {
        return s != null ? s.trim() : null;
    }

    public static int length(String s) {
        return s == null ? 0 : s.length();
    }

    public static String removeLastChar(String str) {
        if (length(str) > 0) {
            return str.substring(0, str.length() - 1);
        } else {
            return StringUtils.EMPTY_STRING;
        }
    }

    public static boolean isStringOk(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static String replaceNull(String text) {
        if (isStringOk(text)) {
            return text;
        } else {
            return EMPTY_STRING;
        }
    }
}