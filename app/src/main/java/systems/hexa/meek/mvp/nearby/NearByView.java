package systems.hexa.meek.mvp.nearby;


import systems.hexa.meek.mvp.MVPView;

public interface NearByView extends MVPView {
    void showLoadingIndicator(boolean show);

    void openMainScreen();

    void finish();
}
