package systems.hexa.meek.mvp.auth.signup;

import java.util.Date;

import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.Gender;
import systems.hexa.meek.mvp.BaseMvpView;

public interface SignUpDetailView extends BaseMvpView {
    void showBirthDateError(int errorRes);

    void showGenderError(int errorRes);

    void setBirthdate(String birthdateStr);

    void setGender(Gender gender);

    void setAuthForm(AuthFormValidation form);

    void showDatePickerDialog(Date currentBirthdate);

    void setFinishTextInBtn();

    void onRegistrationSuccess();
}
