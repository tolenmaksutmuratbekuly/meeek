package systems.hexa.meek.mvp.auth.intro;

import android.content.pm.PackageManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class IntroPresenter extends MvpPresenter<IntroView> {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public void onGrandAccessClick(boolean checkSelfPermissions, boolean checkPermissionRationale) {
        if (checkLocationPermission(checkSelfPermissions, checkPermissionRationale)) {
            if (checkSelfPermissions) {
                getViewState().openQuestionActivity();
                // Request location updates:
//                locationManager.requestLocationUpdates(provider, 400, 1, this);
            }
        }
    }

    private boolean checkLocationPermission(boolean checkSelfPermissions, boolean checkPermissionRationale) {
        if (!checkSelfPermissions) {
            if (checkPermissionRationale) {
                // Show explanation
                getViewState().showGrandAccessDialog();
            } else {
                getViewState().requestPermission();
            }
            return false;
        } else {
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults, boolean checkSelfPermissions) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkSelfPermissions) {
                        getViewState().openQuestionActivity();
//                        locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }
                }
//                else {
//                    // permission denied, boo! Disable the
//                }
            }

        }
    }
}
