package systems.hexa.meek.mvp.auth;

import systems.hexa.meek.mvp.BaseMvpView;

public interface AuthView extends BaseMvpView {
    void openMainScreen();
}
