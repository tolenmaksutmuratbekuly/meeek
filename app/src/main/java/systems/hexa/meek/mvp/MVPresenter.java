package systems.hexa.meek.mvp;

public interface MVPresenter<T> {
    void attachView(T view);

    void detachView();
}