package systems.hexa.meek.mvp.nearby;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;
import systems.hexa.meek.utils.StringUtils;

public class PassCodePresenter implements MVPresenter<PassCodeView> {
    private final DataLayer dataLayer;
    private PassCodeView mvpView;

    public PassCodePresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(PassCodeView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

    public void onBackspaceClick(String accessCode) {
        mvpView.setBackspaceResult(StringUtils.removeLastChar(accessCode));
    }
}
