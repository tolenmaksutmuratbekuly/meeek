package systems.hexa.meek.mvp.questions.ask_question;


import systems.hexa.meek.mvp.MVPView;

public interface SelectLocationView extends MVPView {
    void showLoadingIndicator(boolean show);

    void finish();
}
