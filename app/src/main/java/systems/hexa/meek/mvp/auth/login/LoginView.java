package systems.hexa.meek.mvp.auth.login;

import android.support.annotation.StringRes;

import com.google.firebase.auth.FirebaseUser;

import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.mvp.BaseMvpView;

public interface LoginView extends BaseMvpView {
    void setEmail(String email);

    void setPwd(String pwd);

    void showEmailError(@StringRes int errorRes);

    void showPwdError(@StringRes int errorRes);

    void onSuccessAuthorized(User user);
}
