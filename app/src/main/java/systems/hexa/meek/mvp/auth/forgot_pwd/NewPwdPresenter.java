package systems.hexa.meek.mvp.auth.forgot_pwd;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import org.parceler.Parcels;

import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.utils.Constants;

import static systems.hexa.meek.utils.StringUtils.trim;

@InjectViewState
public class NewPwdPresenter extends MvpPresenter<NewPwdView> {
    private AuthFormValidation form;
    private String confirmPwd;

    public void init() {
        if (form == null) {
            throw new NullPointerException("form == null. Did you forget to call onRestoreInstanceState() ?");
        } else {
            getViewState().setNewPwd(form.pwd);
            getViewState().setConfirmPwd(form.pwd);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putParcelable(Constants.FORM_KEY, Parcels.wrap(form));
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(Constants.FORM_KEY)) {
            form = Parcels.unwrap(savedState.getParcelable(Constants.FORM_KEY));
        } else {
            form = new AuthFormValidation();
        }
    }

    public void setNewPwd(String newPwd) {
        form.pwd = trim(newPwd);
    }

    public void setConfirmPwd(String confirmPwd) {
        this.confirmPwd = trim(confirmPwd);
    }

    public void validation() {
        boolean ok;
        ok = validatePwd();
        ok &= validateConfirmPwd();
        if (ok) {

        }
    }

    private boolean validatePwd() {
        int errorRes = form.validatePwd();
        if (errorRes != 0) {
            getViewState().showNewPwdError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateConfirmPwd() {
        int errorRes = form.validateConfirmPwd(confirmPwd);
        if (errorRes != 0) {
            getViewState().showConfirmPwdError(errorRes);
        }
        return errorRes == 0;
    }
}
