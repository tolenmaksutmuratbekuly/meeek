package systems.hexa.meek.mvp.auth.intro;

import systems.hexa.meek.mvp.BaseMvpView;

public interface IntroView extends BaseMvpView {
    void openQuestionActivity();

    void finish();

    void showGrandAccessDialog();

    void requestPermission();
}