package systems.hexa.meek.mvp.auth.signup;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import systems.hexa.meek.data.models.auth.AuthFormValidation;

import static systems.hexa.meek.utils.StringUtils.length;
import static systems.hexa.meek.utils.StringUtils.trim;

@InjectViewState
public class SignUpPresenter extends MvpPresenter<SignUpView> {
    private AuthFormValidation authForm;

    public void init(AuthFormValidation form) {
        authForm = form;
        if (authForm != null) {
            if (length(authForm.firstName) > 0)
                getViewState().setFirstName(form.firstName);
            if (length(authForm.lastName) > 0)
                getViewState().setLastName(form.lastName);

        }
    }

    public void setFirstName(String firstName) {
        authForm.firstName = trim(firstName);
    }

    public void setLastName(String lastName) {
        authForm.lastName = trim(lastName);
    }

    public void validate() {
        if (validateFirstName() && validateLastName()) {
            getViewState().setAuthForm(authForm);
        }
    }

    private boolean validateFirstName() {
        int errorRes = authForm.validateFirstName();
        if (errorRes != 0) {
            getViewState().showFirstNameError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateLastName() {
        int errorRes = authForm.validateLastName();
        if (errorRes != 0) {
            getViewState().showLastNameError(errorRes);
        }
        return errorRes == 0;
    }
}
