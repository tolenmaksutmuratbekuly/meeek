package systems.hexa.meek.mvp.auth.forgot_pwd;


import systems.hexa.meek.mvp.BaseMvpView;

public interface ForgotPwdView extends BaseMvpView {
    void openMainScreen();
}
