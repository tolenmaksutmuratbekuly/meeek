package systems.hexa.meek.mvp.questions.detail.chat;


import android.content.Context;
import android.support.annotation.StringRes;

import java.util.List;

import systems.hexa.meek.data.models.questions.detail.chat.ChatMessage;
import systems.hexa.meek.mvp.MVPView;

public interface ChatView extends MVPView {
    Context getContext();

    void addMessages(List<ChatMessage> promotions);

    void showListPlaceholder(@StringRes int textRes);

    void hideListPlaceholder();
}
