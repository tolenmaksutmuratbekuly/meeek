package systems.hexa.meek.mvp.auth.login;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.facebook.AccessToken;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.kelvinapps.rxfirebase.RxFirebaseAuth;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.social.facebook.FacebookProvider;
import systems.hexa.meek.data.social.facebook.FacebookProviderException;
import systems.hexa.meek.data.social.google.GoogleAuthProvider;
import systems.hexa.meek.data.social.google.GoogleProviderException;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.FBUser;
import systems.hexa.meek.data.models.auth.GPlusUser;
import systems.hexa.meek.data.models.auth.Users;
import systems.hexa.meek.mvp.auth.signup.CreateNewUser;
import systems.hexa.meek.utils.LOTimber;

@InjectViewState
public class StartPresenter extends MvpPresenter<StartView> {
    private DataLayer dataLayer;
    private FacebookProvider fbProvider;
    private GoogleAuthProvider gplusProvider;
    private Subscription googleLogoutSubscription;
    private Subscription loginSubscription;
    private CreateNewUser createNewUser;
    private AuthFormValidation authForm; // We use it for Google Plus authentication

    public void init(DataLayer dataLayer, FacebookProvider fbProvider, GoogleAuthProvider gplusProvider, AuthFormValidation form) {
        this.dataLayer = dataLayer;
        this.fbProvider = fbProvider;
        this.gplusProvider = gplusProvider;
        createNewUser = new CreateNewUser(dataLayer.getFirebaseDatabase());
    }

    @Override
    public void detachView(StartView view) {
        super.detachView(view);
        if (loginSubscription != null) {
            loginSubscription.unsubscribe();
            loginSubscription = null;
        }
        if (googleLogoutSubscription != null) {
            googleLogoutSubscription.unsubscribe();
            googleLogoutSubscription = null;
        }
    }

    private void onRegisteredSuccess(String uniqueId) {
        // Save current userId locally
        dataLayer.getPreferences().setUserId(uniqueId);
        getViewState().onRegistrationSuccess();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Facebook Authentication
    ///////////////////////////////////////////////////////////////////////////

    public void performFbAuth() {
        if (fbProvider == null) return;
        if (loginSubscription != null) loginSubscription.unsubscribe();

        loginSubscription = fbProvider.getAccessToken()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .subscribe(new Subscriber<AccessToken>() {
                    @Override
                    public void onNext(AccessToken accessToken) {
                        fbProvider.getFBUser(accessToken)
                                .subscribe(new Subscriber<FBUser>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        getViewState().showLoadingIndicator(false);
                                        getViewState().showRequestError(e.getMessage());
                                    }

                                    @Override
                                    public void onNext(FBUser fbUser) {
                                        signInWithCredentials(new Users(fbUser.first_name, fbUser.last_name,
                                                fbUser.first_name + " " + fbUser.last_name, fbUser.email,
                                                fbUser.first_name + fbUser.last_name,
                                                fbUser.getBirthday(),
                                                fbUser.getGender() + ""), accessToken.getToken());
                                    }
                                });
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showLoadingIndicator(false);
                        if (e instanceof FacebookProviderException) {
                            switch (((FacebookProviderException) e).errorCode) {
                                case FACEBOOK_SDK_ERROR:
                                    fbProvider.logout();
                                    getViewState().onFacebookSdkError();
                                    break;
                                case PERMISSION_NOT_GRANTED:
                                    getViewState().onEmailPermissionNotGranted();
                                    break;
                            }
                        } else if (e instanceof GoogleProviderException) {
                            switch (((GoogleProviderException) e).errorCode) {
                                case GOOGLE_API_CONNECTING:
                                    getViewState().onGoogleApiConnectingError();
                                    break;
                                case GOOGLE_API_NOT_CONNECTED:
                                    getViewState().onGoogleApiNotConnectedError();
                                    break;
                                case GOOGLE_SDK_ERROR:
                                    gplusProvider.logout();
                                    getViewState().onGoogleSdkError();
                                    break;
                            }
                        } else {
                            gplusProvider.logout();
                        }
                    }
                });
    }

    private void signInWithCredentials(Users user, String token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        RxFirebaseAuth.signInWithCredential(dataLayer.getFirebaseAuth(), credential)
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<AuthResult>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showRequestError(e.getMessage());
                    }

                    @Override
                    public void onNext(AuthResult task) {
                        if (task.getUser() != null) {
                            FirebaseUser firebaseUser = task.getUser();
                            String uniqueId = firebaseUser.getUid();
                            createNewUser.userAlreadyExists(uniqueId, userExist -> {
                                if (userExist) {
                                    onRegisteredSuccess(uniqueId);
                                } else {
                                    createNewUser.createNewUser(uniqueId, user, new CreateNewUser.Callback() {
                                        @Override
                                        public void showLoadingIndicator(boolean show) {
                                            getViewState().showLoadingIndicator(show);
                                        }

                                        @Override
                                        public void onUserCreatedSuccess() {
                                            onRegisteredSuccess(uniqueId);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
    }

    ///////////////////////////////////////////////////////////////////////////
    // Google Plus authentication
    ///////////////////////////////////////////////////////////////////////////

    public void performGPlusAuth() {
        if (fbProvider == null) return;
        if (loginSubscription != null) loginSubscription.unsubscribe();

        loginSubscription = gplusProvider.getUserInfo()
                .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<GPlusUser>() {
                    @Override
                    public void onNext(GPlusUser gPlusUser) {
                        LOTimber.d("asdlkasjdl " +
                                "token=" + gPlusUser.token + "|" +
                                "displayName=" + gPlusUser.displayName + "|" +
                                "familyName=" + gPlusUser.familyName + "|" +
                                "givenName=" + gPlusUser.givenName + "|" +
                                "email=" + gPlusUser.email + "|" +
                                "id=" + gPlusUser.id + "|");
                        authForm = new AuthFormValidation(gPlusUser.email, gPlusUser.givenName, gPlusUser.familyName,
                                null, null, gPlusUser.token);
                        getViewState().onAuthorizedGPlus(authForm);
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showLoadingIndicator(false);
                        if (e instanceof FacebookProviderException) {
                            switch (((FacebookProviderException) e).errorCode) {
                                case FACEBOOK_SDK_ERROR:
                                    fbProvider.logout();
                                    getViewState().onFacebookSdkError();
                                    break;
                                case PERMISSION_NOT_GRANTED:
                                    getViewState().onEmailPermissionNotGranted();
                                    break;
                            }
                        } else if (e instanceof GoogleProviderException) {
                            switch (((GoogleProviderException) e).errorCode) {
                                case GOOGLE_API_CONNECTING:
                                    getViewState().onGoogleApiConnectingError();
                                    break;
                                case GOOGLE_API_NOT_CONNECTED:
                                    getViewState().onGoogleApiNotConnectedError();
                                    break;
                                case GOOGLE_SDK_ERROR:
                                    gplusProvider.logout();
                                    getViewState().onGoogleSdkError();
                                    break;
                            }
                        } else {
                            gplusProvider.logout();
                        }
                    }
                });
    }

    public void performLogout() {
        getViewState().showLoadingIndicator(true);
        fbProvider.logout();
        googleLogoutSubscription = gplusProvider.logoutAsync()
                .onErrorResumeNext(Observable.just(null))
                .subscribe(v -> getViewState().showLoadingIndicator(false));
    }
}
