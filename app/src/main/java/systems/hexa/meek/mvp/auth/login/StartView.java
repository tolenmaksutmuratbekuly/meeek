package systems.hexa.meek.mvp.auth.login;

import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.BaseMvpView;

public interface StartView extends BaseMvpView {
    void onFacebookSdkError();

    void onEmailPermissionNotGranted();

    void onGoogleApiConnectingError();

    void onGoogleApiNotConnectedError();

    void onGoogleSdkError();

    void onRegistrationSuccess();

    void onAuthorizedGPlus(AuthFormValidation authForm);
}
