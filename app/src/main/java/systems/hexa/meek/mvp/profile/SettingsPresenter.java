package systems.hexa.meek.mvp.profile;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class SettingsPresenter implements MVPresenter<SettingsView> {
    private final DataLayer dataLayer;
    private SettingsView mvpView;

    public SettingsPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(SettingsView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }

    public void onDoneClick(String validation) {
        mvpView.openSelectLocationActivity();
    }
}
