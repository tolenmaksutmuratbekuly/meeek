package systems.hexa.meek.mvp.auth.signup;

import android.support.annotation.StringRes;

import systems.hexa.meek.mvp.BaseMvpView;

public interface SignUpFinishView extends BaseMvpView {
    void setEmail(String email);

    void setPwd(String pwd);

    void showEmailError(@StringRes int errorRes);

    void showConfirmEmailError(@StringRes int errorRes);

    void showPwdError(@StringRes int errorRes);

    void onRegistrationSuccess();
}
