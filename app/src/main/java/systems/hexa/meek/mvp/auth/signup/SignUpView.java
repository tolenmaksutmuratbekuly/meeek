package systems.hexa.meek.mvp.auth.signup;

import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.BaseMvpView;

public interface SignUpView extends BaseMvpView {
    void showFirstNameError(int errorRes);

    void showLastNameError(int errorRes);

    void setFirstName(String firstName);

    void setLastName(String lastName);

    void setAuthForm(AuthFormValidation form);
}
