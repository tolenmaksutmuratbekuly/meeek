package systems.hexa.meek.mvp.profile;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.MVPresenter;
import systems.hexa.meek.utils.ColorUtils;

public class QuestionsTabPresenter implements MVPresenter<QuestionsTabView> {
    private final DataLayer dataLayer;
    private QuestionsTabView mvpView;
    private Context context;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private List<QuestionsFeed> loadedItems;
    private boolean isLoading;


    public QuestionsTabPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(QuestionsTabView view) {
        mvpView = view;
        context = mvpView.getCtx();
        this.loadedItems = new ArrayList<>();
        reset();
    }

    private void reset() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        if (mvpView != null) {
            mvpView.showLoadingIndicator(isLoading);
        }
    }

    public void loadMoreItems() {
        setLoading(true);
//numPagesLoaded + 1, ITEMS_PER_PAGE

        // After request
        setLoading(false);
        numPagesLoaded = numPagesLoaded + 1;
        List<QuestionsFeed> questions = justTemplate();
        if (questions == null || questions.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(questions);
            if (mvpView != null) {
                mvpView.addQuestionListItems(questions);
                mvpView.hideListPlaceholder();
            }
        }
    }

    public void pullToRefreshItems() {
        setLoading(true);

        // AFter request
        setLoading(false);
        if (mvpView != null) {
            mvpView.showRefreshingIndicator(false);
        }

        reset();
        numPagesLoaded = 1;

        List<QuestionsFeed> questions = justTemplate();
        if (questions == null || questions.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(questions);
            if (mvpView != null) {
                mvpView.hideListPlaceholder();
                mvpView.addQuestionListItems(loadedItems);
            }
        }
    }

    private List<QuestionsFeed> justTemplate() {
        List<QuestionsFeed> questions = new ArrayList<>();
        QuestionsFeed questionsFeed = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_1));
        questions.add(questionsFeed);
        QuestionsFeed questionsFeed2 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_YEAH,
                ColorUtils.getGradient(context, ColorUtils.GR_2));
        questions.add(questionsFeed2);
        QuestionsFeed questionsFeed3 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_3));
        questions.add(questionsFeed3);
        QuestionsFeed questionsFeed4 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_4));
        questions.add(questionsFeed4);

        QuestionsFeed questionsFeed5 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_NOPE,
                ColorUtils.getGradient(context, ColorUtils.GR_5));
        questions.add(questionsFeed5);

        QuestionsFeed questionsFeed6 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_6));
        questions.add(questionsFeed6);

        QuestionsFeed questionsFeed7 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_7));
        questions.add(questionsFeed7);

        QuestionsFeed questionsFeed8 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_8));
        questions.add(questionsFeed8);

        QuestionsFeed questionsFeed9 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_NEUTRAL,
                ColorUtils.getGradient(context, ColorUtils.GR_9));
        questions.add(questionsFeed9);

        QuestionsFeed questionsFeed10 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_10));
        questions.add(questionsFeed10);

        QuestionsFeed questionsFeed11 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_11));
        questions.add(questionsFeed11);

        QuestionsFeed questionsFeed12 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_12));
        questions.add(questionsFeed12);

        QuestionsFeed questionsFeed13 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(context, ColorUtils.GR_13));
        questions.add(questionsFeed13);

        return questions;
    }

    private void checkIfEmpty() {
        if (loadedItems.isEmpty() && mvpView != null) {
            mvpView.showListPlaceholder(R.string.no_data);
        }
    }
}