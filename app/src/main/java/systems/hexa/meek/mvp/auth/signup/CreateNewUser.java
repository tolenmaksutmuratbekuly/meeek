package systems.hexa.meek.mvp.auth.signup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.Date;

import systems.hexa.meek.data.models.auth.Users;

import static systems.hexa.meek.utils.Constants.KEY_SEARCH_ATTRIBUTES;
import static systems.hexa.meek.utils.Constants.KEY_USERNAME;
import static systems.hexa.meek.utils.Constants.T_USERNAMES;
import static systems.hexa.meek.utils.Constants.T_USERS;
import static systems.hexa.meek.utils.DateUtils.BIRTHDATE_DISPLAY_FORMAT;
import static systems.hexa.meek.utils.DateUtils.getUsernameBirthdate;
import static systems.hexa.meek.utils.StringUtils.EMPTY_STRING;
import static systems.hexa.meek.utils.StringUtils.isStringOk;

public class CreateNewUser {
    private FirebaseDatabase firebaseDatabase;
    private Callback callback;

    public CreateNewUser(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public void createNewUser(String uniqueId, Users user, Callback callback) {
        this.callback = callback;
        // Create template user, for later updating
        DatabaseReference usersTable = firebaseDatabase.getReference(T_USERS).child(uniqueId);
        usersTable.setValue(user);

        // Generating unique username
        generateUsername(usersTable, user);
    }

    private String username;
    private long currNumberOfSameNameAndDob;

    private void generateUsername(DatabaseReference usersTable, Users user) {
        callback.showLoadingIndicator(true);
        String birthdayResult = EMPTY_STRING;
        try {
            if (isStringOk(user.date_of_birth)) {
                Date date = BIRTHDATE_DISPLAY_FORMAT.parse(user.date_of_birth);
                birthdayResult = getUsernameBirthdate(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        username = user.first_name.toLowerCase() + user.last_name.toLowerCase() + birthdayResult;
        DatabaseReference ref = firebaseDatabase.getReference().child(T_USERNAMES).child(username);
        currNumberOfSameNameAndDob = 0;
        ref.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                if (currentData.getValue() != null) {
                    currNumberOfSameNameAndDob = (long) (currentData.getValue());
                    currNumberOfSameNameAndDob = currNumberOfSameNameAndDob + 1;
                    currentData.setValue(currNumberOfSameNameAndDob);
                } else {
                    currentData.setValue(0);
                }

                username = currNumberOfSameNameAndDob == 0 ? username : username + "_" + ((int) currNumberOfSameNameAndDob);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                // Update username
                usersTable.child(KEY_USERNAME).setValue(username);

                // Update Search attributes
                String searchAttributes = user.first_name + " " + user.last_name + " " + username;
                usersTable.child(KEY_SEARCH_ATTRIBUTES).setValue(searchAttributes);

                callback.showLoadingIndicator(false);
                callback.onUserCreatedSuccess();
            }
        });
    }

    public void userAlreadyExists(String uniqueId, UserAlreadyRegistered callback) {
        DatabaseReference usersTable = firebaseDatabase.getReference(T_USERS);
        usersTable.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.userAlreadyExists(dataSnapshot.hasChild(uniqueId));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public interface Callback {
        void showLoadingIndicator(boolean show);

        void onUserCreatedSuccess();
    }

    public interface UserAlreadyRegistered {
        void userAlreadyExists(boolean userExist);
    }
}
