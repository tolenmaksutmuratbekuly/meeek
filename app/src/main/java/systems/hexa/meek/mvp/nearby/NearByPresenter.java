package systems.hexa.meek.mvp.nearby;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class NearByPresenter implements MVPresenter<NearByView> {
    private final DataLayer dataLayer;
    private NearByView mvpView;

    public NearByPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(NearByView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }

}
