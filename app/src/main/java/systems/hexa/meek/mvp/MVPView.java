package systems.hexa.meek.mvp;

public interface MVPView extends MVPErrorView {
    void showProgressView(boolean show);

    void showProgressViewMsg(boolean show, String msg);

    void showResultView(boolean success);

    void showResultViewMsg(boolean success, String msg);

    void showProgressDialog();

    void dismissProgressDialog();
}