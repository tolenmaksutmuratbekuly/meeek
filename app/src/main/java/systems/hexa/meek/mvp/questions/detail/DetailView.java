package systems.hexa.meek.mvp.questions.detail;

import android.content.Context;

import java.util.List;

public interface DetailView {
    void showLoadingIndicator(boolean show);

    void finish();

    Context getCtx();

    void setCardsList(List<String> mData);
}
