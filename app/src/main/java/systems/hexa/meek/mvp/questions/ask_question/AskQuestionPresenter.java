package systems.hexa.meek.mvp.questions.ask_question;

import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.questions.new_question.GradientColor;
import systems.hexa.meek.mvp.MVPresenter;
import systems.hexa.meek.utils.ColorUtils;

import static android.text.TextUtils.isEmpty;

public class AskQuestionPresenter implements MVPresenter<AskQuestionView> {
    private final DataLayer dataLayer;
    private AskQuestionView mvpView;

    public AskQuestionPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(AskQuestionView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }

    public void loadColors() {
        List<GradientColor> colors = new ArrayList<>();
        for (int i = 0; i < ColorUtils.GRADIENT_COLORS.length; i++) {
            colors.add(new GradientColor(ColorUtils.GRADIENT_COLORS[i]));
        }
        mvpView.addColors(colors);
    }

    public void onDoneClick(String validation) {
        if (isEmpty(validation.trim())) {
            mvpView.showQuestionInputError();
        } else {
            mvpView.openSelectLocationActivity();
        }
    }
}
