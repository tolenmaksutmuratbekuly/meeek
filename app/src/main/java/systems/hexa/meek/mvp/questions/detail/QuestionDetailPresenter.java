package systems.hexa.meek.mvp.questions.detail;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class QuestionDetailPresenter implements MVPresenter<QuestionDetailView> {
    private final DataLayer dataLayer;
    private QuestionDetailView mvpView;

    public QuestionDetailPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(QuestionDetailView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }

}
