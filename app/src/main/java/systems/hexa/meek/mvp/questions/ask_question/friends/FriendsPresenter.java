package systems.hexa.meek.mvp.questions.ask_question.friends;

import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.questions.new_question.Friends;
import systems.hexa.meek.mvp.MVPresenter;

public class FriendsPresenter implements MVPresenter<FriendsView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;

    private FriendsView mvpView;
    //    private ErrorHandler errorHandler;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private List<Friends> loadedItems;
    private boolean isLoading;

    public FriendsPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.loadedItems = new ArrayList<>();
        reset();
    }

    private void reset() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(FriendsView view) {
        mvpView = view;
        mvpView.showLoadingIndicator(isLoading);
//        if (noMoreItems) {
//            checkIfEmpty();
//        } else {
//            if (numPagesLoaded == 0) {
//                loadMoreItems();
//            }
//        }
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        if (mvpView != null) {
            mvpView.showLoadingIndicator(isLoading);
        }
    }

    public void loadMoreItems() {
        setLoading(true);
//numPagesLoaded + 1, ITEMS_PER_PAGE

        // After request
        setLoading(false);
        numPagesLoaded = numPagesLoaded + 1;
        List<Friends> friends = justTemplate();
        if (friends == null || friends.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(friends);
            if (mvpView != null) {
                mvpView.addFriendsListItems(friends);
                mvpView.hideListPlaceholder();
            }
        }
    }

    private List<Friends> justTemplate() {
        List<Friends> list = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            Friends f1 = new Friends("Jason_" + i, "Statham_" + i, "@statham_" + i, false);
            list.add(f1);
        }

        return list;
    }

    public void pullToRefreshItems() {
        setLoading(true);

        // AFter request
        setLoading(false);
        if (mvpView != null) {
            mvpView.showRefreshingIndicator(false);
        }

        reset();
        numPagesLoaded = 1;

        List<Friends> friends = justTemplate();
        if (friends == null || friends.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(friends);
            if (mvpView != null) {
                mvpView.hideListPlaceholder();
                mvpView.addFriendsListItems(loadedItems);
            }
        }
    }

}
