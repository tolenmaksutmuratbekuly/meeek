package systems.hexa.meek.mvp.auth.signup;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.kelvinapps.rxfirebase.RxFirebaseAuth;

import rx.Subscriber;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.Users;

import static systems.hexa.meek.utils.DateUtils.BIRTHDATE_DISPLAY_FORMAT;
import static systems.hexa.meek.utils.StringUtils.trim;

@InjectViewState
public class SignUpFinishPresenter extends MvpPresenter<SignUpFinishView> {
    private DataLayer dataLayer;
    private AuthFormValidation authForm;
    private CreateNewUser createNewUser;
    private String confirmEmail;

    public void init(DataLayer dataLayer, AuthFormValidation form) {
        this.dataLayer = dataLayer;
        authForm = form;
        if (authForm != null) {
            if (form.email != null) {
                getViewState().setEmail(form.email);
            }

            if (form.pwd != null) {
                getViewState().setPwd(form.pwd);
            }
        }

        createNewUser = new CreateNewUser(dataLayer.getFirebaseDatabase());
    }

    public void setEmail(String email) {
        authForm.email = trim(email);
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public void setPwd(String pwd) {
        authForm.pwd = trim(pwd);
    }

    public void validate() {
        boolean ok;
        ok = validateEmail();
        ok &= validateConfirmEmail();
        ok &= validatePwd();
        if (ok) {
            RxFirebaseAuth.createUserWithEmailAndPassword(dataLayer.getFirebaseAuth(), authForm.email, authForm.pwd)
                    .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                    .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                    .subscribe(new Subscriber<AuthResult>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            getViewState().showRequestError(e.getMessage());
                        }

                        @Override
                        public void onNext(AuthResult task) {
                            if (task.getUser() != null) {
                                FirebaseUser user = task.getUser();
                                String uniqueId = user.getUid();
                                createNewUser.createNewUser(uniqueId, new Users(authForm.firstName, authForm.lastName,
                                        authForm.firstName + " " + authForm.lastName, authForm.email,
                                        authForm.firstName + authForm.lastName,
                                        authForm.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT),
                                        authForm.gender + ""), new CreateNewUser.Callback() {
                                    @Override
                                    public void showLoadingIndicator(boolean show) {
                                        getViewState().showLoadingIndicator(show);
                                    }

                                    @Override
                                    public void onUserCreatedSuccess() {
                                        // Save current userId locally
                                        dataLayer.getPreferences().setUserId(uniqueId);
                                        getViewState().onRegistrationSuccess();
                                    }
                                });
                            }
                        }
                    });
        }
    }

    // Validation
    private boolean validateEmail() {
        int errorRes = authForm.validateEmail();
        if (errorRes != 0) {
            getViewState().showEmailError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateConfirmEmail() {
        int errorRes = authForm.validateConfirmEmail(confirmEmail);
        if (errorRes != 0) {
            getViewState().showConfirmEmailError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validatePwd() {
        int errorRes = authForm.validatePwd();
        if (errorRes != 0) {
            getViewState().showPwdError(errorRes);
        }
        return errorRes == 0;
    }
}
