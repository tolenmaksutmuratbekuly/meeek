package systems.hexa.meek.mvp.profile;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class ProfilePresenter implements MVPresenter<ProfileView> {
    private final DataLayer dataLayer;
    private ProfileView mvpView;

    public ProfilePresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(ProfileView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }

    public void onDoneClick(String validation) {
        mvpView.openSelectLocationActivity();
    }
}
