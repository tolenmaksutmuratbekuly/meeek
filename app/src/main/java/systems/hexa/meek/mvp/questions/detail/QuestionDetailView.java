package systems.hexa.meek.mvp.questions.detail;


import systems.hexa.meek.mvp.MVPView;

public interface QuestionDetailView extends MVPView {
    void showLoadingIndicator(boolean show);

    void openMainScreen();

    void finish();
}
