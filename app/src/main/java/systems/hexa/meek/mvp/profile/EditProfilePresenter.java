package systems.hexa.meek.mvp.profile;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class EditProfilePresenter implements MVPresenter<EditProfileView> {
    private final DataLayer dataLayer;
    private EditProfileView mvpView;

    public EditProfilePresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(EditProfileView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }
}
