package systems.hexa.meek.mvp.questions.ask_question;


import java.util.List;

import systems.hexa.meek.data.models.questions.new_question.GradientColor;
import systems.hexa.meek.mvp.MVPView;

public interface AskQuestionView extends MVPView {
    void showLoadingIndicator(boolean show);

    void finish();

    void addColors(List<GradientColor> colorList);

    void showQuestionInputError();

    void openSelectLocationActivity();
}
