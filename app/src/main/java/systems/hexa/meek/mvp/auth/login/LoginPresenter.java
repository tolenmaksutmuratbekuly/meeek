package systems.hexa.meek.mvp.auth.login;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import org.parceler.Parcels;

import rx.Subscriber;
import rx.Subscription;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.data.params.auth.AuthEmailPwdParams;
import systems.hexa.meek.data.repository.auth.AuthRepositoryProvider;
import systems.hexa.meek.mvp.BaseMvpPresenter;
import systems.hexa.meek.utils.Constants;

import static systems.hexa.meek.utils.StringUtils.trim;

@InjectViewState
public class LoginPresenter extends BaseMvpPresenter<LoginView> {
    private DataLayer dataLayer;
    private AuthFormValidation form;

    public void init(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        if (form == null) {
            throw new NullPointerException("form == null. Did you forget to call onRestoreInstanceState() ?");
        } else {
            getViewState().setEmail(form.email);
            getViewState().setPwd(form.pwd);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putParcelable(Constants.FORM_KEY, Parcels.wrap(form));
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(Constants.FORM_KEY)) {
            form = Parcels.unwrap(savedState.getParcelable(Constants.FORM_KEY));
        } else {
            form = new AuthFormValidation();
        }
    }

    public void setEmail(String email) {
        form.email = trim(email);
    }

    public void setPwd(String pwd) {
        form.pwd = trim(pwd);
    }

    public void signInWithEmailAndPassword() {
        if (validateEmail() && validatePwd()) {
            Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                    .signInWithEmailAndPassword(new AuthEmailPwdParams(form.email, form.pwd))
                    .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                    .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                    .subscribe(new Subscriber<User>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            handleResponseError(context, e);
                        }

                        @Override
                        public void onNext(User user) {
                            if (user != null) {
                                dataLayer.getPreferences().setUserId(user.uid);
                                getViewState().onSuccessAuthorized(user);
                            }
                        }
                    });
            compositeSubscription.add(subscription);
        }
    }

    private boolean validateEmail() {
        int errorRes = form.validateEmail();
        if (errorRes != 0) {
            getViewState().showEmailError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validatePwd() {
        int errorRes = form.validatePwd();
        if (errorRes != 0) {
            getViewState().showPwdError(errorRes);
        }
        return errorRes == 0;
    }
}