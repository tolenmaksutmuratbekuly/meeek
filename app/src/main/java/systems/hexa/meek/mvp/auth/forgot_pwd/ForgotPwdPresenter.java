package systems.hexa.meek.mvp.auth.forgot_pwd;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import org.parceler.Parcels;

import systems.hexa.meek.data.models.auth.AuthFormValidation;

import static systems.hexa.meek.utils.Constants.FORM_KEY;

@InjectViewState
public class ForgotPwdPresenter extends MvpPresenter<ForgotPwdView> {
    private AuthFormValidation form;

    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putParcelable(FORM_KEY, Parcels.wrap(form));
        }
    }

    public void onRestoreInstanceState(Bundle savedState) {
        if (savedState != null && savedState.containsKey(FORM_KEY)) {
            form = Parcels.unwrap(savedState.getParcelable(FORM_KEY));
        } else {
            form = new AuthFormValidation();
        }
    }

    public void setAuthForm(AuthFormValidation form) {
        this.form = form;
    }

    public AuthFormValidation getAuthForm() {
        if (form == null) {
            form = new AuthFormValidation();
        }
        return form;
    }
}
