package systems.hexa.meek.mvp.auth.forgot_pwd;

import systems.hexa.meek.mvp.BaseMvpView;

public interface NewPwdView extends BaseMvpView {

    void showNewPwdError(int errorRes);

    void showConfirmPwdError(int errorRes);

    void setNewPwd(String firstName);

    void setConfirmPwd(String lastName);
}
