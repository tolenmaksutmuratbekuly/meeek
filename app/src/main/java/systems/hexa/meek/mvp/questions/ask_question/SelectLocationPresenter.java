package systems.hexa.meek.mvp.questions.ask_question;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class SelectLocationPresenter implements MVPresenter<SelectLocationView> {
    private final DataLayer dataLayer;
    private SelectLocationView mvpView;

    public SelectLocationPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(SelectLocationView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }
}
