package systems.hexa.meek.mvp.auth.forgot_pwd;

import com.arellomobile.mvp.InjectViewState;

import rx.Subscriber;
import rx.Subscription;
import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.ApiResultSuccess;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.repository.auth.AuthRepositoryProvider;
import systems.hexa.meek.mvp.BaseMvpPresenter;

import static systems.hexa.meek.utils.StringUtils.length;
import static systems.hexa.meek.utils.StringUtils.trim;

@InjectViewState
public class ForgotPresenter extends BaseMvpPresenter<ForgotView> {
    private AuthFormValidation authForm;

    public void init(DataLayer dataLayer, AuthFormValidation form) {
        authForm = form;
        this.dataLayer = dataLayer;
        if (authForm != null) {
            if (length(authForm.email) > 0)
                getViewState().setEmail(form.email);
        }
    }

    public void setEmail(String email) {
        authForm.email = trim(email);
    }

    public void validation() {
        if (validateEmail()) {
            Subscription subscription = AuthRepositoryProvider.provideRepository(dataLayer.getApi())
                    .resetPassword(authForm.email)
                    .doOnSubscribe(() -> getViewState().showLoadingIndicator(true))
                    .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                    .subscribe(new Subscriber<ApiResultSuccess>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            handleResponseError(context, e);
                        }

                        @Override
                        public void onNext(ApiResultSuccess response) {
                            if (response.success) {
                                getViewState().successSent();
                            } else {
                                getViewState().showRequestError(context.getString(R.string.err_retry_please));
                            }
                        }
                    });
            compositeSubscription.add(subscription);
        }
    }

    private boolean validateEmail() {
        int errorRes = authForm.validateEmail();
        if (errorRes != 0) {
            getViewState().showEmailError(errorRes);
        }
        return errorRes == 0;
    }
}
