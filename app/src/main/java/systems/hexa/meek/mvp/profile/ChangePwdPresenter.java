package systems.hexa.meek.mvp.profile;

import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class ChangePwdPresenter implements MVPresenter<ChangePwdView> {
    private final DataLayer dataLayer;
    private ChangePwdView mvpView;

    public ChangePwdPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(ChangePwdView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

//    public void setErrorHandler(ErrorHandler errorHandler) {
//        this.errorHandler = errorHandler;
//    }
}
