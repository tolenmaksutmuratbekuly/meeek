package systems.hexa.meek.mvp.questions;


import android.content.Context;
import android.support.annotation.StringRes;

import java.util.List;

import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.MVPView;

public interface QuestionView extends MVPView {
    Context getContext();

    void showLoadingIndicator(boolean show);

    void showRefreshingIndicator(boolean show);

    void addQuestionListItems(List<QuestionsFeed> promotions);

    void showListPlaceholder(@StringRes int textRes);

    void hideListPlaceholder();

    void showSplashScreen(int type, String description);

    void setFloatButton(int type);
}
