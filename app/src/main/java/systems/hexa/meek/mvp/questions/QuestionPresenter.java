package systems.hexa.meek.mvp.questions;


import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.MVPresenter;
import systems.hexa.meek.utils.ColorUtils;

import static systems.hexa.meek.utils.Constants.TYPE;
import static systems.hexa.meek.utils.Constants.TYPE_FRIEND;

public class QuestionPresenter implements MVPresenter<QuestionView> {
    private static final int ITEMS_PER_PAGE = 10;

    private final DataLayer dataLayer;
    private QuestionView mvpView;
    private int numPagesLoaded;
    private boolean noMoreItems;
    private List<QuestionsFeed> loadedItems;
    private boolean isLoading;

    public QuestionPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.loadedItems = new ArrayList<>();
        reset();
    }

    private void reset() {
        numPagesLoaded = 0;
        noMoreItems = false;
        loadedItems.clear();
    }

    @Override
    public void attachView(QuestionView view) {
        mvpView = view;
        mvpView.showLoadingIndicator(isLoading);
//        if (noMoreItems) {
//            checkIfEmpty();
//        } else {
//            if (numPagesLoaded == 0) {
//                loadMoreItems();
//            }
//        }
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        if (mvpView != null) {
            mvpView.showLoadingIndicator(isLoading);
        }
    }

    public void loadMoreItems() {
        setLoading(true);
//numPagesLoaded + 1, ITEMS_PER_PAGE

        // After request
        setLoading(false);
        numPagesLoaded = numPagesLoaded + 1;
        List<QuestionsFeed> questions = justTemplate();
        if (questions == null || questions.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(questions);
            if (mvpView != null) {
                mvpView.addQuestionListItems(questions);
                mvpView.hideListPlaceholder();
            }
        }
    }

    public void pullToRefreshItems() {
        setLoading(true);

        // After request
        setLoading(false);
        if (mvpView != null) {
            mvpView.showRefreshingIndicator(false);
        }

        reset();
        numPagesLoaded = 1;

        List<QuestionsFeed> questions = justTemplate();
        if (questions == null || questions.isEmpty()) {
            noMoreItems = true;
        } else {
            loadedItems.addAll(questions);
            if (mvpView != null) {
                mvpView.hideListPlaceholder();
                mvpView.addQuestionListItems(loadedItems);
            }
        }
    }

    private List<QuestionsFeed> justTemplate() {
        List<QuestionsFeed> questions = new ArrayList<>();
        QuestionsFeed questionsFeed = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_1));
        questions.add(questionsFeed);
        QuestionsFeed questionsFeed2 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_YEAH,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_2));
        questions.add(questionsFeed2);
        QuestionsFeed questionsFeed3 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_3));
        questions.add(questionsFeed3);
        QuestionsFeed questionsFeed4 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_4));
        questions.add(questionsFeed4);

        QuestionsFeed questionsFeed5 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_NOPE,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_5));
        questions.add(questionsFeed5);

        QuestionsFeed questionsFeed6 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_6));
        questions.add(questionsFeed6);

        QuestionsFeed questionsFeed7 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_7));
        questions.add(questionsFeed7);

        QuestionsFeed questionsFeed8 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_8));
        questions.add(questionsFeed8);

        QuestionsFeed questionsFeed9 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_NEUTRAL,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_9));
        questions.add(questionsFeed9);

        QuestionsFeed questionsFeed10 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_10));
        questions.add(questionsFeed10);

        QuestionsFeed questionsFeed11 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_11));
        questions.add(questionsFeed11);

        QuestionsFeed questionsFeed12 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_12));
        questions.add(questionsFeed12);

        QuestionsFeed questionsFeed13 = new QuestionsFeed(QuestionsFeed.TYPE_ANSWER_DEFAULT,
                ColorUtils.getGradient(mvpView.getContext(), ColorUtils.GR_13));
        questions.add(questionsFeed13);

        return questions;
    }

    private void checkIfEmpty() {
        if (loadedItems.isEmpty() && mvpView != null) {
            mvpView.showListPlaceholder(R.string.no_data);
        }
    }

    private void handleLoadError(Throwable e) {
        if (mvpView != null) {
            mvpView.showLoadingIndicator(false);
            mvpView.showRefreshingIndicator(false);
        }
    }

    public void showSplashScreen(int type, String description) {
        if (type != 0) {
            mvpView.setFloatButton(type);
            if (type != TYPE_FRIEND && !dataLayer.getPreferences().hasShownSplashScreen(TYPE + "_" + type)) {
                mvpView.showSplashScreen(type, description);
                dataLayer.getPreferences().setShownSplashScreen(true, TYPE + "_" + type);
            }
        }
    }
}
