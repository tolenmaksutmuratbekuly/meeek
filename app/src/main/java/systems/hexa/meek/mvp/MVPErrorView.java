package systems.hexa.meek.mvp;

import systems.hexa.meek.data.network.exceptions.APIException;
import systems.hexa.meek.data.network.exceptions.ConnectionTimeOutException;
import systems.hexa.meek.data.network.exceptions.UnknownException;

public interface MVPErrorView {

    void handleAPIException(APIException e);

    void handleConnectionTimeOutException(ConnectionTimeOutException e);

    void handleUnknownException(UnknownException e);

    void showErrorDialog(String message);

    String getUnknownExceptionMessage();

    String getConnectionTimeOutExceptionMessage();

}