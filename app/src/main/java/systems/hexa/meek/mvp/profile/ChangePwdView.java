package systems.hexa.meek.mvp.profile;


import systems.hexa.meek.mvp.MVPView;

public interface ChangePwdView extends MVPView {
    void showLoadingIndicator(boolean show);

    void finish();
}
