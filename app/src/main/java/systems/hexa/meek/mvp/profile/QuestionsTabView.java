package systems.hexa.meek.mvp.profile;

import android.content.Context;
import android.support.annotation.StringRes;

import java.util.List;

import systems.hexa.meek.data.models.questions.QuestionsFeed;

public interface QuestionsTabView {
    void showLoadingIndicator(boolean show);

    void finish();

    Context getCtx();

    void showRefreshingIndicator(boolean show);

    void addQuestionListItems(List<QuestionsFeed> questions);

    void showListPlaceholder(@StringRes int textRes);

    void hideListPlaceholder();

}
