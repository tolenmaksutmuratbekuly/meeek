package systems.hexa.meek.mvp.auth.forgot_pwd;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class RecoveryPresenter extends MvpPresenter<RecoverySentView> {
    public void init() {
    }
}
