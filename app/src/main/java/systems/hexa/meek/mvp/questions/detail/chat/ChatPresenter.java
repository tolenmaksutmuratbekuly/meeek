package systems.hexa.meek.mvp.questions.detail.chat;


import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.questions.detail.chat.ChatMessage;
import systems.hexa.meek.mvp.MVPresenter;
import systems.hexa.meek.utils.StringUtils;

public class ChatPresenter implements MVPresenter<ChatView> {
    private final DataLayer dataLayer;
    private ChatView mvpView;
    private List<ChatMessage> loadedItems;

    public ChatPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.loadedItems = new ArrayList<>();
    }

    @Override
    public void attachView(ChatView view) {
        mvpView = view;
    }

    @Override
    public void detachView() {
        mvpView = null;
    }

    public void onDestroy() {
//        if (questionSubscription != null) {
//            questionSubscription.unsubscribe();
//            questionSubscription = null;
//        }
    }

    public void loadMoreItems() {
        List<ChatMessage> messages = justTemplate();
        loadedItems.addAll(messages);
        if (mvpView != null) {
            mvpView.addMessages(messages);
            mvpView.hideListPlaceholder();
        }
    }

    private List<ChatMessage> justTemplate() {
        List<ChatMessage> chatMessageList = new ArrayList<>();

        ChatMessage chatMessage1 = new ChatMessage("sadklasjdl", 1);
        chatMessageList.add(chatMessage1);

        ChatMessage chatMessage2 = new ChatMessage("sadklasjdasdasQasl", 2);
        chatMessageList.add(chatMessage2);

        ChatMessage chatMessage3 = new ChatMessage("sadklasjdasdasQasl", 3);
        chatMessageList.add(chatMessage3);

        ChatMessage chatMessage4 = new ChatMessage("sadklasjdasdasQasl", 4);
        chatMessageList.add(chatMessage4);

        ChatMessage chatMessage5 = new ChatMessage("sadklasjdasdasQasl", ChatMessage.LOCAL_USER_ID);
        chatMessageList.add(chatMessage5);

        ChatMessage chatMessage6 = new ChatMessage("sadklasjdasdasQasl", 6);
        chatMessageList.add(chatMessage6);

        return chatMessageList;
    }

    private void checkIfEmpty() {
        if (loadedItems.isEmpty() && mvpView != null) {
            mvpView.showListPlaceholder(R.string.no_data);
        }
    }

    public void onMessageSendClick(String msg) {
        if (StringUtils.length(msg) > 0) {
            List<ChatMessage> chatMessageList = new ArrayList<>();
            ChatMessage chatMessage5 = new ChatMessage(msg, ChatMessage.LOCAL_USER_ID);
            chatMessageList.add(chatMessage5);
            if (mvpView != null) {
                mvpView.addMessages(chatMessageList);
            }
        }
    }
}
