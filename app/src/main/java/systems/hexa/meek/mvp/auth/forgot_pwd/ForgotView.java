package systems.hexa.meek.mvp.auth.forgot_pwd;

import android.support.annotation.StringRes;

import systems.hexa.meek.mvp.BaseMvpView;

public interface ForgotView extends BaseMvpView {
    void setEmail(String email);

    void showEmailError(@StringRes int errorRes);

    void successSent();
}
