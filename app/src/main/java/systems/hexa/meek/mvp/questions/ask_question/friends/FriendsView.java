package systems.hexa.meek.mvp.questions.ask_question.friends;


import android.support.annotation.StringRes;

import java.util.List;

import systems.hexa.meek.data.models.questions.new_question.Friends;
import systems.hexa.meek.mvp.MVPView;

public interface FriendsView extends MVPView {
    void showLoadingIndicator(boolean show);

    void showRefreshingIndicator(boolean show);

    void addFriendsListItems(List<Friends> promotions);

    void showListPlaceholder(@StringRes int textRes);

    void hideListPlaceholder();

}
