package systems.hexa.meek.mvp.auth.signup;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.kelvinapps.rxfirebase.RxFirebaseAuth;

import java.util.Date;

import rx.Subscriber;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.Gender;
import systems.hexa.meek.data.models.auth.Users;

import static systems.hexa.meek.utils.DateUtils.BIRTHDATE_DISPLAY_FORMAT;
import static systems.hexa.meek.utils.StringUtils.length;

@InjectViewState
public class SignUpDetailPresenter extends MvpPresenter<SignUpDetailView> {
    private DataLayer dataLayer;
    private AuthFormValidation authForm;
    private boolean isGPlusAuth;
    private CreateNewUser createNewUser;

    public void init(DataLayer dataLayer, AuthFormValidation form, boolean isGPlusAuth) {
        this.dataLayer = dataLayer;
        authForm = form;
        this.isGPlusAuth = isGPlusAuth;
        createNewUser = new CreateNewUser(dataLayer.getFirebaseDatabase());
        if (isGPlusAuth) {
            getViewState().setFinishTextInBtn();
        }
        if (authForm != null) {
            if (length(authForm.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT)) > 0) {
                getViewState().setBirthdate(authForm.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
            }

            if (authForm.gender != null) {
                getViewState().setGender(authForm.gender);
            }
        }
    }

    public void setBirthdate(Date birthdate) {
        authForm.birthdate = birthdate;
        getViewState().setBirthdate(authForm.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT));
    }

    public void setGender(Gender gender) {
        authForm.gender = gender;
    }

    public void checkGeneralInfo() {
        if (validateBirthdate() && validateGender()) {
            if (isGPlusAuth) {
                signInWithCredentials(new Users(authForm.firstName, authForm.lastName,
                        authForm.firstName + " " + authForm.lastName, authForm.email,
                        authForm.firstName + authForm.lastName,
                        authForm.getBirthdateStr(BIRTHDATE_DISPLAY_FORMAT),
                        authForm.gender + ""), authForm.getGPlusToken());
            } else {
                getViewState().setAuthForm(authForm);
            }
        }
    }

    private void signInWithCredentials(Users user, String token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        RxFirebaseAuth.signInWithCredential(dataLayer.getFirebaseAuth(), credential)
                .doOnTerminate(() -> getViewState().showLoadingIndicator(false))
                .subscribe(new Subscriber<AuthResult>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showRequestError(e.getMessage());
                    }

                    @Override
                    public void onNext(AuthResult task) {
                        if (task.getUser() != null) {
                            FirebaseUser firebaseUser = task.getUser();
                            String uniqueId = firebaseUser.getUid();
                            createNewUser.userAlreadyExists(uniqueId, userExist -> {
                                if (userExist) {
                                    onRegisteredSuccess(uniqueId);
                                } else {
                                    createNewUser.createNewUser(uniqueId, user, new CreateNewUser.Callback() {
                                        @Override
                                        public void showLoadingIndicator(boolean show) {
                                            getViewState().showLoadingIndicator(show);
                                        }

                                        @Override
                                        public void onUserCreatedSuccess() {
                                            onRegisteredSuccess(uniqueId);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
    }

    private void onRegisteredSuccess(String uniqueId) {
        // Save current userId locally
        dataLayer.getPreferences().setUserId(uniqueId);
        getViewState().onRegistrationSuccess();
    }

    public void showDatePickerDialog() {
        getViewState().showDatePickerDialog(authForm.birthdate);
    }

    private boolean validateBirthdate() {
        int errorRes = authForm.validateBirthdate();
        if (errorRes != 0) {
            getViewState().showBirthDateError(errorRes);
        }
        return errorRes == 0;
    }

    private boolean validateGender() {
        int errorRes = authForm.validateGender();
        if (errorRes != 0) {
            getViewState().showGenderError(errorRes);
        }
        return errorRes == 0;
    }
}
