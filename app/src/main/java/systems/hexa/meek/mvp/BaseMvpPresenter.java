package systems.hexa.meek.mvp;

import android.content.Context;

import com.arellomobile.mvp.MvpPresenter;

import rx.subscriptions.CompositeSubscription;
import systems.hexa.meek.R;
import systems.hexa.meek.config.DebugConfig;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.network.error.RetrofitErrorHandler;
import systems.hexa.meek.data.network.exceptions.APIException;
import systems.hexa.meek.data.network.exceptions.ConnectionTimeOutException;
import systems.hexa.meek.data.network.exceptions.UnknownException;
import systems.hexa.meek.utils.StringUtils;

public abstract class BaseMvpPresenter<T extends BaseMvpView> extends MvpPresenter<T> {
    protected Context context;
    protected DataLayer dataLayer;
    protected CompositeSubscription compositeSubscription = new CompositeSubscription();

    protected void handleResponseError(Context context, Throwable e) {
        if (DebugConfig.DEV_BUILD) {
            e.printStackTrace();
        }
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (APIException e1) {
            getViewState().showRequestError(StringUtils.replaceNull(e1.getErrorMessage()));
        } catch (UnknownException e1) {
            getViewState().showRequestError(context.getString(R.string.err_generic_request));
        } catch (ConnectionTimeOutException e1) {
            getViewState().showRequestError(context.getString(R.string.err_generic_network));
        }
    }

    protected void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}
