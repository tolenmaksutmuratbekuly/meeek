package systems.hexa.meek.mvp.questions.detail;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.MVPresenter;

public class ResultPresenter implements MVPresenter<ResultView> {
    private final DataLayer dataLayer;
    private ResultView mvpView;
    private Context context;

    public ResultPresenter(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    @Override
    public void attachView(ResultView view) {
        mvpView = view;
        context = mvpView.getCtx();
    }

    @Override
    public void detachView() {
        mvpView = null;
    }


    public void fillWithTestData() {
        List<String> mData = new ArrayList<>();
        for (int x = 0; x < 10; x++) {
            mData.add((x + 1) + " " + context.getString(R.string.lorem_ipsum_long));
        }

        mvpView.setCardsList(mData);
    }
}
