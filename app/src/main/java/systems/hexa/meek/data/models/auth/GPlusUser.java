package systems.hexa.meek.data.models.auth;

public class GPlusUser {
    public String token;
    public String displayName;
    public String familyName;
    public String givenName;
    public String email;
    public String id;

    public GPlusUser(String token, String displayName, String familyName, String givenName, String email, String id) {
        this.token = token;
        this.displayName = displayName;
        this.familyName = familyName;
        this.givenName = givenName;
        this.email = email;
        this.id = id;
    }
}
