package systems.hexa.meek.data.social;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.Observable;

public interface ShareProvider {
    Observable<Object> share(@NonNull Uri url, @Nullable Uri imageUrl, @Nullable String text);
}
