package systems.hexa.meek.data.network.api.meek;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;
import systems.hexa.meek.data.models.APIResponse;
import systems.hexa.meek.data.models.ApiResultSuccess;
import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.data.params.auth.AuthEmailPwdParams;

public interface API {

    /****** Authorization *******/
    @POST("signInWithEmailAndPassword/")
    Observable<APIResponse<User>> signInWithEmailAndPassword(@Body AuthEmailPwdParams params);

    @FormUrlEncoded
    @POST("resetPassword/")
    Observable<APIResponse<ApiResultSuccess>> resetPassword(@Field("email") String email);

    /**
     * REGISTRATION
     */
    @POST("user/registrationStart")
    Observable<ResponseBody> registrationStart(@Body Object params);

}