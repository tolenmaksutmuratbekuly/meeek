package systems.hexa.meek.data.models.questions.detail.chat;

public class ChatMessage {
    public static final int LOCAL_USER_ID = 123142;
    public String message;
    public int userId;

    public ChatMessage(String message, int userId) {
        this.message = message;
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public int getUserId() {
        return userId;
    }
}
