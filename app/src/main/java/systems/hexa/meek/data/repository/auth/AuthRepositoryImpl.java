package systems.hexa.meek.data.repository.auth;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import systems.hexa.meek.data.models.ApiResultSuccess;
import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.data.network.api.meek.API;
import systems.hexa.meek.data.params.auth.AuthEmailPwdParams;
import systems.hexa.meek.utils.rx.Transformers;

class AuthRepositoryImpl implements AuthRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<User> signInWithEmailAndPassword(AuthEmailPwdParams params) {
        return api
                .signInWithEmailAndPassword(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(Transformers.<User>responseDataExtractor());
    }

    @Override
    public Observable<ApiResultSuccess> resetPassword(String email) {
        return api
                .resetPassword(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(Transformers.<ApiResultSuccess>responseDataExtractor());
    }
}