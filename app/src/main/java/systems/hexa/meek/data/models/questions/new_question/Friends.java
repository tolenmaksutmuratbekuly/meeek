package systems.hexa.meek.data.models.questions.new_question;

public class Friends {
    private String name;
    private String surname;
    private String username;
    private boolean checked;

    public Friends(String name, String surname, String username, boolean checked) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
