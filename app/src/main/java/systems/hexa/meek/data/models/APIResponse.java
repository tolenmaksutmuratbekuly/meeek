package systems.hexa.meek.data.models;

import systems.hexa.meek.data.network.exceptions.APIException;

public class APIResponse<T> {
    public T data;

    public T extractData() throws APIException {
        return data;
    }
}
