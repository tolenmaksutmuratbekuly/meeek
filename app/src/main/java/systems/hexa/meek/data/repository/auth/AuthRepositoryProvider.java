package systems.hexa.meek.data.repository.auth;

import android.support.annotation.NonNull;

import systems.hexa.meek.data.network.api.meek.API;


public class AuthRepositoryProvider {
    private static AuthRepository repository;

    @NonNull
    public static AuthRepository provideRepository(API api) {
        if (repository == null) {
            repository = new AuthRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(AuthRepository repo) {
        repository = repo;
    }
}