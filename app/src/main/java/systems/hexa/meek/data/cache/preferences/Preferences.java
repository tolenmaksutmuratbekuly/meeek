package systems.hexa.meek.data.cache.preferences;

public interface Preferences {
    boolean isAuthenticated();

    void setUserId(String userId);

    boolean hasShownSplashScreen(String key);

    void setShownSplashScreen(boolean shown, String key);
}
