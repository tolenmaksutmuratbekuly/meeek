package systems.hexa.meek.data.models.auth;

import java.text.ParseException;
import java.util.Date;

import static systems.hexa.meek.utils.DateUtils.BIRTHDATE_FACEBOOK;
import static systems.hexa.meek.utils.DateUtils.getBirthdateStr;
import static systems.hexa.meek.utils.StringUtils.EMPTY_STRING;
import static systems.hexa.meek.utils.StringUtils.isStringOk;

public class FBUser {
    public String id;
    public String name;
    public String birthday;
    public String email;
    public String first_name;
    public String gender;
    public String last_name;

    public Gender getGender() {
        if (gender != null)
            return Gender.fromString(gender);
        else return null;
    }

    public String getBirthday() {
        String birthdayResult = EMPTY_STRING;
        try {
            if (isStringOk(birthday)) {
                Date date = BIRTHDATE_FACEBOOK.parse(birthday);
                birthdayResult = getBirthdateStr(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return birthdayResult;
    }
}
