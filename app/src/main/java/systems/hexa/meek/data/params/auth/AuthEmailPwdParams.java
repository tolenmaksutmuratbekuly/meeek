package systems.hexa.meek.data.params.auth;

public class AuthEmailPwdParams {
    public String email;
    public String password;

    public AuthEmailPwdParams(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
