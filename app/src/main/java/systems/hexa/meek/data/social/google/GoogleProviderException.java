package systems.hexa.meek.data.social.google;

import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.Status;

import static systems.hexa.meek.data.social.google.GoogleProviderException.ErrorCode.CANCELED;
import static systems.hexa.meek.data.social.google.GoogleProviderException.ErrorCode.GOOGLE_SDK_ERROR;

public class GoogleProviderException extends RuntimeException {
    public final ErrorCode errorCode;
    public final int googleStatusCode;

    public GoogleProviderException(Throwable cause) {
        super(cause);
        errorCode = GOOGLE_SDK_ERROR;
        googleStatusCode = 0;
    }

    public GoogleProviderException(ErrorCode errorCode) {
        this.errorCode = errorCode;
        googleStatusCode = 0;
    }

    public GoogleProviderException(Status status) {
        googleStatusCode = status.getStatusCode();
        if (status.isCanceled() || googleStatusCode == GoogleSignInStatusCodes.SIGN_IN_CANCELLED) {
            errorCode = CANCELED;
        } else {
            errorCode = GOOGLE_SDK_ERROR;
        }
    }

    @Override
    public String getMessage() {
        if (googleStatusCode == 0) {
            return errorCode.toString();
        } else {
            return errorCode + ", statusCode = " + googleStatusCode;
        }
    }

    public enum ErrorCode {
        CANCELED,
        GOOGLE_SDK_ERROR,
        GOOGLE_API_CONNECTING,
        GOOGLE_API_NOT_CONNECTED
    }
}
