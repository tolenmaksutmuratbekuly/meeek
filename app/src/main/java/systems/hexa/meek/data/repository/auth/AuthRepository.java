package systems.hexa.meek.data.repository.auth;

import rx.Observable;
import systems.hexa.meek.data.models.ApiResultSuccess;
import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.data.network.api.meek.API;
import systems.hexa.meek.data.params.auth.AuthEmailPwdParams;

public interface AuthRepository {

    void setAPI(API api);

    Observable<User> signInWithEmailAndPassword(AuthEmailPwdParams params);

    Observable<ApiResultSuccess> resetPassword(String email);
}