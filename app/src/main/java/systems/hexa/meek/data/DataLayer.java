package systems.hexa.meek.data;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import systems.hexa.meek.data.cache.preferences.Preferences;
import systems.hexa.meek.data.cache.preferences.PreferencesProvider;
import systems.hexa.meek.data.network.RetrofitServiceGenerator;
import systems.hexa.meek.data.network.api.meek.API;

public class DataLayer {
    private static volatile DataLayer instance;

    public static DataLayer getInstance(Context context) {
        if (instance == null) {
            synchronized (DataLayer.class) {
                if (instance == null) instance = new DataLayer(context.getApplicationContext());
            }
        }
        return instance;
    }

    private final PreferencesProvider preferencesProvider;
    private final FirebaseDatabase firebaseDatabase;
    private final FirebaseAuth mAuth;
    private final API api;

    private DataLayer(Context context) {
        preferencesProvider = new PreferencesProvider(context);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferencesProvider);
        api = retrofitServiceGenerator.createService(API.class);

    }

    public Preferences getPreferences() {
        return preferencesProvider;
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public FirebaseAuth getFirebaseAuth() {
        return mAuth;
    }

    public void wipeOut() {
        preferencesProvider.clearAllPreferences();
    }

    public API getApi() {
        return api;
    }
}
