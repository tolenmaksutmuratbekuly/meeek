package systems.hexa.meek.data.social.facebook;

public class FacebookProviderException extends Exception {
    public final ErrorCode errorCode;

    public FacebookProviderException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.toString(), cause);
        this.errorCode = errorCode;
    }

    public FacebookProviderException(ErrorCode errorCode) {
        super(errorCode.toString());
        this.errorCode = errorCode;
    }

    public enum ErrorCode {
        CANCELED,
        FACEBOOK_SDK_ERROR,
        RECEIVED_NULL_POST_ID,
        PERMISSION_NOT_GRANTED
    }
}
