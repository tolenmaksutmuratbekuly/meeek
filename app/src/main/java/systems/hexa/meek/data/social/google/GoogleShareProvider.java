package systems.hexa.meek.data.social.google;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.Observable;
import systems.hexa.meek.data.social.ShareProvider;
import systems.hexa.meek.utils.Validate;

import static systems.hexa.meek.data.social.google.GoogleProviderException.ErrorCode.CANCELED;

public class GoogleShareProvider implements ShareProvider {
    private static final int SHARE_INTENT_REQUEST_CODE = 900;

    private final Activity activity;
    private ShareIntentCallback callback;

    public GoogleShareProvider(Activity activity) {
        this.activity = activity;
    }

    @Override
    public Observable<Object> share(@NonNull final Uri url, @Nullable final Uri imageUrl, @Nullable final String text) {
        return Observable.create(subscriber -> {
            Validate.runningOnUiThread();
//            Intent shareIntent = new PlusShare.Builder(activity)
//                    .setType("text/plain")
//                    .setText((text != null ? text + " " : "") + url)
//                    .setStream(imageUrl)
//                    .getIntent();

            callback = new ShareIntentCallback() {
                @Override
                public void onSuccess() {
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                }

                @Override
                public void onFailure() {
                    subscriber.onError(new GoogleProviderException(CANCELED));
                }
            };

            try {
//                activity.startActivityForResult(shareIntent, SHARE_INTENT_REQUEST_CODE);
            } catch (ActivityNotFoundException e) {
                callback = null;
                subscriber.onError(new GoogleProviderException(e));
            }
        });
    }

    /**
     * <b>Don't forget to call this method from {@link Activity#onActivityResult(int, int, Intent)}</b>
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SHARE_INTENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (callback != null) callback.onSuccess();
            } else {
                // TODO: Can we determine a reason of failure here, so that we can show a proper error message to user?
                if (callback != null) callback.onFailure();
            }
            callback = null;
            return true;
        }
        return false;
    }

    private interface ShareIntentCallback {
        void onSuccess();

        void onFailure();
    }
}
