package systems.hexa.meek.data.models.auth;

public class Users {
    public String first_name;
    public String last_name;
    public String search_attributes;
    public String email;
    public String username;
    public String date_of_birth;
    public String gender;

    public Users(String first_name, String last_name, String search_attributes, String email, String username, String date_of_birth, String gender) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.search_attributes = search_attributes;
        this.email = email;
        this.username = username;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }
}
