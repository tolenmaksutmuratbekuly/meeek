package systems.hexa.meek.data.social.google;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import java.io.IOException;

import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import systems.hexa.meek.BuildConfig;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.GPlusUser;
import systems.hexa.meek.ui.auth.login.StartFragment;
import systems.hexa.meek.utils.DebugUtils;
import systems.hexa.meek.utils.Validate;

import static systems.hexa.meek.data.social.google.GoogleProviderException.ErrorCode.GOOGLE_API_CONNECTING;
import static systems.hexa.meek.data.social.google.GoogleProviderException.ErrorCode.GOOGLE_API_NOT_CONNECTED;

public class GoogleAuthProvider implements GPlusProvider, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int SIGN_IN_INTENT_REQUEST_CODE = 9001;
    private static final String OAUTH2_SCOPE = "oauth2:" + Scopes.PLUS_LOGIN + " " + Scopes.EMAIL;

    private final StartFragment context;
    private final GoogleSignInOptions signInOptions;
    private GoogleApiClient googleApiClient;
    private SignInIntentCallback signInCallback;
    private CustomClientConnectionListener clientConnectionListener;

    public GoogleAuthProvider(StartFragment context) {
        this.context = context;

        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken(context.getString(R.string.google_plus_id))
                .requestEmail()
                .build();
    }

    private synchronized GoogleApiClient getGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context.getContext())
                    .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        return googleApiClient;
    }

    private Observable<Void> connect() {
        return Observable.create(subscriber -> {
            Validate.runningOnUiThread();
            GoogleApiClient client = getGoogleApiClient();
            if (client.isConnecting()) {
                subscriber.onError(new GoogleProviderException(GOOGLE_API_CONNECTING));
            } else if (client.isConnected()) {
                subscriber.onNext(null);
                subscriber.onCompleted();
            } else {
                if (BuildConfig.DEBUG) {
                    DebugUtils.androidAssert(clientConnectionListener == null);
                }
                clientConnectionListener = new CustomClientConnectionListener() {
                    @Override
                    public void onConnected() {
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onConnectionFailed() {
                        subscriber.onError(new GoogleProviderException(GOOGLE_API_NOT_CONNECTED));
                    }
                };
                client.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
            }
        });
    }

    private Observable<GoogleSignInAccount> signIn() {
        return Observable.create(subscriber -> {
            Validate.runningOnUiThread();
            signInCallback = result -> {
                if (result.isSuccess()) {
                    subscriber.onNext(result.getSignInAccount());
                    subscriber.onCompleted();
                } else {
                    subscriber.onError(new GoogleProviderException(result.getStatus()));
                }
            };
            Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            context.startActivityForResult(intent, SIGN_IN_INTENT_REQUEST_CODE);
        });
    }

    private Observable<GPlusUser> getUserInfo(GoogleSignInAccount googleSignInAccount) {
        return Observable.create(subscriber -> {
            try {
                Account account = new Account(googleSignInAccount.getEmail(), GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
                String token = GoogleAuthUtil.getToken(context.getContext(), account, OAUTH2_SCOPE);
                String displayName = googleSignInAccount.getDisplayName();
                String familyName = googleSignInAccount.getFamilyName();
                String givenName = googleSignInAccount.getGivenName();
                String email = googleSignInAccount.getEmail();
                String id = googleSignInAccount.getId();

                GPlusUser gPlusUser = new GPlusUser(token, displayName, familyName, givenName, email, id);
                subscriber.onNext(gPlusUser);
                subscriber.onCompleted();
            } catch (IOException | GoogleAuthException e) {
                subscriber.onError(new GoogleProviderException(e));
            }
        });
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGN_IN_INTENT_REQUEST_CODE) {
            if (signInCallback != null) {
                signInCallback.call(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
                signInCallback = null;
            }
            return true;
        }

//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//
//            // G+
//            if (googleApiClient.hasConnectedApi(Plus.API)) {
//                Person person  = Plus.PeopleApi.getCurrentPerson(googleApiClient);
//                if (person != null) {
//                    Log.i(TAG, "--------------------------------");
//                    Log.i(TAG, "Display Name: " + person.getDisplayName());
//                    Log.i(TAG, "Gender: " + person.getGender());
//                    Log.i(TAG, "About Me: " + person.getAboutMe());
//                    Log.i(TAG, "Birthday: " + person.getBirthday());
//                    Log.i(TAG, "Current Location: " + person.getCurrentLocation());
//                    Log.i(TAG, "Language: " + person.getLanguage());
//                } else {
//                    Log.e(TAG, "Error!");
//                }
//            } else {
//                Log.e(TAG, "Google+ not connected");
//            }
//        }

        return false;
    }

    @Override
    public Observable<GPlusUser> getUserInfo() {
        return connect()
                .flatMap(new Func1<Void, Observable<GoogleSignInAccount>>() {
                    @Override
                    public Observable<GoogleSignInAccount> call(Void aVoid) {
                        return signIn();
                    }
                })
                .observeOn(Schedulers.io())
                .flatMap(new Func1<GoogleSignInAccount, Observable<GPlusUser>>() {
                    @Override
                    public Observable<GPlusUser> call(GoogleSignInAccount account) {
                        return getUserInfo(account);
                    }
                });
    }

    public void revokeAccess() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.revokeAccess(googleApiClient);
        }
    }

    public void logout() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(googleApiClient);
        }
    }

    public void disconnect() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    public Observable<Void> logoutAsync() {
        return connect().map(new Func1<Void, Void>() {
            @Override
            public Void call(Void aVoid) {
                Auth.GoogleSignInApi.signOut(googleApiClient);
                return null;
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // GoogleApiClient connection callbacks
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onConnected(Bundle bundle) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnected();
            clientConnectionListener = null;
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnectionFailed();
            clientConnectionListener = null;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (clientConnectionListener != null) {
            clientConnectionListener.onConnectionFailed();
            clientConnectionListener = null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Internal callbacks
    ///////////////////////////////////////////////////////////////////////////

    private interface SignInIntentCallback {
        void call(GoogleSignInResult result);
    }

    private interface CustomClientConnectionListener {
        void onConnected();

        void onConnectionFailed();
    }
}
