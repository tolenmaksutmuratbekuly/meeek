package systems.hexa.meek.data.models.questions;

import android.graphics.drawable.GradientDrawable;

public class QuestionsFeed {
    public static final int TYPE_ANSWER_DEFAULT = 0;
    public static final int TYPE_ANSWER_YEAH = 1;
    public static final int TYPE_ANSWER_NOPE = 2;
    public static final int TYPE_ANSWER_NEUTRAL = 3;
    private Integer answerType;
    private GradientDrawable gradient;

    public QuestionsFeed(Integer answerType, GradientDrawable gradient) {
        this.answerType = answerType;
        this.gradient = gradient;
    }

    public Integer getAnswerType() {
        return answerType;
    }

    public void setAnswerType(Integer answerType) {
        this.answerType = answerType;
    }

    public GradientDrawable getGradient() {
        return gradient;
    }
}
