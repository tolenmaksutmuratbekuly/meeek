package systems.hexa.meek.data.social.facebook;

import com.facebook.AccessToken;

import rx.Observable;
import systems.hexa.meek.data.models.auth.FBUser;

public interface FBProvider {
    Observable<AccessToken> getAccessToken();

    Observable<FBUser> getFBUser(AccessToken accessToken);
}
