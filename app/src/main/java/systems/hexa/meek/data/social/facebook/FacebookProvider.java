package systems.hexa.meek.data.social.facebook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import systems.hexa.meek.data.social.ShareProvider;
import systems.hexa.meek.data.models.auth.FBUser;
import systems.hexa.meek.ui.auth.login.StartFragment;
import systems.hexa.meek.utils.Validate;

import static systems.hexa.meek.data.social.facebook.FacebookProviderException.ErrorCode.CANCELED;
import static systems.hexa.meek.data.social.facebook.FacebookProviderException.ErrorCode.FACEBOOK_SDK_ERROR;
import static systems.hexa.meek.data.social.facebook.FacebookProviderException.ErrorCode.PERMISSION_NOT_GRANTED;
import static systems.hexa.meek.data.social.facebook.FacebookProviderException.ErrorCode.RECEIVED_NULL_POST_ID;

public class FacebookProvider implements ShareProvider, FBProvider {
    private final StartFragment context;
    private final CallbackManager callbackManager;
    private final ShareDialog shareDialog;

    public FacebookProvider(StartFragment context) {
        FacebookSdk.sdkInitialize(context.getContext());
        AccessToken.refreshCurrentAccessTokenAsync();

        this.context = context;
        this.callbackManager = CallbackManager.Factory.create();
        this.shareDialog = new ShareDialog(context);
    }

    /**
     * <b>Don't forget to call this method from {@link Activity#onActivityResult(int, int, Intent)}</b>
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {
        return callbackManager.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public Observable<Object> share(@NonNull Uri url, @Nullable Uri imageUrl, @Nullable String text) {
        final ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                .setContentUrl(url)
                .setContentDescription(text)
                .setImageUrl(imageUrl)
                .build();

        return getPublishPermissions(Collections.singleton("publish_actions"))
                .flatMap(new Func1<AccessToken, Observable<String>>() {
                    @Override
                    public Observable<String> call(AccessToken accessToken) {
                        return shareInternal(shareLinkContent);
                    }
                })
                .cast(Object.class);
    }

    @Override
    public Observable<AccessToken> getAccessToken() {
        return getReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
    }

    @Override
    public Observable<FBUser> getFBUser(AccessToken mAccessToken) {
        return Observable.just(mAccessToken)
                .map(accessToken -> graphRequest(mAccessToken))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private FBUser graphRequest(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, birthday, gender");
        request.setParameters(parameters);
        GraphResponse graphResponse = request.executeAndWait();

        return new Gson().fromJson(String.valueOf(graphResponse.getJSONObject()), FBUser.class);
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    private static boolean hasPermissions(AccessToken accessToken, Collection<String> permissions) {
        if (permissions == null || permissions.isEmpty()) return true;
        Set<String> grantedPermissions = accessToken != null ? accessToken.getPermissions() : null;
        if (grantedPermissions == null || grantedPermissions.isEmpty()) return false;

        for (String permission : permissions) {
            if (!grantedPermissions.contains(permission)) return false;
        }
        return true;
    }

    private Observable<AccessToken> getReadPermissions(Collection<String> requiredPermissions) {
        return getPermissions(false, requiredPermissions, null);
    }

    private Observable<AccessToken> getPublishPermissions(Collection<String> requiredPermissions) {
        return getPermissions(true, requiredPermissions, null);
    }

    private Observable<AccessToken> getPermissions(final boolean publish, final Collection<String> requiredPermissions,
                                                   final Collection<String> optionalPermissions) {
        final ArrayList<String> allPermissions = new ArrayList<>();
        if (requiredPermissions != null) allPermissions.addAll(requiredPermissions);
        if (optionalPermissions != null) allPermissions.addAll(optionalPermissions);

        return Observable.create(subscriber -> {
            Validate.runningOnUiThread();
            LoginManager loginManager = LoginManager.getInstance();
            loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    AccessToken accessToken = loginResult != null ? loginResult.getAccessToken() : null;
                    if (hasPermissions(accessToken, requiredPermissions)) {
                        subscriber.onNext(accessToken);
                        subscriber.onCompleted();
                    } else {
                        subscriber.onError(new FacebookProviderException(PERMISSION_NOT_GRANTED));
                    }
                }

                @Override
                public void onCancel() {
                    subscriber.onError(new FacebookProviderException(CANCELED));
                }

                @Override
                public void onError(FacebookException e) {
                    if (e instanceof FacebookAuthorizationException) {
                        // This happens when current token becomes invalid, e.g when user revokes permission for our
                        // app in Facebook settings. We invalidate current token so the user is re-asked for
                        // permissions next time.
                        AccessToken.setCurrentAccessToken(null);
                    }

                    subscriber.onError(new FacebookProviderException(FACEBOOK_SDK_ERROR, e));
                }
            });

            if (publish) {
                loginManager.logInWithPublishPermissions(context, allPermissions);
            } else {
                loginManager.logInWithReadPermissions(context, allPermissions);
            }
        });
    }

    private Observable<String> shareInternal(final ShareContent shareContent) {
        return Observable.create(subscriber -> {
            Validate.runningOnUiThread();

            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    if (result != null && result.getPostId() != null) {
                        subscriber.onNext(result.getPostId());
                        subscriber.onCompleted();
                    } else {
                        // Null post id might mean that "publish_actions" permission has been revoked by the user
                        // but current access token still shows this permission as granted.
                        AccessToken.refreshCurrentAccessTokenAsync();
                        subscriber.onError(new FacebookProviderException(RECEIVED_NULL_POST_ID));
                    }
                }

                @Override
                public void onCancel() {
                    subscriber.onError(new FacebookProviderException(CANCELED));
                }

                @Override
                public void onError(FacebookException e) {
                    subscriber.onError(new FacebookProviderException(FACEBOOK_SDK_ERROR, e));
                }
            });
            shareDialog.show(shareContent);
        });
    }
}
