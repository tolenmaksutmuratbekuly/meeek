package systems.hexa.meek.data.models.auth;

import static systems.hexa.meek.utils.Constants.KEY_GENDER_FEMALE;
import static systems.hexa.meek.utils.Constants.KEY_GENDER_MALE;

public enum Gender {
    M(0), F(1);

    public final int intValue;

    Gender(int intValue) {
        this.intValue = intValue;
    }

    public static Gender fromString(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case KEY_GENDER_MALE:
                return M;
            case KEY_GENDER_FEMALE:
                return F;
            default:
                return null;
        }
    }
}
