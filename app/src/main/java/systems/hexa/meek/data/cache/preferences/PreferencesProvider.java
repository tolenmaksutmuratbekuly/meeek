package systems.hexa.meek.data.cache.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static android.text.TextUtils.isEmpty;
import static systems.hexa.meek.utils.Constants.USER_ID;
import static systems.hexa.meek.utils.StringUtils.EMPTY_STRING;

public class PreferencesProvider implements Preferences {
    private final SharedPreferences sharedPreferences;

    public PreferencesProvider(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearAllPreferences() {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().clear().apply();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Preferences implementation
    ///////////////////////////////////////////////////////////////////////////

    public boolean isAuthenticated() {
        return !isEmpty(sharedPreferences.getString(USER_ID, EMPTY_STRING));
    }

    @Override
    public void setUserId(String token) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putString(USER_ID, token).apply();
        }
    }

    @Override
    public boolean hasShownSplashScreen(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    @Override
    public void setShownSplashScreen(boolean shown, String key) {
        synchronized (sharedPreferences) {
            sharedPreferences.edit().putBoolean(key, shown).apply();
        }
    }
}
