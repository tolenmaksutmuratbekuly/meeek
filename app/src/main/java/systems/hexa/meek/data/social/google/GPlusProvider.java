package systems.hexa.meek.data.social.google;

import rx.Observable;
import systems.hexa.meek.data.models.auth.GPlusUser;

public interface GPlusProvider {
    Observable<GPlusUser> getUserInfo();
}
