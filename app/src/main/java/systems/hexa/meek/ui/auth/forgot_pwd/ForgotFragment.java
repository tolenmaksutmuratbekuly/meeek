package systems.hexa.meek.ui.auth.forgot_pwd;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.auth.forgot_pwd.ForgotPresenter;
import systems.hexa.meek.mvp.auth.forgot_pwd.ForgotView;
import systems.hexa.meek.ui.base.BaseFragment;

import static systems.hexa.meek.utils.Constants.FORM_KEY;
import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class ForgotFragment extends BaseFragment implements ForgotView {
    @InjectPresenter
    ForgotPresenter mvpPresenter;
    private PageSwapCallback callback;

    @BindView(R.id.ll_content) LinearLayout ll_content;
    @BindView(R.id.et_email) MaterialEditText et_email;

    public static ForgotFragment newInstance(AuthFormValidation authForm) {
        ForgotFragment fragment = new ForgotFragment();
        Bundle data = new Bundle();
        data.putParcelable(FORM_KEY, Parcels.wrap(authForm));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_forgot_pwd;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        handleIntent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    private void handleIntent() {
        if (getArguments() != null && getArguments().containsKey(FORM_KEY)) {
            AuthFormValidation form = Parcels.unwrap(getArguments().getParcelable(FORM_KEY));
            mvpPresenter.init(dataLayer, form);
        }
    }

    @OnClick(R.id.btn_submit)
    void onSubmitClick() {
        clearFocusFromAllViews(ll_content);
        hideSoftKeyboard(ll_content);
        showErrIcon(et_email, 0);
        mvpPresenter.setEmail(et_email.getText().toString());
        mvpPresenter.validation();
    }

    @OnClick(R.id.btn_back_to_signin)
    void onBackToSignInClick() {
        if (callback != null) {
            callback.onBackToSignInClick();
        }
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ForgotView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setEmail(String email) {
        et_email.setText(email);
    }

    @Override
    public void showEmailError(@StringRes int errorRes) {
        showInputFieldError(et_email, errorRes);
        showErrIcon(et_email, R.drawable.err_input_require);
    }

    @Override
    public void successSent() {
        if (callback != null) {
            callback.onSubmitClick();
        }
    }
}