package systems.hexa.meek.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.profile.SettingsPresenter;
import systems.hexa.meek.mvp.profile.SettingsView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.questions.ask_question.SelectLocationActivity;

public class SettingsActivity extends BaseActivity1 implements SettingsView {
    @BindView(R.id.toolbar) Toolbar toolbar;

    private SettingsPresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_settings;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new SettingsPresenter(dataLayer);
        mvpPresenter.attachView(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void goBackStack() {
        finish();
    }

    @OnClick({R.id.ll_prof_container, R.id.ll_change_pwd})
    void onMenuItemClick(LinearLayout layout) {
        if (layout.getId() == R.id.ll_prof_container)
            startActivity(ProfileActivity.getIntent(this));
        else if (layout.getId() == R.id.ll_change_pwd)
            startActivity(ChangePwdActivity.getIntent(this));
    }


    ///////////////////////////////////////////////////////////////////////////
    // SettingsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openSelectLocationActivity() {
        startActivity(SelectLocationActivity.getIntent(this));
    }
}