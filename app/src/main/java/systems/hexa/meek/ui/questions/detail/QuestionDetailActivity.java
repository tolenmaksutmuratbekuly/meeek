package systems.hexa.meek.ui.questions.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import butterknife.BindView;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.questions.detail.QuestionDetailPresenter;
import systems.hexa.meek.mvp.questions.detail.QuestionDetailView;
import systems.hexa.meek.ui.base.BaseFragmentActivity1;
import systems.hexa.meek.utils.ColorUtils;
import systems.hexa.meek.utils.Constants;

public class QuestionDetailActivity extends BaseFragmentActivity1 implements QuestionDetailView {
    private int answerType;
    private boolean answered;

    private QuestionDetailPresenter mvpPresenter;
    @BindView(R.id.frgm_container) FrameLayout frgm_container;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_framelayout;
    }

    public static Intent getIntent(Context context, int answerType, boolean answered) {
        Intent intent = new Intent(context, QuestionDetailActivity.class);
        intent.putExtra(Constants.ANSWER, answerType);
        intent.putExtra(Constants.RESULT, answered);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        answerType = intent.getIntExtra(Constants.ANSWER, 0);
        answered = intent.getBooleanExtra(Constants.RESULT, false);

        mvpPresenter = new QuestionDetailPresenter(dataLayer);
        mvpPresenter.attachView(this);
        frgm_container.setBackground(ColorUtils.getGradient(this, R.color.nearby_gradient_1, R.color.nearby_gradient_2));

        if (savedInstanceState == null) {
            if (answered) {
                ResultFragment fragment = ResultFragment.newInstance(null, answerType);
                replaceFragment(fragment, false);
            } else {
                DetailFragment detailFragment = DetailFragment.newInstance(null);
                replaceFragment(detailFragment, false);
            }
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // NearByView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openMainScreen() {
//        startActivity(MainActivity.getIntent(this, false));
//        finish();
    }
}