package systems.hexa.meek.ui.questions.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.questions.detail.ResultPresenter;
import systems.hexa.meek.mvp.questions.detail.ResultView;
import systems.hexa.meek.ui.base.BaseFragment1;
import systems.hexa.meek.ui.nearby.PageSwapCallback;
import systems.hexa.meek.ui.questions.detail.chat.ChatActivity;
import systems.hexa.meek.utils.Constants;
import systems.hexa.meek.views.swipe_view.SwipeStack;

public class ResultFragment extends BaseFragment1 implements ResultView {
    private ResultPresenter mvpPresenter;
    private PageSwapCallback callback;

    @BindView(R.id.swipe_stack) SwipeStack swipe_stack;
    private ArrayList<String> mData;
    private SwipeStackAdapter mAdapter;
    private int answerType;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_detail;
    }

    public static ResultFragment newInstance(PageSwapCallback callback, int answerType) {
        ResultFragment fragment = new ResultFragment();
        fragment.callback = callback;

        Bundle args = new Bundle();
        args.putInt(Constants.ANSWER, answerType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        answerType = getArguments().getInt(Constants.ANSWER);
        mvpPresenter = new ResultPresenter(dataLayer);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        assert v != null;
        ButterKnife.bind(this, v);
        mvpPresenter.attachView(this);

        configureStackView();
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mvpPresenter.detachView();
    }

    private void configureStackView() {
        mData = new ArrayList<>();

        int layoutId = 0;
        if (answerType == QuestionsFeed.TYPE_ANSWER_YEAH) {
            layoutId = SwipeStackAdapter.LAYOUT_RESULT_GENDER;
        } else if (answerType == QuestionsFeed.TYPE_ANSWER_NOPE) {
            layoutId = SwipeStackAdapter.LAYOUT_RESULT;
        } else if (answerType == QuestionsFeed.TYPE_ANSWER_NEUTRAL) {
            layoutId = SwipeStackAdapter.LAYOUT_RESULT_GENDER;
        }

        mAdapter = new SwipeStackAdapter(getContext(), mData, layoutId, answerType);
        mAdapter.setCallback(() -> startActivity(ChatActivity.getIntent(getContext())));
        swipe_stack.setAdapter(mAdapter);
        swipe_stack.setListener(new SwipeStack.SwipeStackListener() {
            @Override
            public void onViewSwipedToLeft(int position) {

            }

            @Override
            public void onViewSwipedToRight(int position) {

            }

            @Override
            public void onStackEmpty() {

            }
        });

        mvpPresenter.fillWithTestData();

    }

    ///////////////////////////////////////////////////////////////////////////
    // DetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void finish() {

    }

    @Override
    public Context getCtx() {
        return getContext();
    }

    @Override
    public void setCardsList(List<String> mData) {
        this.mData.addAll(mData);
    }
}