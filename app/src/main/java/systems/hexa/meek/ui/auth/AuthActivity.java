package systems.hexa.meek.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.auth.AuthPresenter;
import systems.hexa.meek.mvp.auth.AuthView;
import systems.hexa.meek.ui.auth.intro.IntroActivity;
import systems.hexa.meek.ui.auth.login.LoginFragment;
import systems.hexa.meek.ui.auth.login.StartFragment;
import systems.hexa.meek.ui.auth.signup.SignUpDetailFragment;
import systems.hexa.meek.ui.auth.signup.SignUpFinishFragment;
import systems.hexa.meek.ui.auth.signup.SignUpFragment;
import systems.hexa.meek.ui.base.BaseFragmentActivity;
import systems.hexa.meek.ui.questions.QuestionsActivity;
import systems.hexa.meek.utils.LOTimber;

import static systems.hexa.meek.utils.Constants.TYPE_WORLD;

public class AuthActivity extends BaseFragmentActivity implements AuthView, PageSwapCallback {
    @InjectPresenter
    AuthPresenter mvpPresenter;
    private StartFragment startFragment;
    private LoginFragment loginFragment;
    private SignUpFragment signUpFragment;
    private SignUpDetailFragment signUpDetailFragment;
    private SignUpFinishFragment signUpFinishFragment;

    @BindView(R.id.toolbar) Toolbar toolbar;

//    public static Intent newLogoutIntent(Context context) {
//        return new Intent(context, AuthActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_auth;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.init(dataLayer);
        mvpPresenter.onRestoreInstanceState(savedInstanceState);

        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            startFragment = (StartFragment) getFragment(StartFragment.class.getName());
            replaceFragment(startFragment, false);
            toolbar.setVisibility(View.INVISIBLE);
        }
        toolbar.setNavigationOnClickListener(v -> goBackStack());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mvpPresenter.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        Fragment f = findFragmentById();
        if (f instanceof StartFragment && f.isVisible()) {
            super.onBackPressed();
        } else if (f instanceof LoginFragment && f.isVisible()) {
            toolbar.setVisibility(View.INVISIBLE);
            replaceFragment(getFragment(StartFragment.class.getName()), false);
        } else if (f instanceof SignUpFragment && f.isVisible()) {
            replaceFragment(getFragment(LoginFragment.class.getName()), false);
        } else if (f instanceof SignUpDetailFragment && f.isVisible()) {
            replaceFragment(getFragment(SignUpFragment.class.getName()), false);
        } else if (f instanceof SignUpFinishFragment && f.isVisible()) {
            replaceFragment(getFragment(SignUpDetailFragment.class.getName()), false);
        }
    }

    private Fragment getFragment(String className) {
        LOTimber.d("salkdasjdlkasd SECONDD=" + className);

        if (className.equals(StartFragment.class.getName())) {
            if (startFragment == null) startFragment = StartFragment.newInstance(mvpPresenter.getAuthForm());
            return startFragment;

        } else if (className.equals(LoginFragment.class.getName())) {
            if (loginFragment == null) loginFragment = LoginFragment.newInstance();
            return loginFragment;

        } else if (className.equals(SignUpFragment.class.getName())) {
            if (signUpFragment == null) {
                signUpFragment = SignUpFragment.newInstance(mvpPresenter.getAuthForm());
            }
            return signUpFragment;

        } else if (className.equals(SignUpDetailFragment.class.getName())) {
            LOTimber.d("salkdasjdlkasd THIRD=" + className);
            if (signUpDetailFragment == null) {
                signUpDetailFragment = SignUpDetailFragment.newInstance(
                        mvpPresenter.getAuthForm(), mvpPresenter.isGPlusAuth());
                LOTimber.d("salkdasjdlkasd FORTH=" + signUpDetailFragment);
            }

            return signUpDetailFragment;

        } else if (className.equals(SignUpFinishFragment.class.getName())) {
            if (signUpFinishFragment == null)
                signUpFinishFragment = SignUpFinishFragment.newInstance(mvpPresenter.getAuthForm());
            return signUpFinishFragment;
        }
        return null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // AuthView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void openMainScreen() {
        startActivity(QuestionsActivity.alreadyAuthorized(this, TYPE_WORLD, getString(R.string.t_world_desc)));
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PageSwapCallback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onLoginWithEmailClick() {
        toolbar.setVisibility(View.VISIBLE);
        replaceFragment(getFragment(LoginFragment.class.getName()), false);
    }

    @Override
    public void onSignUpClick() {
        replaceFragment(getFragment(SignUpFragment.class.getName()), false);
    }

    @Override
    public void openSignUpDetail(AuthFormValidation form, boolean isGPlusAuth) {
        mvpPresenter.setAuthForm(form);
        mvpPresenter.setIsGPlusAuth(isGPlusAuth);
        LOTimber.d("salkdasjdlkasd FIRST=" + SignUpDetailFragment.class.getName());
        replaceFragment(getFragment(SignUpDetailFragment.class.getName()), false);
    }

    @Override
    public void openSignUpFinish(AuthFormValidation form) {
        mvpPresenter.setAuthForm(form);
        replaceFragment(getFragment(SignUpFinishFragment.class.getName()), false);
    }

    @Override
    public void onRegistrationSuccess() {
        startActivity(IntroActivity.getIntent(this));
        finish();
    }
}