package systems.hexa.meek.ui.auth.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.Button;
import android.widget.RadioGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.parceler.Parcels;

import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.data.models.auth.Gender;
import systems.hexa.meek.mvp.auth.signup.SignUpDetailPresenter;
import systems.hexa.meek.mvp.auth.signup.SignUpDetailView;
import systems.hexa.meek.ui.auth.PageSwapCallback;
import systems.hexa.meek.ui.base.BaseFragment;
import systems.hexa.meek.utils.LOTimber;
import systems.hexa.meek.utils.ToastUtils;
import systems.hexa.meek.views.dialogs.DatePickerDialog;

import static systems.hexa.meek.utils.Constants.FORM_KEY;
import static systems.hexa.meek.utils.Constants.KEY_BOOLEAN;
import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class SignUpDetailFragment extends BaseFragment implements SignUpDetailView {
    @InjectPresenter
    SignUpDetailPresenter mvpPresenter;
    private PageSwapCallback callback;
    @BindView(R.id.et_birthday) MaterialEditText et_birthday;
    @BindView(R.id.rg_gender) RadioGroup rg_gender;
    @BindView(R.id.btn_next) Button btn_next;

    public static SignUpDetailFragment newInstance(AuthFormValidation form, boolean isGPlusAuth) {
        SignUpDetailFragment fragment = new SignUpDetailFragment();
        Bundle data = new Bundle();
        data.putParcelable(FORM_KEY, Parcels.wrap(form));
        data.putBoolean(KEY_BOOLEAN, isGPlusAuth);
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_signup_detail;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        LOTimber.d("salkdasjdlkasd YEAPTA DETAIL=");
        configureGenderRadioGroup();
        handleIntent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    private void handleIntent() {
        if (getArguments() != null && getArguments().containsKey(FORM_KEY)
                && getArguments().containsKey(KEY_BOOLEAN)) {
            AuthFormValidation form = Parcels.unwrap(getArguments().getParcelable(FORM_KEY));
            boolean isGPlusAuth = getArguments().getBoolean(KEY_BOOLEAN);
            if (form != null) {
                LOTimber.d("salkdasjdlkasd YEAPTA DETAIL FOORRM NOT NULL");
                mvpPresenter.init(dataLayer, form, isGPlusAuth);
            }
        }
    }

    @OnClick(R.id.btn_next)
    void onBtnNextClick() {
        clearFocusFromAllViews(fl_parent);
        hideSoftKeyboard(fl_parent);
        showErrIcon(et_birthday, 0);
        mvpPresenter.checkGeneralInfo();
    }

    @OnClick(R.id.et_birthday)
    void onBirthdaySelect() {
        mvpPresenter.showDatePickerDialog();
    }

    private void configureGenderRadioGroup() {
        rg_gender.setOnCheckedChangeListener((group, checkedId) ->
                mvpPresenter.setGender(radioButtonIdToGender(checkedId)));
    }

    private static int genderToRadioButtonId(Gender gender) {
        if (gender == Gender.M) {
            return R.id.rb_male;
        } else if (gender == Gender.F) {
            return R.id.rb_female;
        } else {
            return -1;
        }
    }

    private static Gender radioButtonIdToGender(int id) {
        switch (id) {
            case R.id.rb_male:
                return Gender.M;
            case R.id.rb_female:
                return Gender.F;
            default:
                return null;
        }
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // SignUpDetailView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showBirthDateError(int errorRes) {
        showInputFieldError(et_birthday, errorRes);
        showErrIcon(et_birthday, R.drawable.err_input_require);
    }

    @Override
    public void showGenderError(int errorRes) {
        ToastUtils.showToast(getContext(), errorRes);
    }

    @Override
    public void setBirthdate(String birthdateStr) {
        et_birthday.setText(birthdateStr);
    }

    @Override
    public void setGender(Gender gender) {
        rg_gender.check(genderToRadioButtonId(gender));
    }

    @Override
    public void showDatePickerDialog(Date currentBirthdate) {
        DatePickerDialog dialog = DatePickerDialog.newInstance(currentBirthdate);
        dialog.setCallback(date -> mvpPresenter.setBirthdate(date));
        dialog.show(getFragmentManager(), "BirthdatePickerDialog");
    }

    @Override
    public void setFinishTextInBtn() {
        btn_next.setText(getString(R.string.action_finish));
    }

    @Override
    public void setAuthForm(AuthFormValidation form) {
        if (callback != null) {
            callback.openSignUpFinish(form);
        }
    }

    @Override
    public void onRegistrationSuccess() {
        if (callback != null) {
            callback.onRegistrationSuccess();
        }
    }
}