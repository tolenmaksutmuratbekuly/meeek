package systems.hexa.meek.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.profile.ChangePwdPresenter;
import systems.hexa.meek.mvp.profile.ChangePwdView;
import systems.hexa.meek.ui.base.BaseActivity1;

public class ChangePwdActivity extends BaseActivity1 implements ChangePwdView {
    @BindView(R.id.toolbar) Toolbar toolbar;

    private ChangePwdPresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_change_pwd;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ChangePwdActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new ChangePwdPresenter(dataLayer);
        mvpPresenter.attachView(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    @OnClick(R.id.tv_cancel)
    void goBackStack() {
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ChangePwdView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }
}