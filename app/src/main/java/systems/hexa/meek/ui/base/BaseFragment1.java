package systems.hexa.meek.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import systems.hexa.meek.data.DataLayer;

public abstract class BaseFragment1 extends Fragment {
    protected DataLayer dataLayer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataLayer = DataLayer.getInstance(getContext());
    }

    protected abstract int getLayoutResourceId();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, v);
        return v;
    }
}