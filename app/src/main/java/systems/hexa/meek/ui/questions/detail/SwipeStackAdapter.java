package systems.hexa.meek.ui.questions.detail;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.utils.StringUtils;
import systems.hexa.meek.views.dialogs.CommonDialog;
import systems.hexa.meek.views.progress_bar.multi_progress.MultiProgressBar;
import systems.hexa.meek.views.progress_bar.multi_progress.ProgressItem;

class SwipeStackAdapter extends BaseAdapter {
    static final int LAYOUT_DETAIL = R.layout.adapter_question_detail;
    static final int LAYOUT_RESULT = R.layout.adapter_question_result;
    static final int LAYOUT_RESULT_GENDER = R.layout.adapter_question_result_gender;
    private Context context;
    private LayoutInflater inflater;
    private List<String> mData;
    private int layout;
    private int answerType;
    private Callback callback;

    SwipeStackAdapter(Context context, List<String> data, int layout, int answerType) {
        this.context = context;
        this.mData = data;
        this.layout = layout;
        this.answerType = answerType;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
            if (layout == LAYOUT_DETAIL) {
                convertView.setTag(new DetailViewHolder(convertView));
            } else if (layout == LAYOUT_RESULT) {
                convertView.setTag(new ResultViewHolder(convertView));
            } else if (layout == LAYOUT_RESULT_GENDER) {
                convertView.setTag(new ResultGenderViewHolder(convertView));
            }
        }

        if (layout == LAYOUT_DETAIL) {
            DetailViewHolder viewHolder = (DetailViewHolder) convertView.getTag();
            viewHolder.bindHolder(mData.get(position));
        } else if (layout == LAYOUT_RESULT) {
            ResultViewHolder viewHolder = (ResultViewHolder) convertView.getTag();
            viewHolder.bindHolder(mData.get(position));
        } else if (layout == LAYOUT_RESULT_GENDER) {
            ResultGenderViewHolder viewHolder = (ResultGenderViewHolder) convertView.getTag();
            viewHolder.bindHolder(mData.get(position));
        }

        return convertView;
    }

    private void answerButtonsState(FloatingActionButton fb, boolean result, int activeColor, int defaultColor,
                                    int activeRes, int defaultRes) {
        fb.setBackgroundTintList(ColorStateList.valueOf(result ? activeColor : defaultColor));
        fb.setImageResource(result ? activeRes : defaultRes);
    }

    private void actionMoreDialog() {
        CommonDialog dialog = new CommonDialog(context);
        dialog.showListDialog(context, Arrays.asList(context.getResources().getStringArray(R.array.share_dialog_titles)));
        dialog.setCallbackListView(index -> {
            if (index == 3) {
                dialog.showListDialog(context, Arrays.asList(context.getResources().getStringArray(R.array.dialog_block_titles)));
                dialog.setCallbackListView(index1 -> {

                });
            } else if (index == 4) {
                dialog.showDialogTitleDescSingleBtn(
                        context.getString(R.string.user_blocked), context.getString(R.string.you_can_unblock), true, true);
            }
        });
    }

    class DetailViewHolder extends RecyclerView.ViewHolder {
        Context context;
        String bindedComment;
        @BindView(R.id.tv_full_txt) TextView tv_full_txt;

        DetailViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindHolder(String str) {
            bindedComment = str;
            if (str == null) {
                tv_full_txt.setText(null);
                return;
            }
            tv_full_txt.setText(str);
        }

        @OnClick(R.id.img_action_more)
        void actionMoreClick() {
            actionMoreDialog();
        }
    }

    class ResultViewHolder extends RecyclerView.ViewHolder {
        Context context;
        String bindedComment;
        @BindView(R.id.tv_full_txt) TextView tv_full_txt;
        @BindView(R.id.multi_pb) MultiProgressBar multi_pb;

        @BindView(R.id.fab_nah) FloatingActionButton fab_nah;
        @BindView(R.id.fab_neutral) FloatingActionButton fab_neutral;
        @BindView(R.id.fab_yeah) FloatingActionButton fab_yeah;
        @BindView(R.id.tv_fab_neutral) TextView tv_fab_neutral;

        @BindColor(R.color.multi_pb_nope) int colorNope;
        @BindColor(R.color.multi_pb_neutral) int colorNeutral;
        @BindColor(R.color.multi_pb_yeah) int colorYeah;
        @BindColor(R.color.white) int colorWhite;
        @BindColor(R.color.answer_neutral_progress_label) int colorNeutralProgressLabel;
        @BindColor(R.color.neutral_active_btn) int colorNeutralActive;
        @BindColor(R.color.nickname) int colorNeutralText;

        @BindString(R.string.answer_nope) String answer_nope;
        @BindString(R.string.answer_neutral) String answer_neutral;
        @BindString(R.string.answer_yeah) String answer_yeah;

        ResultViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindHolder(String str) {
            bindedComment = str;
            if (str == null) {
                tv_full_txt.setText(null);
                return;
            }
            tv_full_txt.setText(str);
            multi_pb.setTouchable(false);
            multi_pb.showLabel(true);
            multi_pb.getThumb().mutate().setAlpha(0);

            // Answer buttons
            answerButtonsState(fab_nah, answerType == QuestionsFeed.TYPE_ANSWER_NOPE, colorNope, colorWhite,
                    R.drawable.ans_no_active, R.drawable.ans_no_default);

            fab_neutral.setBackgroundTintList(ColorStateList.valueOf(answerType == QuestionsFeed.TYPE_ANSWER_NEUTRAL
                    ? colorNeutralActive : colorWhite));
            tv_fab_neutral.setTextColor(answerType == QuestionsFeed.TYPE_ANSWER_NEUTRAL ? colorWhite : colorNeutralText);

            answerButtonsState(fab_yeah, answerType == QuestionsFeed.TYPE_ANSWER_YEAH, colorYeah, colorWhite,
                    R.drawable.ans_yes_active, R.drawable.ans_yes_default);

            initDataToSeekbar();
        }

        @OnClick(R.id.img_action_more)
        void actionMoreClick() {
            actionMoreDialog();
        }

        @OnClick(R.id.btn_join_chat)
        void onJoinChatClick() {
            if (callback != null) {
                callback.onJoinChatClick();
            }
        }

        private void initDataToSeekbar() {
            ArrayList<ProgressItem> progressItemList = new ArrayList<>();
            // Nope
            ProgressItem mProgressItem = new ProgressItem(colorNope, colorWhite, answer_nope, 40);
            progressItemList.add(mProgressItem);

            // Neutral
            mProgressItem = new ProgressItem(colorNeutral, colorNeutralProgressLabel, answer_neutral, 15);
            progressItemList.add(mProgressItem);
            // Yeah
            mProgressItem = new ProgressItem(colorYeah, colorWhite, answer_yeah, 45);
            progressItemList.add(mProgressItem);

            multi_pb.initData(progressItemList);
            multi_pb.invalidate();
        }
    }

    class ResultGenderViewHolder extends RecyclerView.ViewHolder {
        Context context;
        String bindedComment;
        @BindView(R.id.tv_full_txt) TextView tv_full_txt;
        @BindView(R.id.multi_pb) MultiProgressBar multi_pb;
        @BindView(R.id.pc_male) PieChart pc_male;
        @BindView(R.id.pc_female) PieChart pc_female;
        @BindView(R.id.multi_age_lower_21) MultiProgressBar multi_age_lower_21;
        @BindView(R.id.multi_age_21_40) MultiProgressBar multi_age_21_40;
        @BindView(R.id.multi_age_greater_40) MultiProgressBar multi_age_greater_40;
        @BindView(R.id.tv_fab_neutral) TextView tv_fab_neutral;

        @BindView(R.id.fab_nah) FloatingActionButton fab_nah;
        @BindView(R.id.fab_neutral) FloatingActionButton fab_neutral;
        @BindView(R.id.fab_yeah) FloatingActionButton fab_yeah;

        // Binding colors
        @BindColor(R.color.multi_pb_nope) int colorNope;
        @BindColor(R.color.multi_pb_neutral) int colorNeutral;
        @BindColor(R.color.multi_pb_yeah) int colorYeah;
        @BindColor(R.color.white) int colorWhite;
        @BindColor(R.color.answer_neutral_progress_label) int colorNeutralProgressLabel;
        @BindColor(R.color.neutral_active_btn) int colorNeutralActive;
        @BindColor(R.color.nickname) int colorNeutralText;

        // Binding strings
        @BindString(R.string.answer_nope) String answer_nope;
        @BindString(R.string.answer_neutral) String answer_neutral;
        @BindString(R.string.answer_yeah) String answer_yeah;

        int malePercents[] = {40, 35, 25};
        int femalePercents[] = {20, 30, 50};

        ResultGenderViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        void bindHolder(String str) {
            bindedComment = str;
            if (str == null) {
                tv_full_txt.setText(null);
                return;
            }
            tv_full_txt.setText(str);
            configureMultiProgressBar(multi_pb, 40, 15, 45, true);

            configureMultiProgressBar(multi_age_lower_21, 45, 15, 40, false);
            configureMultiProgressBar(multi_age_21_40, 50, 20, 30, false);
            configureMultiProgressBar(multi_age_greater_40, 30, 25, 45, false);

            configureCircleChart(pc_male, malePercents);
            configureCircleChart(pc_female, femalePercents);

            // Answer buttons
            answerButtonsState(fab_nah, answerType == QuestionsFeed.TYPE_ANSWER_NOPE, colorNope, colorWhite,
                    R.drawable.ans_no_active, R.drawable.ans_no_default);

            fab_neutral.setBackgroundTintList(ColorStateList.valueOf(answerType == QuestionsFeed.TYPE_ANSWER_NEUTRAL
                    ? colorNeutralActive : colorWhite));
            tv_fab_neutral.setTextColor(answerType == QuestionsFeed.TYPE_ANSWER_NEUTRAL ? colorWhite : colorNeutralText);

            answerButtonsState(fab_yeah, answerType == QuestionsFeed.TYPE_ANSWER_YEAH, colorYeah, colorWhite,
                    R.drawable.ans_yes_active, R.drawable.ans_yes_default);

        }

        @OnClick(R.id.img_action_more)
        void actionMoreClick() {
            actionMoreDialog();
        }

        private void configureCircleChart(PieChart pieChart, int[] percents) {
            pieChart.setUsePercentValues(true);
            pieChart.getDescription().setEnabled(false);
            pieChart.setTransparentCircleRadius(0f);
            pieChart.getLegend().setEnabled(false);
            pieChart.setDrawEntryLabels(false);
            pieChart.setEntryLabelTextSize(11f);
            pieChart.setHoleRadius(53f);

            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    pieChart.setDrawEntryLabels(true);
                }

                @Override
                public void onNothingSelected() {
                    pieChart.setDrawEntryLabels(false);
                }
            });

            // Configure PieChart
            List<PieEntry> pieEntriesMale = new ArrayList<>();
            for (int malePercent : percents) {
                pieEntriesMale.add(new PieEntry(malePercent, malePercent + "%"));
            }

            PieDataSet pieDataSet = new PieDataSet(pieEntriesMale, StringUtils.EMPTY_STRING);
            pieDataSet.setColors(colorNope, colorNeutral, colorYeah);
            PieData pieData = new PieData(pieDataSet);
            pieData.setDrawValues(false);
            pieChart.setData(pieData);
        }

        private void configureMultiProgressBar(final MultiProgressBar mpb, int nope, int neutral, int yeah, boolean show) {
            mpb.setTouchable(!show);
            mpb.showLabel(show);
            mpb.getThumb().mutate().setAlpha(0);
            ArrayList<ProgressItem> progressItemList = new ArrayList<>();
            // Nope
            ProgressItem mProgressItem = new ProgressItem(colorNope, colorWhite, answer_nope, nope);
            progressItemList.add(mProgressItem);

            // Neutral
            mProgressItem = new ProgressItem(colorNeutral, colorNeutralProgressLabel, answer_neutral, neutral);
            progressItemList.add(mProgressItem);
            // Yeah
            mProgressItem = new ProgressItem(colorYeah, colorWhite, answer_yeah, yeah);
            progressItemList.add(mProgressItem);

            mpb.initData(progressItemList);
            mpb.invalidate();
        }
    }

    public interface Callback {
        void onJoinChatClick();
    }
}