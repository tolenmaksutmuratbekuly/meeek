package systems.hexa.meek.ui.questions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.questions.QuestionPresenter;
import systems.hexa.meek.mvp.questions.QuestionView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.base.recycler_view.ListNoDataViewHolder;
import systems.hexa.meek.ui.nearby.NearByActivity;
import systems.hexa.meek.ui.profile.SettingsActivity;
import systems.hexa.meek.ui.questions.ask_question.AskQuestionActivity;
import systems.hexa.meek.ui.questions.detail.QuestionDetailActivity;
import systems.hexa.meek.utils.LOTimber;
import systems.hexa.meek.utils.ViewUtils;
import systems.hexa.meek.utils.recycler_view.EndlessScrollListener;
import systems.hexa.meek.utils.recycler_view.SwipeRefreshUtils;
import systems.hexa.meek.views.circlemenu.CircleMenu;

import static systems.hexa.meek.utils.Constants.DESCRIPTION;
import static systems.hexa.meek.utils.Constants.TYPE;
import static systems.hexa.meek.utils.Constants.TYPE_CITY;
import static systems.hexa.meek.utils.Constants.TYPE_COUNTRY;
import static systems.hexa.meek.utils.Constants.TYPE_FRIEND;
import static systems.hexa.meek.utils.Constants.TYPE_GROUP;
import static systems.hexa.meek.utils.Constants.TYPE_WORLD;

public class QuestionsActivity extends BaseActivity1 implements QuestionView {
    private QuestionPresenter mvpPresenter;

    @BindView(R.id.rl_screen) RelativeLayout rl_screen;
    @BindView(R.id.img_screen) ImageView img_screen;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_logo_toolbar) ImageView img_logo_toolbar;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindView(R.id.list_placeholder_stub) ViewStub list_placeholder_stub;
    @BindView(R.id.circle_menu) CircleMenu circle_menu;

    @BindView(R.id.img_type) ImageView img_type;
    @BindView(R.id.tv_type) TextView tv_type;
    @BindView(R.id.tv_splash_desc) TextView tv_splash_desc;
    private int type;
    private ListNoDataViewHolder listPlaceholderViewHolder;
    private EndlessScrollListener endlessScrollListener;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_questions;
    }

    public static Intent getIntent(Context context, int type, String description) {
        Intent intent = new Intent(context, QuestionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        bundle.putString(DESCRIPTION, description);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent alreadyAuthorized(Context context, int type, String description) {
        Intent intent = new Intent(context, QuestionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        bundle.putString(DESCRIPTION, description);
        intent.putExtras(bundle);
        return intent;
    }

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mvpPresenter = new QuestionPresenter(dataLayer);
        mvpPresenter.attachView(this);

        FirebaseUser user = dataLayer.getFirebaseAuth().getCurrentUser();
        if (user != null) {
            LOTimber.d("salkdjasd token=" +
                    user.getUid() + "|" +
                    user.getDisplayName() + "|" +
                    user.getEmail());
        }

        handleIntent(getIntent());
        configureRecyclerView();
        mvpPresenter.loadMoreItems();
        configureCircleMenu();
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            type = bundle.getInt(TYPE);
            String description = bundle.getString(DESCRIPTION);
            mvpPresenter.showSplashScreen(type, description);
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        endlessScrollListener = null;
        recycler_view.clearOnScrollListeners();
        recycler_view.setAdapter(null);
        swipe_refresh_layout.setOnRefreshListener(null);
        listPlaceholderViewHolder = null;
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_questions, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                menuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchView.setOnSearchClickListener(v -> {
            img_logo_toolbar.setVisibility(View.GONE);
            setItemsVisibility(menu, menuItem, false);
        });
        searchView.setOnCloseListener(() -> {
            img_logo_toolbar.setVisibility(View.VISIBLE);
            setItemsVisibility(menu, menuItem, true);
            return false;
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(SettingsActivity.getIntent(this));
        }
//        else if (id == R.id.action_notify) {
//
//        }
        return super.onOptionsItemSelected(item);

    }

    private void configureRecyclerView() {
        QuestionsAdapter promoListAdapter = new QuestionsAdapter(this);
        promoListAdapter.setCallback(new QuestionsAdapter.Callback() {
            @Override
            public void onItemClick(QuestionsFeed questionsFeed, boolean answered) {
                startActivity(QuestionDetailActivity.getIntent(getContext(), questionsFeed.getAnswerType(), answered));
            }

            @Override
            public void qChangeListener() {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setAdapter(promoListAdapter);

        endlessScrollListener = new EndlessScrollListener(recycler_view) {
            @Override
            public void onRequestMoreItems() {
//                mvpPresenter.loadMoreItems();
            }
        };
        recycler_view.addOnScrollListener(endlessScrollListener);
        SwipeRefreshUtils.setColorSchemeColors(this, swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
    }

    private void configureListPlaceholder() {
        listPlaceholderViewHolder.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
        listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(this);
    }

    private void configureCircleMenu() {
        circle_menu.setOnItemClickListener(menuButton -> {
            circle_menu.setVisibility(View.INVISIBLE);
            int id = menuButton.getId();
            if (id == R.id.cmb_world) {
                openQuestionsActivity(TYPE_WORLD, getString(R.string.t_world_desc));
            } else if (id == R.id.cmb_city) {
                openQuestionsActivity(TYPE_CITY, getString(R.string.t_city_desc));
            } else if (id == R.id.cmb_country) {
                openQuestionsActivity(TYPE_COUNTRY, getString(R.string.t_country_desc));
            } else if (id == R.id.cmb_nearby) {
                img_type.setImageResource(R.drawable.ic_float_nearby);
                tv_type.setText(getString(R.string.t_nearby));
                startActivity(NearByActivity.getIntent(getContext()));
            } else if (id == R.id.cmb_group) {
                openQuestionsActivity(TYPE_GROUP, getString(R.string.t_group_desc));
            } else if (id == R.id.cmb_friend) {
                openQuestionsActivity(TYPE_FRIEND, null);
            }
        });

        circle_menu.setStateUpdateListener(new CircleMenu.OnStateUpdateListener() {
            @Override
            public void onMenuExpanded() {

            }

            @Override
            public void onMenuCollapsed() {
                circle_menu.setVisibility(View.INVISIBLE);
            }
        });
        circle_menu.setVisibility(View.INVISIBLE);
    }

    private void openQuestionsActivity(int type, String desc) {
        if (this.type != type) {
            startActivity(QuestionsActivity.getIntent(getContext(), type, desc));
        }
    }

    @OnClick(R.id.ll_type)
    void onMenuOpen() {
        circle_menu.setVisibility(View.VISIBLE);
        circle_menu.onCenterButtonClick();
    }

    @OnClick(R.id.cv_new_question)
    void onCreateNewQuestionClick() {
        startActivity(AskQuestionActivity.getIntent(this));
    }

    ///////////////////////////////////////////////////////////////////////////
    // QuestionView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        recycler_view.post(() -> ((QuestionsAdapter) recycler_view.getAdapter()).setLoading(show));
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipe_refresh_layout.setRefreshing(show);
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setRefreshing(show);
        }
    }

    @Override
    public void addQuestionListItems(List<QuestionsFeed> promotions) {
        recycler_view.post(() -> ((QuestionsAdapter) recycler_view.getAdapter()).addQuestions(promotions));
        recycler_view.post(needMoreItemsChecker);
    }

    @Override
    public void showListPlaceholder(@StringRes int textRes) {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListNoDataViewHolder(list_placeholder_stub.inflate());
            configureListPlaceholder();
        }
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaceholder() {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        recycler_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSplashScreen(int type, String description) {
        rl_screen.setVisibility(View.VISIBLE);
        tv_splash_desc.setText(description);
        if (type == TYPE_WORLD)
            img_screen.setImageResource(R.drawable.ic_type_world);
        else if (type == TYPE_COUNTRY)
            img_screen.setImageResource(R.drawable.ic_type_country);
        else if (type == TYPE_CITY)
            img_screen.setImageResource(R.drawable.ic_type_city);
        else if (type == TYPE_GROUP)
            img_screen.setImageResource(R.drawable.ic_type_group);

        ViewUtils.goneView(rl_screen, 2800);
    }

    @Override
    public void setFloatButton(int type) {
        if (type == TYPE_WORLD) {
            img_type.setImageResource(R.drawable.ic_float_world);
            tv_type.setText(getString(R.string.t_world));
        } else if (type == TYPE_COUNTRY) {
            img_type.setImageResource(R.drawable.ic_float_country);
            tv_type.setText(getString(R.string.t_country));
        } else if (type == TYPE_CITY) {
            img_type.setImageResource(R.drawable.ic_float_city);
            tv_type.setText(getString(R.string.t_city));
        } else if (type == TYPE_GROUP) {
            img_type.setImageResource(R.drawable.ic_float_group);
            tv_type.setText(getString(R.string.t_group));
        } else if (type == TYPE_FRIEND) {
            img_type.setImageResource(R.drawable.ic_float_friends);
            tv_type.setText(getString(R.string.t_friend));
        }
    }
}