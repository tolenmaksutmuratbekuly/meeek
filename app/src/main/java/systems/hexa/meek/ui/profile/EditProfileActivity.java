package systems.hexa.meek.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.profile.EditProfilePresenter;
import systems.hexa.meek.mvp.profile.EditProfileView;
import systems.hexa.meek.ui.base.BaseActivity1;

public class EditProfileActivity extends BaseActivity1 implements EditProfileView {
    @BindView(R.id.toolbar) Toolbar toolbar;

    private EditProfilePresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_edit_profile;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, EditProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new EditProfilePresenter(dataLayer);
        mvpPresenter.attachView(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    @OnClick(R.id.tv_cancel)
    void goBackStack() {
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // EditProfileView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }
}