package systems.hexa.meek.ui.base;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.BaseMvpView;
import systems.hexa.meek.utils.SnackbarUtils;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {
    protected DataLayer dataLayer;
    @BindView(R.id.fl_parent) protected FrameLayout fl_parent;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;

    protected abstract int getLayoutResourceId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResourceId() != -1) setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        dataLayer = DataLayer.getInstance(this);
    }

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_parent, errorMessage);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onInvalidToken() {
    }
}
