package systems.hexa.meek.ui.auth.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.User;
import systems.hexa.meek.mvp.auth.login.LoginPresenter;
import systems.hexa.meek.mvp.auth.login.LoginView;
import systems.hexa.meek.ui.auth.PageSwapCallback;
import systems.hexa.meek.ui.auth.forgot_pwd.ForgotPwdActivity;
import systems.hexa.meek.ui.base.BaseFragment;
import systems.hexa.meek.ui.questions.QuestionsActivity;

import static systems.hexa.meek.utils.Constants.TYPE_WORLD;
import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class LoginFragment extends BaseFragment implements LoginView {
    @InjectPresenter
    LoginPresenter mvpPresenter;

    private PageSwapCallback callback;
    @BindView(R.id.et_email) MaterialEditText et_email;
    @BindView(R.id.et_password) MaterialEditText et_password;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_login;
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.onRestoreInstanceState(savedInstanceState);
        mvpPresenter.init(dataLayer);
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mvpPresenter.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    @OnClick(R.id.btn_login)
    void onLoginClick() {
        clearFocusFromAllViews(fl_parent);
        hideSoftKeyboard(fl_parent);
        showErrIcon(et_email, 0);
        showErrIcon(et_password, 0);
        mvpPresenter.setEmail(et_email.getText().toString());
        mvpPresenter.setPwd(et_password.getText().toString());
        mvpPresenter.signInWithEmailAndPassword();
    }

    @OnClick(R.id.btn_signup)
    void onSignUpClick() {
        if (callback != null) {
            callback.onSignUpClick();
        }
    }

    @OnClick(R.id.tv_forgot_pwd)
    void onForgotPwdClick() {
        startActivity(ForgotPwdActivity.getIntent(getContext()));
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // LoginView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setEmail(String email) {
        et_email.setText(email);
    }

    @Override
    public void setPwd(String pwd) {
        et_password.setText(pwd);
    }

    @Override
    public void showEmailError(@StringRes int errorRes) {
        showInputFieldError(et_email, errorRes);
        showErrIcon(et_email, R.drawable.err_input_require);
    }

    @Override
    public void showPwdError(@StringRes int errorRes) {
        showInputFieldError(et_password, errorRes);
        showErrIcon(et_password, R.drawable.err_input_require);
    }

    @Override
    public void onSuccessAuthorized(User user) {
        startActivity(QuestionsActivity.getIntent(getContext(), TYPE_WORLD, getString(R.string.t_world_desc)));
    }
}