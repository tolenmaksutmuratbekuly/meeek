package systems.hexa.meek.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.profile.ProfilePresenter;
import systems.hexa.meek.mvp.profile.ProfileView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.base.view_pager.ViewPagerAdapter;
import systems.hexa.meek.ui.questions.ask_question.SelectLocationActivity;

public class ProfileActivity extends BaseActivity1 implements ProfileView {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.pager) ViewPager viewPager;

    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinator_layout;

    private ProfilePresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new ProfilePresenter(dataLayer);
        mvpPresenter.attachView(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        configureViewPager();
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void goBackStack() {
        finish();
    }

    @OnClick(R.id.cv_edit_profile)
    void onEditProfileClick() {
        startActivity(EditProfileActivity.getIntent(this));
    }

    private void configureViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(QuestionsTabFragment.newInstance(), getString(R.string.tab_responses));
        adapter.addFrag(QuestionsTabFragment.newInstance(), getString(R.string.tab_saved));
        adapter.addFrag(QuestionsTabFragment.newInstance(), getString(R.string.tab_questions));
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ProfileView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openSelectLocationActivity() {
        startActivity(SelectLocationActivity.getIntent(this));
    }
}