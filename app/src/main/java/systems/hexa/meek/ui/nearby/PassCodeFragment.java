package systems.hexa.meek.ui.nearby;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.nearby.PassCodePresenter;
import systems.hexa.meek.mvp.nearby.PassCodeView;
import systems.hexa.meek.ui.base.BaseFragment1;
import systems.hexa.meek.utils.StringUtils;
import systems.hexa.meek.utils.animation.AccessCodeAnimationUtils;

public class PassCodeFragment extends BaseFragment1 implements PassCodeView {
    private PassCodePresenter mvpPresenter;
    private PageSwapCallback callback;

    private static final String TEMP_ACCESS_CODE = "1994";

    @BindView(R.id.ll_rbs) LinearLayout ll_rbs;
    @BindView(R.id.rb_number_1) RadioButton rb_number_1;
    @BindView(R.id.rb_number_2) RadioButton rb_number_2;
    @BindView(R.id.rb_number_3) RadioButton rb_number_3;
    @BindView(R.id.rb_number_4) RadioButton rb_number_4;
    @BindView(R.id.et_accesscode) EditText et_accesscode;
    @BindView(R.id.tv_number) TextView tv_number;

    private Animation access_code_error, access_code_sleep;

    @BindView(R.id.ll_content) LinearLayout ll_content;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_pass_code;
    }

    public static PassCodeFragment newInstance(PageSwapCallback callback) {
        PassCodeFragment fragment = new PassCodeFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new PassCodePresenter(dataLayer);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        assert v != null;
        ButterKnife.bind(this, v);
        mvpPresenter.attachView(this);

        initViews();
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mvpPresenter.detachView();
    }

    private void initViews() {
        et_accesscode.requestFocus();

        // Animations
        AccessCodeAnimationUtils.setEditText(et_accesscode);
        access_code_error = AccessCodeAnimationUtils.loadAnimation(getContext(), R.anim.pincode_error);
        access_code_sleep = AccessCodeAnimationUtils.loadAnimation(getContext(), R.anim.pincode_sleep);
        access_code_error.setAnimationListener(AccessCodeAnimationUtils.setAnimationListener());
        access_code_sleep.setAnimationListener(AccessCodeAnimationUtils.setAnimationListener());
    }

    @OnClick({
            R.id.tv_1,
            R.id.tv_2,
            R.id.tv_3,
            R.id.tv_4,
            R.id.tv_5,
            R.id.tv_6,
            R.id.tv_7,
            R.id.tv_8,
            R.id.tv_9,
            R.id.tv_0})
    void onNumberClick(TextView textView) {
        et_accesscode.append(textView.getText());
    }

    @OnClick(R.id.tv_c)
    void onClearClick() {
        et_accesscode.setText(StringUtils.EMPTY_STRING);
    }

    @OnClick(R.id.img_backspace)
    void onBackspaceClick() {
        mvpPresenter.onBackspaceClick(et_accesscode.getText().toString());
    }

    @OnTextChanged(R.id.et_accesscode)
    void onTextChangedAccessCode() {
        String accessCode = et_accesscode.getText().toString();
        if (accessCode.length() >= 0 && accessCode.length() <= 4) {
            switch (accessCode.length()) {
                case 0:
                    onTextLength(false, false, false, false, accessCode);
                    break;
                case 1:
                    onTextLength(true, false, false, false, accessCode);
                    break;
                case 2:
                    onTextLength(true, true, false, false, accessCode);
                    break;
                case 3:
                    onTextLength(true, true, true, false, accessCode);
                    break;
                case 4:
                    onTextLength(true, true, true, true, accessCode);
                    validateAccessCode(accessCode);
                    break;
            }
        }
    }

    private void onTextLength(boolean rb1, boolean rb2, boolean rb3, boolean rb4, String accessCode) {
        tv_number.setText(accessCode);
        rb_number_1.setVisibility(rb1 ? View.GONE : View.VISIBLE);
        rb_number_2.setVisibility(rb2 ? View.GONE : View.VISIBLE);
        rb_number_3.setVisibility(rb3 ? View.GONE : View.VISIBLE);
        rb_number_4.setVisibility(rb4 ? View.GONE : View.VISIBLE);
    }

    private void validateAccessCode(String accessCode) {
        if (accessCode.equals(TEMP_ACCESS_CODE)) {
            callback.onCorrectPassCode();
        } else {
            ll_rbs.startAnimation(access_code_error);
        }
    }

    @Override
    public void setBackspaceResult(String accessCode) {
        et_accesscode.setText(accessCode);
    }

    ///////////////////////////////////////////////////////////////////////////
    // PassCodeView implementation
    ///////////////////////////////////////////////////////////////////////////

//    @Override
//    public void successSent() {
//        if (callback != null) {
//            callback.onCorrectPassCode();
//        }
//    }
}