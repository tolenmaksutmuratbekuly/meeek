package systems.hexa.meek.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.network.exceptions.APIException;
import systems.hexa.meek.data.network.exceptions.ConnectionTimeOutException;
import systems.hexa.meek.data.network.exceptions.UnknownException;
import systems.hexa.meek.mvp.MVPView;

public abstract class BaseActivity1 extends AppCompatActivity implements MVPView {
    protected DataLayer dataLayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResourceId() != -1) setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        dataLayer = DataLayer.getInstance(this);
    }

    protected abstract int getLayoutResourceId();

    @Override
    public void showProgressView(boolean show) {

    }

    @Override
    public void showProgressViewMsg(boolean show, String msg) {

    }

    @Override
    public void showResultView(boolean success) {

    }

    @Override
    public void showResultViewMsg(boolean success, String msg) {

    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void dismissProgressDialog() {

    }

    @Override
    public void handleAPIException(APIException e) {

    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {

    }

    @Override
    public void handleUnknownException(UnknownException e) {

    }

    @Override
    public void showErrorDialog(String message) {

    }

    @Override
    public String getUnknownExceptionMessage() {
        return null;
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return null;
    }
}