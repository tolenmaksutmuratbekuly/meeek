package systems.hexa.meek.ui.nearby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import butterknife.BindView;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.nearby.NearByPresenter;
import systems.hexa.meek.mvp.nearby.NearByView;
import systems.hexa.meek.ui.base.BaseFragmentActivity1;
import systems.hexa.meek.ui.questions.detail.DetailFragment;
import systems.hexa.meek.ui.questions.detail.ResultFragment;
import systems.hexa.meek.utils.ColorUtils;

public class NearByActivity extends BaseFragmentActivity1 implements NearByView, PageSwapCallback {
    private NearByPresenter mvpPresenter;
    private DetailFragment detailFragment;
    private ResultFragment resultFragment;
    @BindView(R.id.frgm_container) FrameLayout frgm_container;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_framelayout;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, NearByActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new NearByPresenter(dataLayer);
        mvpPresenter.attachView(this);

        frgm_container.setBackground(ColorUtils.getGradient(this, R.color.nearby_gradient_1, R.color.nearby_gradient_2));

        if (savedInstanceState == null) {
            PassCodeFragment pwdCodeFragment = PassCodeFragment.newInstance(this);
            replaceFragment(pwdCodeFragment, false);
        }
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // NearByView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openMainScreen() {
//        startActivity(MainActivity.getIntent(this, false));
//        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PageSwapCallback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onCorrectPassCode() {
        if (detailFragment == null)
            detailFragment = DetailFragment.newInstance(this);
        replaceFragment(detailFragment, false);
    }
}
