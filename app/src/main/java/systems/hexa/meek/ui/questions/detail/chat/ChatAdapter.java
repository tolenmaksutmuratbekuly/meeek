package systems.hexa.meek.ui.questions.detail.chat;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.detail.chat.ChatMessage;
import systems.hexa.meek.ui.base.recycler_view.RecyclerViewBaseAdapter;

class ChatAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = R.layout.adapter_chat_receive;
    private static final int VIEW_TYPE_MESSAGE_SENT = R.layout.adapter_chat_send;
    private Context context;
    private List<ChatMessage> mMessageList;

    ChatAdapter(Context context) {
        this.context = context;
        this.mMessageList = new ArrayList<>();
    }

    void addMessages(List<ChatMessage> messageList) {
        int positionStart = mMessageList.size();
        int itemCount = messageList.size();
        mMessageList.addAll(messageList);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = mMessageList.get(position);
        if (message.getUserId() == ChatMessage.LOCAL_USER_ID) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_MESSAGE_SENT:
                return new SentMessageHolder(inflate(parent, VIEW_TYPE_MESSAGE_SENT));
            case VIEW_TYPE_MESSAGE_RECEIVED:
                return new ReceivedMessageHolder(inflate(parent, VIEW_TYPE_MESSAGE_RECEIVED));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(mMessageList.get(position), position);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(mMessageList.get(position), position);
                break;
        }
    }

    class SentMessageHolder extends MainViewHolder {
        @BindView(R.id.tv_message) TextView tv_message;
        ChatMessage bindedObject;
        int bindedPosition;

        SentMessageHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(ChatMessage object, int position) {
            bindedObject = object;
            bindedPosition = position;

            if (object == null) {
                return;
            }

            tv_message.setText(object.getMessage());
        }
    }

    class ReceivedMessageHolder extends MainViewHolder {
        @BindView(R.id.tv_message) TextView tv_message;
        ChatMessage bindedObject;
        int bindedPosition;

        ReceivedMessageHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(ChatMessage object, int position) {
            bindedObject = object;
            bindedPosition = position;

            if (object == null) {
                return;
            }
        }

        @OnClick(R.id.tv_message)
        void onReactionAnimationClick() {

        }
    }
}
