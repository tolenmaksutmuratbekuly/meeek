package systems.hexa.meek.ui.auth.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import org.parceler.Parcels;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.social.facebook.FacebookProvider;
import systems.hexa.meek.data.social.google.GoogleAuthProvider;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.auth.login.StartPresenter;
import systems.hexa.meek.mvp.auth.login.StartView;
import systems.hexa.meek.ui.auth.PageSwapCallback;
import systems.hexa.meek.ui.auth.intro.IntroActivity;
import systems.hexa.meek.ui.base.BaseFragment;
import systems.hexa.meek.utils.SpannableStringUtils;
import systems.hexa.meek.utils.ToastUtils;

import static systems.hexa.meek.utils.Constants.FORM_KEY;

public class StartFragment extends BaseFragment implements StartView {
    @InjectPresenter
    StartPresenter mvpPresenter;

    private PageSwapCallback callback;
    private FacebookProvider facebookProvider;
    private GoogleAuthProvider googleAuthProvider;

    @BindView(R.id.tv_privacy_policy) TextView tv_privacy_policy;
    @BindString(R.string.label_signing_terms) String label_signing_terms;
    @BindString(R.string.terms_of_services) String terms_of_services;
    @BindString(R.string.privacy_policy) String privacy_policy;
    @BindColor(R.color.btn_border) int btn_border;

    public static StartFragment newInstance(AuthFormValidation authForm) {
        StartFragment fragment = new StartFragment();
        Bundle data = new Bundle();
        data.putParcelable(FORM_KEY, Parcels.wrap(authForm));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_start;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        // Social networks Providers
        facebookProvider = new FacebookProvider(this);
        googleAuthProvider = new GoogleAuthProvider(this);
        handleIntent();
        privacyPolicyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        googleAuthProvider.disconnect();
        mvpPresenter.performLogout();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebookProvider.onActivityResult(requestCode, resultCode, data);
        googleAuthProvider.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    private void handleIntent() {
        if (getArguments() != null && getArguments().containsKey(FORM_KEY)) {
            AuthFormValidation form = Parcels.unwrap(getArguments().getParcelable(FORM_KEY));
            mvpPresenter.init(dataLayer, facebookProvider, googleAuthProvider, form);
        }
    }

    private void privacyPolicyView() {
        String fullTxt = getString(R.string.click_privacy_policy, label_signing_terms, terms_of_services, privacy_policy);
        SpannableString spannableStr = new SpannableString(fullTxt);

        SpannableStringUtils.underlineClickable(() ->
                ToastUtils.showCenteredToast(getContext(), terms_of_services + " clicked", 1000), spannableStr, label_signing_terms.length() + 1, label_signing_terms.length() + terms_of_services.length() + 1, btn_border);

        SpannableStringUtils.underlineClickable(() ->
                ToastUtils.showCenteredToast(getContext(), privacy_policy + " clicked", 1000), spannableStr, fullTxt.length() - privacy_policy.length(), fullTxt.length(), btn_border);

        tv_privacy_policy.setText(spannableStr);
        tv_privacy_policy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @OnClick(R.id.btn_login)
    void onLoginWithEmailClick() {
        if (callback != null) {
            callback.onLoginWithEmailClick();
        }
    }

    @OnClick(R.id.btn_fb)
    void onFacebookButtonClick() {
        mvpPresenter.performFbAuth();
    }

    @OnClick(R.id.btn_gplus)
    void onGooglePlusButtonClick() {
        mvpPresenter.performGPlusAuth();
    }

    @OnClick(R.id.tv_skip_login)
    void onSkipLoginClick() {
        startActivity(IntroActivity.getIntent(getContext()));
    }

    ///////////////////////////////////////////////////////////////////////////
    // StartView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onFacebookSdkError() {
        ToastUtils.showToast(getContext(), R.string.error_message_login_facebook);
    }

    @Override
    public void onEmailPermissionNotGranted() {
        ToastUtils.showToast(getContext(), R.string.error_message_login_email_permission);
    }

    @Override
    public void onGoogleApiConnectingError() {
        ToastUtils.showToast(getContext(), R.string.error_message_login_google_connecting);
    }

    @Override
    public void onGoogleApiNotConnectedError() {
        ToastUtils.showToast(getContext(), R.string.error_message_login_google_not_connected);
    }

    @Override
    public void onGoogleSdkError() {
        ToastUtils.showToast(getContext(), R.string.error_message_login_google_sdk_error);
    }

    @Override
    public void onRegistrationSuccess() {
        if (callback != null) {
            callback.onRegistrationSuccess();
        }
    }

    @Override
    public void onAuthorizedGPlus(AuthFormValidation authForm) {
        if (callback != null) {
            callback.openSignUpDetail(authForm, true);
        }
    }
}