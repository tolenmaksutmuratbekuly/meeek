package systems.hexa.meek.ui.questions.ask_question;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.new_question.GradientColor;
import systems.hexa.meek.utils.ColorUtils;

class GradientColorsAdapter extends BaseAdapter {
    private Context context;
    private static final int DEFAULT_GRADIENT_SIZE = 6;

    private List<GradientColor> colorList = new ArrayList<>();
    private Callback callback;
    private boolean showAll;

    GradientColorsAdapter(Context context) {
        this.context = context;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    void addColors(List<GradientColor> newFavorites) {
        if (newFavorites == null || newFavorites.isEmpty()) {
            colorList = new ArrayList<>();
        } else {
            colorList = new ArrayList<>(newFavorites);
        }
        notifyDataSetChanged();
    }

    private void setShowAllColors(boolean showAll) {
        this.showAll = showAll;
    }

    @Override
    public int getCount() {
        return showAll ? colorList.size() : DEFAULT_GRADIENT_SIZE + 1;
    }

    @Override
    public Object getItem(int position) {
        return colorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_gradient_colors, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.bindHolder(colorList.get(position), position);

        return convertView;
    }

    class ViewHolder {
        GradientColor bindedObject;
        int bindedPosition;
        @BindView(R.id.v_grad_color) View v_grad_color;
        @BindView(R.id.img_more) ImageView img_more;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bindHolder(GradientColor object, int position) {
            bindedObject = object;
            bindedPosition = position;
            if (object == null) {
                return;
            }

            if (!showAll && position == getCount() - 1) {
                img_more.setVisibility(View.VISIBLE);
                v_grad_color.setVisibility(View.GONE);
            } else {
                img_more.setVisibility(View.GONE);
                v_grad_color.setVisibility(View.VISIBLE);
                v_grad_color.setBackground(ColorUtils.getGradient(context, object.colors));
            }
        }

        @OnClick(R.id.img_more)
        void onClickMore() {
            setShowAllColors(true);
            notifyDataSetChanged();
        }

        @OnClick(R.id.v_grad_color)
        void onGradColorClick() {
            if (callback != null) {
                callback.onColorClick(bindedObject.colors, bindedPosition);
            }
        }
    }

    interface Callback {
        void onColorClick(int[] colors, int position);
    }
}
