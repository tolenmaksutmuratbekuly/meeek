package systems.hexa.meek.ui.auth.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.auth.signup.SignUpFinishPresenter;
import systems.hexa.meek.mvp.auth.signup.SignUpFinishView;
import systems.hexa.meek.ui.auth.PageSwapCallback;
import systems.hexa.meek.ui.base.BaseFragment;

import static systems.hexa.meek.utils.Constants.FORM_KEY;
import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class SignUpFinishFragment extends BaseFragment implements SignUpFinishView {
    @InjectPresenter
    SignUpFinishPresenter mvpPresenter;
    private PageSwapCallback callback;

    @BindView(R.id.ll_content) LinearLayout ll_content;
    @BindView(R.id.et_email) MaterialEditText et_email;
    @BindView(R.id.et_confirm_email) MaterialEditText et_confirm_email;
    @BindView(R.id.et_password) MaterialEditText et_password;

    public static SignUpFinishFragment newInstance(AuthFormValidation form) {
        SignUpFinishFragment fragment = new SignUpFinishFragment();
        Bundle data = new Bundle();
        data.putParcelable(FORM_KEY, Parcels.wrap(form));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_signup_finish;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        handleIntent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    private void handleIntent() {
        if (getArguments() != null && getArguments().containsKey(FORM_KEY)) {
            AuthFormValidation form = Parcels.unwrap(getArguments().getParcelable(FORM_KEY));
            if (form != null) {
                mvpPresenter.init(dataLayer, form);
            }
        }
    }

    @OnClick(R.id.btn_finish)
    void onFinishClick() {
        clearFocusFromAllViews(ll_content);
        hideSoftKeyboard(ll_content);
        showErrIcon(et_email, 0);
        showErrIcon(et_confirm_email, 0);
        showErrIcon(et_password, 0);
        mvpPresenter.setEmail(et_email.getText().toString());
        mvpPresenter.setConfirmEmail(et_confirm_email.getText().toString());
        mvpPresenter.setPwd(et_password.getText().toString());
        mvpPresenter.validate();
    }

    @OnEditorAction(R.id.et_password)
    boolean onPwdInputAction(int action) {
        if (action == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyboard(et_password);
            clearFocusFromAllViews(ll_content);
            return true;
        }
        return false;
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // SignUpFinishView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void setEmail(String email) {
        et_email.setText(email);
    }

    @Override
    public void setPwd(String pwd) {
        et_password.setText(pwd);
    }

    @Override
    public void showEmailError(@StringRes int errorRes) {
        showInputFieldError(et_email, errorRes);
        showErrIcon(et_email, R.drawable.err_input_require);
    }

    @Override
    public void showConfirmEmailError(@StringRes int errorRes) {
        showInputFieldError(et_confirm_email, errorRes);
        showErrIcon(et_confirm_email, R.drawable.err_input_require);
    }

    @Override
    public void showPwdError(@StringRes int errorRes) {
        showInputFieldError(et_password, errorRes);
        showErrIcon(et_password, R.drawable.err_input_require);
    }

    @Override
    public void onRegistrationSuccess() {
        if (callback != null) {
            callback.onRegistrationSuccess();
        }
    }
}