package systems.hexa.meek.ui.auth.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.views.circlemenu.CircleMenu;

public class IntroFragment3 extends Fragment {
    @BindView(R.id.circle_menu) CircleMenu circle_menu;

    public static IntroFragment3 newInstance() {
        return new IntroFragment3();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgm_intro3, container, false);
        ButterKnife.bind(this, view);
        circle_menu.showMenu(true);
        return view;
    }


}
