package systems.hexa.meek.ui.questions.ask_question.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.EditText;
import android.widget.FrameLayout;

import java.util.List;

import butterknife.BindView;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.data.models.questions.new_question.Friends;
import systems.hexa.meek.mvp.questions.ask_question.friends.FriendsPresenter;
import systems.hexa.meek.mvp.questions.ask_question.friends.FriendsView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.base.recycler_view.ListNoDataViewHolder;
import systems.hexa.meek.utils.ColorUtils;
import systems.hexa.meek.utils.recycler_view.EndlessScrollListener;
import systems.hexa.meek.utils.recycler_view.SwipeRefreshUtils;
import systems.hexa.meek.views.dialogs.CommonDialog;

public class FriendsActivity extends BaseActivity1 implements FriendsView {
    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.list_placeholder_stub) ViewStub list_placeholder_stub;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fl_content) FrameLayout fl_content;
    private FriendsPresenter mvpPresenter;
    private EndlessScrollListener endlessScrollListener;
    private ListNoDataViewHolder listPlaceholderViewHolder;
    private FriendsAdapter adapter;
    private CommonDialog dialog;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_friends;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, FriendsActivity.class);
    }

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new FriendsPresenter(dataLayer);
        mvpPresenter.attachView(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        fl_content.setBackground(ColorUtils.getGradient(this, R.color.nearby_gradient_1, R.color.nearby_gradient_2));
        dialog = new CommonDialog(this);
        configureRecyclerView();
        mvpPresenter.loadMoreItems();
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
//        mvpPresenter.setErrorHandler(null);
        endlessScrollListener = null;
        recycler_view.clearOnScrollListeners();
        recycler_view.setAdapter(null);
        swipe_refresh_layout.setOnRefreshListener(null);
        listPlaceholderViewHolder = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_friends, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        EditText searchEditText = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(ContextCompat.getColor(this, R.color.white));
        searchEditText.setHintTextColor(ContextCompat.getColor(this, R.color.white));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                menuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.setUsers(true);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void configureRecyclerView() {
        adapter = new FriendsAdapter(this);
        adapter.setUsers(false);
        adapter.setCallback(new FriendsAdapter.Callback() {
            @Override
            public void onItemClick(QuestionsFeed questionsFeed, boolean answered) {

            }

            @Override
            public void onAddClick(Friends friends) {
                dialog.addFriendDialog();
                dialog.setCallbackYesNo(new CommonDialog.CallbackYesNo() {
                    @Override
                    public void onClickYes() {
                        friends.setChecked(true);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onClickNo() {

                    }
                });
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setAdapter(adapter);

        endlessScrollListener = new EndlessScrollListener(recycler_view) {
            @Override
            public void onRequestMoreItems() {
//                mvpPresenter.loadMoreItems();
            }
        };
        recycler_view.addOnScrollListener(endlessScrollListener);
        SwipeRefreshUtils.setColorSchemeColors(this, swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
    }

    private void configureListPlaceholder() {
        listPlaceholderViewHolder.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
        listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(this);
    }

    ///////////////////////////////////////////////////////////////////////////
    // FriendsView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        recycler_view.post(() -> ((FriendsAdapter) recycler_view.getAdapter()).setLoading(show));
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipe_refresh_layout.setRefreshing(show);
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setRefreshing(show);
        }
    }

    @Override
    public void addFriendsListItems(List<Friends> friendsList) {
        recycler_view.post(() -> ((FriendsAdapter) recycler_view.getAdapter()).addFriends(friendsList));
        recycler_view.post(needMoreItemsChecker);
    }

    @Override
    public void showListPlaceholder(@StringRes int textRes) {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListNoDataViewHolder(list_placeholder_stub.inflate());
            configureListPlaceholder();
        }
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaceholder() {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        recycler_view.setVisibility(View.VISIBLE);
    }
}