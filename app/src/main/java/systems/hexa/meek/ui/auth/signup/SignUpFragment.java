package systems.hexa.meek.ui.auth.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.auth.AuthFormValidation;
import systems.hexa.meek.mvp.auth.signup.SignUpPresenter;
import systems.hexa.meek.mvp.auth.signup.SignUpView;
import systems.hexa.meek.ui.auth.PageSwapCallback;
import systems.hexa.meek.ui.base.BaseFragment;

import static systems.hexa.meek.utils.Constants.FORM_KEY;
import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class SignUpFragment extends BaseFragment implements SignUpView {
    @InjectPresenter
    SignUpPresenter mvpPresenter;
    private PageSwapCallback callback;
    @BindView(R.id.et_firstname) MaterialEditText et_firstname;
    @BindView(R.id.et_lastname) MaterialEditText et_lastname;

    public static SignUpFragment newInstance(AuthFormValidation form) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle data = new Bundle();
        data.putParcelable(FORM_KEY, Parcels.wrap(form));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_signup;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        handleIntent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    @OnClick(R.id.btn_next)
    void onBtnNextClick() {
        clearFocusFromAllViews(fl_parent);
        hideSoftKeyboard(fl_parent);
        showErrIcon(et_firstname, 0);
        showErrIcon(et_lastname, 0);
        mvpPresenter.setFirstName(et_firstname.getText().toString());
        mvpPresenter.setLastName(et_lastname.getText().toString());
        mvpPresenter.validate();
    }

    private void handleIntent() {
        if (getArguments() != null && getArguments().containsKey(FORM_KEY)) {
            AuthFormValidation form = Parcels.unwrap(getArguments().getParcelable(FORM_KEY));
            mvpPresenter.init(form);
        }
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // SignUpView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showFirstNameError(int errorRes) {
        showInputFieldError(et_firstname, errorRes);
        showErrIcon(et_firstname, R.drawable.err_input_require);
    }

    @Override
    public void showLastNameError(int errorRes) {
        showInputFieldError(et_lastname, errorRes);
        showErrIcon(et_lastname, R.drawable.err_input_require);
    }

    @Override
    public void setFirstName(String firstName) {
        et_firstname.setText(firstName);
    }

    @Override
    public void setLastName(String lastName) {
        et_lastname.setText(lastName);
    }

    @Override
    public void setAuthForm(AuthFormValidation form) {
        if (callback != null) {
            callback.openSignUpDetail(form, false);
        }
    }
}