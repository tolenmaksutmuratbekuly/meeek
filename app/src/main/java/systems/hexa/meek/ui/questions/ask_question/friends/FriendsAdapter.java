package systems.hexa.meek.ui.questions.ask_question.friends;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.data.models.questions.new_question.Friends;
import systems.hexa.meek.ui.base.recycler_view.RecyclerViewBaseAdapter;

class FriendsAdapter extends RecyclerViewBaseAdapter {
    private static final int FRIENDS_LAYOUT_ID = R.layout.adapter_friends;
    private static final int USERS_LAYOUT_ID = R.layout.adapter_friend_add_users;
    private static final int PROGRESS_BAR_LAYOUT_ID = R.layout.adapter_loading_item;

    private List<Friends> friends;
    private Callback callback;
    private boolean loading;
    private Context context;
    private boolean isUsers;

    FriendsAdapter(Context context) {
        this.context = context;
        this.friends = new ArrayList<>();
    }

    public void setUsers(boolean users) {
        isUsers = users;
        notifyDataSetChanged();
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    void addFriends(List<Friends> newItems) {
        int positionStart = friends.size();
        int itemCount = newItems.size();
        friends.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FRIENDS_LAYOUT_ID:
                return new FriendsViewHolder(inflate(parent, FRIENDS_LAYOUT_ID));
            case USERS_LAYOUT_ID:
                return new UsersViewHolder(inflate(parent, USERS_LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(parent, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else if (isUsers) {
            return USERS_LAYOUT_ID;
        } else {
            return FRIENDS_LAYOUT_ID;
        }
    }

    @Override
    public int getItemCount() {
        int count = friends.size();
        if (loading) {
            count += 1;
        }
        return count;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case FRIENDS_LAYOUT_ID:
                ((FriendsViewHolder) holder).bind(friends.get(position), position);
                break;
            case USERS_LAYOUT_ID:
                ((UsersViewHolder) holder).bind(friends.get(position), position);
                break;
        }
    }

    class FriendsViewHolder extends MainViewHolder {
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_username) TextView tv_username;
        @BindView(R.id.cb_select) ImageView cb_select;

        Friends bindedObject;
        int bindedPosition;

        FriendsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Friends friend, int position) {
            bindedObject = friend;
            bindedPosition = position;

            if (friend == null) {
                return;
            }

            tv_name.setText(friend.getName() + " " + friend.getSurname());
            tv_username.setText(friend.getUsername());
            cb_select.setImageResource(friend.isChecked() ? R.drawable.cb_white_active :
                    R.drawable.cb_white_default);
        }

        @OnClick(R.id.cb_select)
        void onCheckBox() {
            if (bindedObject.isChecked()) {
                cb_select.setImageResource(R.drawable.cb_white_default);
                bindedObject.setChecked(false);
            } else {
                cb_select.setImageResource(R.drawable.cb_white_active);
                bindedObject.setChecked(true);
            }
        }
    }

    class UsersViewHolder extends MainViewHolder {
        @BindView(R.id.ll_add) LinearLayout ll_add;
        @BindView(R.id.tv_name) TextView tv_name;
        @BindView(R.id.tv_username) TextView tv_username;

        Friends bindedObject;
        int bindedPosition;

        UsersViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Friends friend, int position) {
            bindedObject = friend;
            bindedPosition = position;

            if (friend == null) {
                return;
            }

            ll_add.setBackgroundResource(friend.isChecked() ? R.drawable.btn_selector_rounded_addfriend : R.drawable.btn_selector_rounded_addfriend);
            tv_name.setText(friend.getName() + " " + friend.getSurname());
            tv_username.setText(friend.getUsername());
        }

        @OnClick(R.id.ll_add)
        void onAddClick() {
            if (callback != null) {
                callback.onAddClick(bindedObject);
            }
        }
    }

    interface Callback {
        void onItemClick(QuestionsFeed questionsFeed, boolean answered);

        void onAddClick(Friends friends);
    }
}