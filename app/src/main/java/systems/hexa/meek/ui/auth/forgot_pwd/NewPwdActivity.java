package systems.hexa.meek.ui.auth.forgot_pwd;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.auth.forgot_pwd.NewPwdPresenter;
import systems.hexa.meek.mvp.auth.forgot_pwd.NewPwdView;
import systems.hexa.meek.ui.base.BaseActivity;

import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class NewPwdActivity extends BaseActivity implements NewPwdView {
    @InjectPresenter
    NewPwdPresenter mvpPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ll_content) LinearLayout ll_content;
    @BindView(R.id.et_new_pwd) MaterialEditText et_new_pwd;
    @BindView(R.id.et_confirm_pwd) MaterialEditText et_confirm_pwd;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_new_pwd;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, NewPwdActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter.onRestoreInstanceState(savedInstanceState);
        mvpPresenter.init();
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mvpPresenter.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_signin)
    void onSignInClick() {
        clearFocusFromAllViews(ll_content);
        hideSoftKeyboard(ll_content);
        showErrIcon(et_new_pwd, 0);
        showErrIcon(et_confirm_pwd, 0);
        mvpPresenter.setNewPwd(et_new_pwd.getText().toString());
        mvpPresenter.setConfirmPwd(et_confirm_pwd.getText().toString());
        mvpPresenter.validation();
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    private void showErrIcon(MaterialEditText editText, int rightIcon) {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightIcon, 0);
    }

    ///////////////////////////////////////////////////////////////////////////
    // NewPwdView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showNewPwdError(int errorRes) {
        showInputFieldError(et_new_pwd, errorRes);
        showErrIcon(et_new_pwd, R.drawable.err_input_require);
    }

    @Override
    public void showConfirmPwdError(int errorRes) {
        showInputFieldError(et_confirm_pwd, errorRes);
        showErrIcon(et_confirm_pwd, R.drawable.err_input_require);
    }

    @Override
    public void setNewPwd(String firstName) {

    }

    @Override
    public void setConfirmPwd(String lastName) {

    }
}
