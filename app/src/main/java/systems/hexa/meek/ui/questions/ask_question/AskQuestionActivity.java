package systems.hexa.meek.ui.questions.ask_question;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import it.sephiroth.android.library.widget.HListView;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.new_question.GradientColor;
import systems.hexa.meek.mvp.questions.ask_question.AskQuestionPresenter;
import systems.hexa.meek.mvp.questions.ask_question.AskQuestionView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.utils.ColorUtils;
import systems.hexa.meek.views.SettingsEditText;

import static systems.hexa.meek.utils.ContextUtils.clearFocusFromAllViews;
import static systems.hexa.meek.utils.ContextUtils.hideSoftKeyboard;

public class AskQuestionActivity extends BaseActivity1 implements AskQuestionView {
    @BindView(R.id.hlv_colors) HListView hlv_colors;
    @BindView(R.id.et_new_question) SettingsEditText etNewQuestion;
    @BindView(R.id.tv_max_character) TextView tvMaxCharacter;
    @BindView(R.id.fl_content) FrameLayout flContent;
    @BindView(R.id.ll_content) LinearLayout llContent;
    @BindView(R.id.tv_type) TextView tvType;

    private AskQuestionPresenter mvpPresenter;
    private GradientColorsAdapter colorsAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_new_question;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, AskQuestionActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new AskQuestionPresenter(dataLayer);
        mvpPresenter.attachView(this);

        configureColorsList();
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }

    @OnClick(R.id.tv_cancel)
    void onCancelClick() {
        goBackStack();
    }

    @OnClick(R.id.tv_done)
    void onDoneClick() {
        clearFocusFromAllViews(flContent);
        hideSoftKeyboard(flContent);
        mvpPresenter.onDoneClick(etNewQuestion.getText().toString());
    }

    @OnCheckedChanged(R.id.switch_question)
    void onSwitchChange(CompoundButton buttonView, boolean isChecked) {
        tvType.setText(getString(isChecked ? R.string.question_public : R.string.question_private));
    }

    private void configureColorsList() {
        tvMaxCharacter.setText(getString(R.string.sum_of_characters, etNewQuestion.getText().toString().length()));
        colorsAdapter = new GradientColorsAdapter(this);
        colorsAdapter.setCallback((colors, position) -> llContent.setBackground(ColorUtils.getGradient(this, colors)));
        hlv_colors.setAdapter(colorsAdapter);
        mvpPresenter.loadColors();
    }

    @OnTextChanged(R.id.et_new_question)
    void handleTextChange(CharSequence c, int start, int before, int count) {
        tvMaxCharacter.setText(getString(R.string.sum_of_characters, etNewQuestion.getText().toString().length()));
    }

    private void showInputFieldError(MaterialEditText inputField, @StringRes int errorRes) {
        inputField.setError(getString(errorRes));
    }

    ///////////////////////////////////////////////////////////////////////////
    // AskQuestionView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void addColors(List<GradientColor> colorList) {
        if (colorList.size() > 0)
            llContent.setBackground(ColorUtils.getGradient(this, colorList.get(0).colors));
        colorsAdapter.addColors(colorList);
    }

    @Override
    public void showQuestionInputError() {
        showInputFieldError(etNewQuestion, R.string.err_field_can_not_be_empty);
    }

    @Override
    public void openSelectLocationActivity() {
        startActivity(SelectLocationActivity.getIntent(this));
    }
}