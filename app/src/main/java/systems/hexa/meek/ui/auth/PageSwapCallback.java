package systems.hexa.meek.ui.auth;

import systems.hexa.meek.data.models.auth.AuthFormValidation;

public interface PageSwapCallback {
    void onLoginWithEmailClick();

    void onSignUpClick();

    void openSignUpDetail(AuthFormValidation form, boolean isGPlusAuth);

    void openSignUpFinish(AuthFormValidation form);

    void onRegistrationSuccess();
}
