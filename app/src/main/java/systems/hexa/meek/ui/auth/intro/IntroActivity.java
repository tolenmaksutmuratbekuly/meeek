package systems.hexa.meek.ui.auth.intro;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.auth.intro.IntroPresenter;
import systems.hexa.meek.mvp.auth.intro.IntroView;
import systems.hexa.meek.ui.base.BaseActivity;
import systems.hexa.meek.ui.base.view_pager.ViewPagerAdapter;
import systems.hexa.meek.ui.questions.QuestionsActivity;
import systems.hexa.meek.utils.Constants;
import systems.hexa.meek.views.dialogs.CommonDialog;

public class IntroActivity extends BaseActivity implements IntroView {
    @InjectPresenter
    IntroPresenter mvpPresenter;
    private ViewPagerAdapter adapter;

    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.btn_next) Button btn_next;
    @BindString(R.string.grand_access) String grand_access;
    @BindString(R.string.meek_location_dialog) String meek_location_dialog;
    @BindString(R.string.action_next) String action_next;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_intro;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, IntroActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewPager();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        mvpPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults, checkSelfPermissions());
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(IntroFragment.newInstance(R.layout.frgm_intro1));
        adapter.addFragment(IntroFragment.newInstance(R.layout.frgm_intro2));
        adapter.addFragment(IntroFragment3.newInstance());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                btn_next.setText(position == 2 ? grand_access : action_next);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    @OnClick(R.id.btn_next)
    void onNextClick() {
        if (viewPager.getCurrentItem() < adapter.getCount() - 1) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        } else {
            mvpPresenter.onGrandAccessClick(checkSelfPermissions(), checkPermissionRationale());
        }
    }

    public boolean checkSelfPermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkPermissionRationale() {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    ///////////////////////////////////////////////////////////////////////////
    // IntroView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openQuestionActivity() {
        startActivity(QuestionsActivity.getIntent(this, Constants.TYPE_WORLD, getString(R.string.t_world_desc)));
        finish();
    }

    // ===== Request Location permission =====
    @Override
    public void showGrandAccessDialog() {
        CommonDialog dialog = new CommonDialog(this);
        dialog.showDialogYesNo(meek_location_dialog);
        dialog.setCallbackYesNo(new CommonDialog.CallbackYesNo() {
            @Override
            public void onClickYes() {
                requestPermission();
            }

            @Override
            public void onClickNo() {

            }
        });
    }

    @Override
    public void requestPermission() {
        ActivityCompat.requestPermissions(IntroActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                IntroPresenter.MY_PERMISSIONS_REQUEST_LOCATION);
    }
}
