package systems.hexa.meek.ui.auth.forgot_pwd;

public interface PageSwapCallback {
    void onBackToSignInClick();

    void onSubmitClick();
}