package systems.hexa.meek.ui.auth.forgot_pwd;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.auth.forgot_pwd.ForgotPwdPresenter;
import systems.hexa.meek.mvp.auth.forgot_pwd.ForgotPwdView;
import systems.hexa.meek.ui.base.BaseFragmentActivity;

public class ForgotPwdActivity extends BaseFragmentActivity implements ForgotPwdView, PageSwapCallback {
    @InjectPresenter
    ForgotPwdPresenter mvpPresenter;
    private RecoverySentFragment recoverySentFragment;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_auth;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ForgotPwdActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        mvpPresenter.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState == null) {
            ForgotFragment forgotFragment = ForgotFragment.newInstance(mvpPresenter.getAuthForm());
            replaceFragment(forgotFragment, false);
        }
        toolbar.setNavigationOnClickListener(v -> goBackStack());
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mvpPresenter.onSaveInstanceState(outState);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ForgotPwdView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }

    @Override
    public void openMainScreen() {
//        startActivity(MainActivity.getIntent(this, false));
//        finish();
    }

    ///////////////////////////////////////////////////////////////////////////
    // PageSwapCallback implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackToSignInClick() {
        finish();
    }

    @Override
    public void onSubmitClick() {
        if (recoverySentFragment == null)
            recoverySentFragment = RecoverySentFragment.newInstance();
        toolbar.setVisibility(View.INVISIBLE);
        replaceFragment(recoverySentFragment, false);
    }
}
