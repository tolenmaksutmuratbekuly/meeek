package systems.hexa.meek.ui.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.mvp.profile.QuestionsTabPresenter;
import systems.hexa.meek.mvp.profile.QuestionsTabView;
import systems.hexa.meek.ui.base.BaseFragment1;
import systems.hexa.meek.ui.base.recycler_view.ListNoDataViewHolder;
import systems.hexa.meek.ui.questions.QuestionsAdapter;
import systems.hexa.meek.ui.questions.detail.QuestionDetailActivity;
import systems.hexa.meek.utils.recycler_view.EndlessScrollListener;
import systems.hexa.meek.utils.recycler_view.SwipeRefreshUtils;

public class QuestionsTabFragment extends BaseFragment1 implements QuestionsTabView {
    private QuestionsTabPresenter mvpPresenter;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.recycler_view) RecyclerView recycler_view;
    @BindView(R.id.list_placeholder_stub) ViewStub list_placeholder_stub;
    private ListNoDataViewHolder listPlaceholderViewHolder;
    private EndlessScrollListener endlessScrollListener;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_questions_tab;
    }

    public static QuestionsTabFragment newInstance() {
        return new QuestionsTabFragment();
    }

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new QuestionsTabPresenter(dataLayer);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        assert v != null;
        ButterKnife.bind(this, v);
        mvpPresenter.attachView(this);

        configureRecyclerView();
        mvpPresenter.loadMoreItems();

        return v;
    }

    @Override
    public void onDestroy() {
        mvpPresenter.detachView();
//        mvpPresenter.setErrorHandler(null);
        endlessScrollListener = null;
        recycler_view.clearOnScrollListeners();
        recycler_view.setAdapter(null);
        swipe_refresh_layout.setOnRefreshListener(null);
        listPlaceholderViewHolder = null;
        super.onDestroy();
    }


    private void configureRecyclerView() {
        QuestionsAdapter promoListAdapter = new QuestionsAdapter(getContext());
        promoListAdapter.setCallback(new QuestionsAdapter.Callback() {
            @Override
            public void onItemClick(QuestionsFeed questionsFeed, boolean answered) {
                startActivity(QuestionDetailActivity.getIntent(getContext(), questionsFeed.getAnswerType(), answered));
            }

            @Override
            public void qChangeListener() {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setAdapter(promoListAdapter);

        endlessScrollListener = new EndlessScrollListener(recycler_view) {
            @Override
            public void onRequestMoreItems() {
//                mvpPresenter.loadMoreItems();
            }
        };
        recycler_view.addOnScrollListener(endlessScrollListener);
        SwipeRefreshUtils.setColorSchemeColors(getContext(), swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
    }

    private void configureListPlaceholder() {
        listPlaceholderViewHolder.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
        listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(getContext());
    }

    ///////////////////////////////////////////////////////////////////////////
    // QuestionsTabView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void finish() {

    }

    @Override
    public Context getCtx() {
        return getContext();
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        recycler_view.post(() -> ((QuestionsAdapter) recycler_view.getAdapter()).setLoading(show));
    }

    @Override
    public void showRefreshingIndicator(boolean show) {
        swipe_refresh_layout.setRefreshing(show);
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setRefreshing(show);
        }
    }

    @Override
    public void addQuestionListItems(List<QuestionsFeed> questions) {
        recycler_view.post(() -> ((QuestionsAdapter) recycler_view.getAdapter()).addQuestions(questions));
        recycler_view.post(needMoreItemsChecker);
    }

    @Override
    public void showListPlaceholder(@StringRes int textRes) {
        if (listPlaceholderViewHolder == null) {
            listPlaceholderViewHolder = new ListNoDataViewHolder(list_placeholder_stub.inflate());
            configureListPlaceholder();
        }
        listPlaceholderViewHolder.setText(textRes);
        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaceholder() {
        if (listPlaceholderViewHolder != null) {
            listPlaceholderViewHolder.setVisibility(View.GONE);
        }
        recycler_view.setVisibility(View.VISIBLE);
    }
}