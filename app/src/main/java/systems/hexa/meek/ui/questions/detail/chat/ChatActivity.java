package systems.hexa.meek.ui.questions.detail.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.detail.chat.ChatMessage;
import systems.hexa.meek.mvp.questions.detail.chat.ChatPresenter;
import systems.hexa.meek.mvp.questions.detail.chat.ChatView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.base.recycler_view.ListNoDataViewHolder;
import systems.hexa.meek.utils.StringUtils;
import systems.hexa.meek.utils.recycler_view.EndlessScrollListener;

public class ChatActivity extends BaseActivity1 implements ChatView {
    private ChatPresenter mvpPresenter;
    @BindView(R.id.rv_chat) RecyclerView rv_chat;
    @BindView(R.id.et_chatbox) EditText et_chatbox;

    private ListNoDataViewHolder listPlaceholderViewHolder;
    private EndlessScrollListener endlessScrollListener;
    private ChatAdapter adapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_chat;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ChatActivity.class);
    }

    final Runnable needMoreItemsChecker = new Runnable() {
        @Override
        public void run() {
            if (endlessScrollListener != null) {
                endlessScrollListener.triggerCheckForMoreItems();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mvpPresenter = new ChatPresenter(dataLayer);
        mvpPresenter.attachView(this);

        configureRecyclerView();
        mvpPresenter.loadMoreItems();
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
//        mvpPresenter.setErrorHandler(null);
        endlessScrollListener = null;
        rv_chat.clearOnScrollListeners();
        rv_chat.setAdapter(null);
        listPlaceholderViewHolder = null;
        super.onDestroy();
    }

    @OnClick(R.id.img_send)
    void onMessageSendClick() {
        mvpPresenter.onMessageSendClick(et_chatbox.getText().toString());
    }

    private void configureRecyclerView() {
        adapter = new ChatAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rv_chat.setLayoutManager(mLayoutManager);
        rv_chat.setAdapter(adapter);

        endlessScrollListener = new EndlessScrollListener(rv_chat) {
            @Override
            public void onRequestMoreItems() {
//                mvpPresenter.loadMoreItems();
            }
        };
        rv_chat.addOnScrollListener(endlessScrollListener);
    }

    private void configureListPlaceholder() {
//        listPlaceholderViewHolder.setOnRefreshListener(() -> mvpPresenter.pullToRefreshItems());
        listPlaceholderViewHolder.setSwipeRefreshColorSchemeResources(this);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ChatView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void addMessages(List<ChatMessage> messageList) {
        et_chatbox.setText(StringUtils.EMPTY_STRING);
        rv_chat.post(() -> ((ChatAdapter) rv_chat.getAdapter()).addMessages(messageList));
        rv_chat.post(needMoreItemsChecker);
//        rv_chat.post(() -> rv_chat.scrollToPosition(adapter.getItemCount()));
    }

    @Override
    public void showListPlaceholder(@StringRes int textRes) {
//        if (listPlaceholderViewHolder == null) {
//            listPlaceholderViewHolder = new ListNoDataViewHolder(list_placeholder_stub.inflate());
//            configureListPlaceholder();
//        }
//        listPlaceholderViewHolder.setText(textRes);
//        listPlaceholderViewHolder.setVisibility(View.VISIBLE);
//        rv_chat.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaceholder() {
//        if (listPlaceholderViewHolder != null) {
//            listPlaceholderViewHolder.setVisibility(View.GONE);
//        }
//        rv_chat.setVisibility(View.VISIBLE);
    }
}