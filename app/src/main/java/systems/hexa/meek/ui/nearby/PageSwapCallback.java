package systems.hexa.meek.ui.nearby;

public interface PageSwapCallback {

    void onCorrectPassCode();
}