package systems.hexa.meek.ui.questions.ask_question;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.questions.ask_question.SelectLocationPresenter;
import systems.hexa.meek.mvp.questions.ask_question.SelectLocationView;
import systems.hexa.meek.ui.base.BaseActivity1;
import systems.hexa.meek.ui.questions.ask_question.friends.FriendsActivity;
import systems.hexa.meek.utils.ColorUtils;

public class SelectLocationActivity extends BaseActivity1 implements SelectLocationView {
    @BindView(R.id.fl_content) FrameLayout flContent;
    private SelectLocationPresenter mvpPresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_select_location;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, SelectLocationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvpPresenter = new SelectLocationPresenter(dataLayer);
        mvpPresenter.attachView(this);

        flContent.setBackground(ColorUtils.getGradient(this, R.color.nearby_gradient_1, R.color.nearby_gradient_2));
    }

    @Override
    protected void onDestroy() {
        mvpPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    private void goBackStack() {
        finish();
    }


    @OnClick(R.id.rl_friends)
    void onFriendsClick() {
        startActivity(FriendsActivity.getIntent(this));
    }

    ///////////////////////////////////////////////////////////////////////////
    // SelectLocationView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {

    }
}