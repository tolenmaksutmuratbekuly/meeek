package systems.hexa.meek.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.mvp.BaseMvpView;
import systems.hexa.meek.utils.SnackbarUtils;

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {
    @BindView(R.id.fl_parent) protected FrameLayout fl_parent;
    @BindView(R.id.pb_indicator) protected ViewGroup pb_indicator;
    protected DataLayer dataLayer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataLayer = DataLayer.getInstance(getContext());
    }

    protected abstract int getLayoutResourceId();

    protected abstract void onViewCreated(Bundle savedInstanceState);

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, view);
        onViewCreated(savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    ///////////////////////////////////////////////////////////////////////////
    // BaseMvpView implementation                                           ///
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showRequestError(String errorMessage) {
        SnackbarUtils.showSnackbar(fl_parent, errorMessage);
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onInvalidToken() {
//        InvalidToken.wipeOut(getContext(), dataLayer);
    }
}
