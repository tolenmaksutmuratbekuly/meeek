package systems.hexa.meek.ui.auth.forgot_pwd;

import android.content.Context;
import android.os.Bundle;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.mvp.auth.forgot_pwd.RecoveryPresenter;
import systems.hexa.meek.mvp.auth.forgot_pwd.RecoverySentView;
import systems.hexa.meek.ui.base.BaseFragment;

public class RecoverySentFragment extends BaseFragment implements RecoverySentView {
    @InjectPresenter
    RecoveryPresenter mvpPresenter;
    private PageSwapCallback callback;

    public static RecoverySentFragment newInstance() {
        return new RecoverySentFragment();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_forgot_recovery_sent;
    }

    @Override
    protected void onViewCreated(Bundle savedInstanceState) {
        mvpPresenter.init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (PageSwapCallback) context;
        } catch (ClassCastException castException) {
            castException.printStackTrace();
        }
    }

    @OnClick(R.id.btn_back_to_signin)
    void onBackToSignInClick() {
        if (callback != null) {
            callback.onBackToSignInClick();
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // RecoverySentView implementation
    ///////////////////////////////////////////////////////////////////////////

}