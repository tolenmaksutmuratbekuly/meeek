package systems.hexa.meek.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import systems.hexa.meek.R;
import systems.hexa.meek.data.DataLayer;
import systems.hexa.meek.data.network.exceptions.APIException;
import systems.hexa.meek.data.network.exceptions.ConnectionTimeOutException;
import systems.hexa.meek.data.network.exceptions.UnknownException;
import systems.hexa.meek.mvp.MVPView;

public abstract class BaseFragmentActivity1 extends AppCompatActivity implements MVPView {
    protected DataLayer dataLayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResourceId() != -1) setContentView(getLayoutResourceId());
        ButterKnife.bind(this);
        dataLayer = DataLayer.getInstance(this);
    }

    protected abstract int getLayoutResourceId();

    protected void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getName());
        }

        ft.replace(R.id.frgm_container, fragment, fragment.getClass().getName());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    protected Fragment findFragmentById() {
        return findFragmentById(R.id.frgm_container);
    }

    protected Fragment findFragmentById(int id) {
        return getSupportFragmentManager().findFragmentById(id);
    }

    //////////////////////////////////
    /// Delete it later            ///
    //////////////////////////////////
    @Override
    public void showProgressView(boolean show) {

    }

    @Override
    public void showProgressViewMsg(boolean show, String msg) {

    }

    @Override
    public void showResultView(boolean success) {

    }

    @Override
    public void showResultViewMsg(boolean success, String msg) {

    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void dismissProgressDialog() {

    }

    @Override
    public void handleAPIException(APIException e) {

    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {

    }

    @Override
    public void handleUnknownException(UnknownException e) {

    }

    @Override
    public void showErrorDialog(String message) {

    }

    @Override
    public String getUnknownExceptionMessage() {
        return null;
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return null;
    }
}