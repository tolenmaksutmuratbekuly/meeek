package systems.hexa.meek.ui.questions;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import systems.hexa.meek.R;
import systems.hexa.meek.data.models.questions.QuestionsFeed;
import systems.hexa.meek.ui.base.recycler_view.RecyclerViewBaseAdapter;
import systems.hexa.meek.utils.ToastUtils;
import systems.hexa.meek.views.progress_bar.NumberProgressBar;
import systems.hexa.meek.views.swipe_view.SwipeView;

public class QuestionsAdapter extends RecyclerViewBaseAdapter {
    private static final int QUESTION_LAYOUT_ID = R.layout.adapter_question;
    private static final int PROGRESS_BAR_LAYOUT_ID = R.layout.adapter_loading_item;

    private List<QuestionsFeed> questions;
    private Callback callback;
    private boolean loading;
    private Context context;

    public QuestionsAdapter(Context context) {
        this.context = context;
        this.questions = new ArrayList<>();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void setLoading(boolean loading) {
        if (this.loading && !loading) {
            this.loading = false;
            notifyItemRemoved(getItemCount());
        } else if (!this.loading && loading) {
            this.loading = true;
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void addQuestions(List<QuestionsFeed> newItems) {
        int positionStart = questions.size();
        int itemCount = newItems.size();
        questions.addAll(newItems);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case QUESTION_LAYOUT_ID:
                return new QuestionViewHolder(inflate(parent, QUESTION_LAYOUT_ID));
            case PROGRESS_BAR_LAYOUT_ID:
                return new SimpleViewHolder(inflate(parent, PROGRESS_BAR_LAYOUT_ID));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (loading && position == getItemCount() - 1) {
            return PROGRESS_BAR_LAYOUT_ID;
        } else {
            return QUESTION_LAYOUT_ID;
        }
    }

    @Override
    public int getItemCount() {
        int count = questions.size();
        if (loading) {
            count += 1;
        }
        return count;
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case QUESTION_LAYOUT_ID:
                ((QuestionViewHolder) holder).bind(questions.get(position), position);
                break;
        }
    }

    class QuestionViewHolder extends MainViewHolder {
        @BindView(R.id.swipe_view) SwipeView swipe_view;
        @BindView(R.id.ll_question_bg) LinearLayout ll_question_bg;
        @BindView(R.id.ll_answer_bg) LinearLayout ll_answer_bg;
        @BindView(R.id.tv_answer) TextView tv_answer;
        @BindView(R.id.tv_desc) TextView tv_desc;

        // Answer results
        @BindView(R.id.cv_statistics) CardView cv_statistics;
        @BindView(R.id.tv_desc_answer) TextView tv_desc_answer;
        @BindView(R.id.tv_answer_result) TextView tv_answer_result;
        @BindView(R.id.npb_yeah) NumberProgressBar npb_yeah;
        @BindView(R.id.npb_nope) NumberProgressBar npb_nope;
        @BindView(R.id.npb_neutral) NumberProgressBar npb_neutral;

        QuestionsFeed bindedQuestion;
        int bindedPosition;

        QuestionViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(QuestionsFeed question, int position) {
            bindedQuestion = question;
            bindedPosition = position;

            if (question == null) {
                return;
            }

            if (question.getAnswerType() != null && question.getAnswerType() != QuestionsFeed.TYPE_ANSWER_DEFAULT) {
                cv_statistics.setVisibility(View.VISIBLE);
                swipe_view.setVisibility(View.GONE);

                if (question.getAnswerType() == QuestionsFeed.TYPE_ANSWER_YEAH) {
                    answerTextStyle(R.string.answer_yeah, R.color.ans_yeap_bg, R.drawable.tv_rounded_ans_yeap);
                } else if (question.getAnswerType() == QuestionsFeed.TYPE_ANSWER_NOPE) {
                    answerTextStyle(R.string.answer_nope, R.color.ans_nope_bg, R.drawable.tv_rounded_ans_nope);
                } else if (question.getAnswerType() == QuestionsFeed.TYPE_ANSWER_NEUTRAL) {
                    answerTextStyle(R.string.answer_neutral, R.color.ans_neutral_bg, R.drawable.tv_rounded_ans_neutral);
                }
            } else {
                cv_statistics.setVisibility(View.GONE);
                swipe_view.setVisibility(View.VISIBLE);
            }

            swipe_view.setListener(new SwipeView.SwipeListener() {
                @Override
                public void onViewSwipedToTop() {
                    ToastUtils.showToast(context, "SWIPED TO TOP");
                    question.setAnswerType(QuestionsFeed.TYPE_ANSWER_NEUTRAL);
                    notifyItemChanged(position);
                }

                @Override
                public void onViewSwipedToLeft() {
                    question.setAnswerType(QuestionsFeed.TYPE_ANSWER_NOPE);
                    ToastUtils.showToast(context, "SWIPED TO LEFT");
                    notifyItemChanged(position);
                }

                @Override
                public void onViewSwipedToRight() {
                    question.setAnswerType(QuestionsFeed.TYPE_ANSWER_YEAH);
                    ToastUtils.showToast(context, "SWIPED TO RIGHT");
                    notifyItemChanged(position);
                }

                @Override
                public void onSwipeStart() {

                }

                @Override
                public void onSwipeProgress(int swipeDirection) {
                    if (swipeDirection == SwipeView.SWIPE_DIRECTION_ONLY_RIGHT) {
                        answerBgStyle(R.string.answer_yeah, R.color.ans_yeap);
                    } else if (swipeDirection == SwipeView.SWIPE_DIRECTION_ONLY_LEFT) {
                        answerBgStyle(R.string.answer_nope, R.color.ans_nope);
                    } else if (swipeDirection == SwipeView.SWIPE_DIRECTION_ONLY_TOP) {
                        answerBgStyle(R.string.answer_neutral, R.color.ans_neutral);
                    } else {
                        ll_answer_bg.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onSwipeEnd() {
                    ll_answer_bg.setVisibility(View.GONE);
                }

                @Override
                public void onViewClick() {
                    onItemViewClick();
                }
            });
            ll_question_bg.setBackground(question.getGradient());

        }

        void answerBgStyle(@StringRes int title, @ColorRes int color) {
            tv_answer.setText(title);
            ll_answer_bg.setBackgroundColor(ContextCompat.getColor(context, color));
            ll_answer_bg.setVisibility(View.VISIBLE);
        }

        void answerTextStyle(@StringRes int title, @ColorRes int color, @DrawableRes int bg) {
            tv_answer_result.setText(title);
            tv_answer_result.setTextColor(ContextCompat.getColor(context, color));
            tv_answer_result.setBackground(ContextCompat.getDrawable(context, bg));
        }

        @OnClick(R.id.ll_content)
        void onViewClick() {
            onItemViewClick();
        }

        void onItemViewClick() {
            if (callback != null) {
                callback.onItemClick(bindedQuestion,
                        (bindedQuestion.getAnswerType() != null
                                && bindedQuestion.getAnswerType() != QuestionsFeed.TYPE_ANSWER_DEFAULT));
            }
        }
    }

    public interface Callback {
        void onItemClick(QuestionsFeed questionsFeed, boolean answered);

        void qChangeListener();
    }
}